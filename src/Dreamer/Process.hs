{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Dreamer.Process
(
    Process(..),
    Process2(..),
    MFRProcess(..),
    MFRProcess2(..),
    IntuitiveProcess(..),
    IntuitiveProcess2(..),
    CoreProcess(..),
    CoreProcess2(..),
    BareProcess(..),
    ComboProcess(..),
    ComboProcess2(..),
    IRCProcess(..),

    defBareP,
    defCoreP,
    defCoreP2,
    defComboP,
    defComboP2
) where

import Data.Aeson
import Dreamer.Atmosphere.Atom
import Dreamer.Internal.Limit
import Dreamer.Internal.Operation

class Process d v where
    defproc :: v -> Timestamp -> d v
    defproc' :: v -> Timestamp -> d v
    defproc'' :: v -> Timestamp -> d v
    defproc''' :: v -> Timestamp -> d v
    defproc'''' :: v -> Timestamp -> d v
    defproc''''' :: v -> Timestamp -> d v
    currval :: d v -> v

    defproc' = defproc
    defproc'' = defproc
    defproc''' = defproc
    defproc'''' = defproc
    defproc''''' = defproc

    {-# MINIMAL defproc | currval #-}

class (Process d v) => Process2 d v where
    defproc2 :: v -> Timestamp -> d v
    defproc2' :: v -> Timestamp -> d v
    defproc2'' :: v -> Timestamp -> d v
    defproc2''' :: v -> Timestamp -> d v
    defproc2'''' :: v -> Timestamp -> d v
    defproc2''''' :: v -> Timestamp -> d v
    currval2 :: d v -> BareProcess v

    defproc2' = defproc2
    defproc2'' = defproc2
    defproc2''' = defproc2
    defproc2'''' = defproc2
    defproc2''''' = defproc2

    {-# MINIMAL defproc2 | currval2 #-}

class (Process d v) => MFRProcess d v where
    mframp :: d v -> CoreProcess QAtomFull
    mfrcombo :: d v -> (ComboProcess v) QAtomPos

class (MFRProcess d v) => MFRProcess2 d v where
    mframp2 :: d v -> CoreProcess2 QAtomFull
    mfrcombo2 :: d v -> (ComboProcess2 v) QAtomPos

-- The intuitive process is made of a core process and three MFR processes.
-- Being a process itself, in addition to those inner processes, it has a
-- current value of its own, usually determined by the inner processes.
class (Process (d md) v, MFRProcess md v) => IntuitiveProcess d md v where
    intuicore :: d md v -> CoreProcess v
    intuimu :: d md v -> md v
    intuifo :: d md v -> md v
    intuiri :: d md v -> md v

class (Process2 (d md) v, IntuitiveProcess d md v, MFRProcess2 md v) => IntuitiveProcess2 d md v where
    intuicore2 :: d md v -> CoreProcess2 v
    intuimu2 :: d md v -> md v
    intuifo2 :: d md v -> md v
    intuiri2 :: d md v -> md v

data BareProcess v = BareP {
    bvalue :: v,
    btimes :: QAtomPosInf,
    bstamp :: Timestamp,
    btotal :: QAtomPosInf
} | StrippedBareP v deriving (Show, Read)

data CoreProcess v = CoreP {
    value  :: v,
    times  :: QAtomPosInf,
    stamp  :: Timestamp,
    total  :: QAtomPosInf,
    change :: BareProcess QAtomPos
} | StrippedCoreP v | UpgradedCoreP (CoreProcess2 v) deriving (Show, Read)

data CoreProcess2 v = CoreP2 {
    value2  :: CoreProcess v,
    times2  :: BareProcess QAtomPosInf,
    stamp2  :: BareProcess Timestamp,
    total2  :: BareProcess QAtomPosInf,
    change2 :: BareProcess QAtomPos
} | StrippedCoreP2 (CoreProcess v) deriving (Show, Read)

{-
    ComboProcess can be used in the following manner:
    Say you have an element. We check the current position
    of the ComboProcess. We take the result of currpos % elemlim
    and use that as the position of the CoreProcess we are to
    update in the elems array. We proceed with updating the
    CoreProcess in a manner similar to ongoing frequency.
    We may then calculate the atemporal diversity of elems.
-}
data ComboProcess v a = ComboP {
    ambig :: a,
    elems   :: [CoreProcess v],
    currpos :: QAtomPosInf
} | StrippedComboP a | UpgradedComboP (ComboProcess2 v a) deriving (Show, Read)

data ComboProcess2 v a =  ComboP2 {
    ambig2 :: BareProcess a,
    elems2   :: [CoreProcess2 v],
    currpos2 :: BareProcess QAtomPosInf
} | StrippedComboP2 (ComboProcess v a) deriving (Show, Read)

data IRCProcess n d (md :: * -> *) v = IRCP {
    irccurrval :: n,
    ircrange :: (QAtomPos, (n,n)),
    ircintui :: d md v
} deriving (Show, Read)

instance Process BareProcess v where
    defproc qv qstamp = defBareP qv qstamp
    currval = bvalue

instance Process CoreProcess v where
    defproc qv qstamp = defCoreP qv qstamp
    currval = value

instance Process (ComboProcess v) a where
    currval = ambig

instance Process CoreProcess2 v where
    defproc qv qstamp = StrippedCoreP2 $ defCoreP qv qstamp
    currval = value . value2

instance Process (ComboProcess2 v) a where
    currval = bvalue . ambig2

instance (IntuitiveProcess d md v) => Process (IRCProcess n d md) v where
    currval (IRCP _ _ d) = currval d

instance (IntuitiveProcess d md v) => IntuitiveProcess (IRCProcess n d) md v where
    intuicore (IRCP _ _ d) = intuicore d
    intuimu (IRCP _ _ d) = intuimu d
    intuifo (IRCP _ _ d) = intuifo d
    intuiri (IRCP _ _ d) = intuiri d

-- Process2
instance Process2 CoreProcess2 v where
    defproc2 qv qstamp = defCoreP2 (defCoreP qv qstamp) qstamp
    currval2 cd2 = let
        cd = value2 cd2
        in BareP {bvalue=(value cd), btimes=(times cd), bstamp=(stamp cd), btotal=(total cd)}

instance Process2 (ComboProcess2 v) QAtomPos where
    currval2 = ambig2

defCoreP :: v -> Timestamp -> CoreProcess v
defCoreP qv qstamp = CoreP {value=qv, times=unitAtomPosInf, total=minAtomPosInf, change=defBareP minAtomPos qstamp, stamp=qstamp}

defCoreP2 :: CoreProcess v -> Timestamp -> CoreProcess2 v
defCoreP2 cp qstamp = CoreP2 {value2=cp, times2=defBareP unitAtomPosInf qstamp, total2=defBareP minAtomPosInf qstamp, change2=defBareP minAtomPos qstamp, stamp2=defBareP qstamp qstamp}

defBareP :: v -> Timestamp -> BareProcess v
defBareP qv qstamp = BareP {bvalue=qv, btimes=unitAtomPosInf, btotal=minAtomPosInf, bstamp=qstamp}

defComboP :: v -> Timestamp -> ComboProcess v QAtomPos
defComboP qv qstamp = ComboP {ambig=zeroAtomPos, elems=[defCoreP qv qstamp], currpos=zeroAtomPosInf}

defComboP2 :: CoreProcess v -> Timestamp -> ComboProcess2 v QAtomPos
defComboP2 cp qstamp = ComboP2 {ambig2=defBareP zeroAtomPos qstamp, elems2=[defCoreP2 cp qstamp], currpos2=defBareP zeroAtomPosInf qstamp}
