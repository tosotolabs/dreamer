module Dreamer.Internal.Operation
(
    -- Strings
    String255,
    mkjson,
    mkstring255,
    tolower,
    ----- Text
    gettval,

    -- Numbers
    avg,
    biasedavg,
    fi,
    getdval,
    getival,
    inrange,
    tugofwar,

    -- Time
    getnanotime,

    -- Maybe
    condmaybe,
    getmaybe,
    getmaybe',

    -- Lists
    combinations,
    norm,
    norm2,
    normwith,
    norm2with,

    -- Tuples
    third4,
    fourth4,

    -- Graph-related
    atempdiversity,
    fba3,
    fba3_T,

    -- logging
    Logger,
    ErrorLogger,
    logerrors,
    tolog
) where

import           Control.Exception                 (onException)
import qualified Data.ByteString.Char8             as B (ByteString, appendFile,
                                                         pack)
import           Data.Graph.Inductive.Graph
import           Data.Graph.Inductive.PatriciaTree
import qualified Data.List                         as L
import           Data.Monoid                       ((<>))
import qualified Data.Text                         as T
import           Data.Time.LocalTime
import           Database.Bolt                     hiding (Structure)
import           Dreamer.Metadata
import           System.Clock
import           System.IO                         (FilePath)

-- Strings
mkjson :: (Bool, Bool) -> [(String, String)] -> String
mkjson (esck, escv) ((xk,xv):xs) = "{" ++ (foldl (\acc (k,v) -> let
        k' = if esck then (show k) else k
        v' = if escv then (show v) else v
        in acc++", "++k'++":"++v') ((if esck then show xk else xk)++":"++(if escv then show xv else xv)) xs) ++ "}"

tolower :: String -> String
tolower = (T.unpack . T.toLower . T.pack)

----- Text
gettval :: Value -> T.Text
gettval (T a) = a

-- Numbers
avg :: [Double] -> Double
avg xs = (sum xs)/(fi $ length xs)

biasedavg :: Double -> Double -> Double -> Double
biasedavg 0 0 _ = 0
biasedavg 0 y b = y/(2+b)
biasedavg x y b = (x+y)/(2+(((y/x)-1)*b))

fi :: (Integral a, Num b) => a -> b
fi = fromIntegral

getdval :: Value -> Double
getdval (F a) = a

getival :: Value -> Int
getival (I a) = a

inrange :: (Double, Double) -> Double -> Bool
inrange (a,b) x = let
    (a',b') = if a<b then (a,b) else (b,a)
    in x >= a' && x <= b'

-- a should in [bx,by]
similarpos :: Double -> (Double, Double) -> (Double, Double) -> Double
similarpos a (bx,rx) (by,ry) = ((a-(min bx by))/(abs $ by-bx)) * (abs (ry-rx)) + (min rx ry)

-- Does not initially sort pairs before calculation
similarpos' :: Double -> (Double, Double) -> (Double, Double) -> Double
similarpos' a (bx,rx) (by,ry) = let denom = by-bx
    in ((a-bx)/if denom == 0 then 1 else denom) * (abs (ry-rx)) + (min rx ry)
-- zero denom

sortpairs2 :: (Double, Double) -> (Double, Double) -> ((Double, Double), (Double, Double))
sortpairs2 (a,x) (b,y) = if a > b then ((b,y), (a,x)) else ((a,x), (b,y))

tugofwar :: (Double, Double) -> (Double, Double) -> Double
tugofwar (a,x) (b,y) = let
    (minab, maxab) = (min a b, max a b)
    (ar, br) = if minab == a then (x,y) else (y,x)
    p = (abs $ b-a)/2
    posx = minab + (p - p * ar)
    posy = maxab - (p - p * br)
    in (posx+posy)/2

-- Does not initially sort pairs before calculation
tugofwar' :: (Double, Double) -> (Double, Double) -> Double
tugofwar' (a,x) (b,y) = let
    p = (b-a)/2
    posx = a + (p - p * x)
    posy = b - (p - p * y)
    in (posx+posy)/2

-- Time
getnanotime :: IO Double
getnanotime = do currtime <- getTime Realtime; return $ fromIntegral $ toNanoSecs currtime

-- Maybe
condmaybe :: Maybe a -> (a -> b) -> b -> b
condmaybe mb f b = case mb of
    Just x    -> f x
    otherwise -> b

getmaybe :: Maybe a -> a -> a
getmaybe mb a = maybe a id mb

getmaybe' :: Maybe a -> a
getmaybe' mb = case mb of
    Just a    -> a
    otherwise -> error "Got Nothing"

-- Lists
combinations :: Int -> [a] -> [[a]]
combinations 0 _  = [ [] ]
combinations n xs = [ y:ys | y:xs' <- L.tails xs
                           , ys <- combinations (n-1) xs']

-- TODO:FIX: very naive; normalize with sqrt of sum of all squared elements
norm :: [Double] -> [Double]
norm xs = normwith (maximum xs) xs

norm2 :: [(a, Double)] -> [(a, Double)]
norm2 xs = norm2with (maximum (map (\(_,v) -> v) xs)) xs

normwith :: Double -> [Double] -> [Double]
normwith n xs = map (\v -> v/n) xs

norm2with :: Double -> [(a, Double)] -> [(a, Double)]
norm2with n xs = map (\(a,v) -> (a,v/n)) xs

-- Tuples
fst4 :: (a,b,c,d) -> a
fst4 (a,_,_,_) = a

snd4 :: (a,b,c,d) -> b
snd4 (_,b,_,_) = b

third4 :: (a,b,c,d) -> c
third4 (_,_,c,_) = c

fourth4 :: (a,b,c,d) -> d
fourth4 (_,_,_,d) = d

-- This is simple combination diversity calculation.
atempdiversity :: [Double] -> Double
atempdiversity xs = if length xs == 1
    then 0
    else let
        xs' = norm xs
        gr = (mkGraph [(n,xs' !! n) | n <- [0..((length xs')-1)]]
                        [(n,a,abs $ (xs' !! n)-(xs' !! a))
                            |   n <- [0..((length xs')-1)],
                                a <- [(n+1)..((length xs')-1)]]) :: Gr Double Double
        subAvgs = [(sum $ map edgeLabel $ (out gr n) ++ (inn gr n))/(fi $ (order gr)-1) | n <- [0..(fi $ (order gr)-1)]]
        in (sum subAvgs)/(fi $ order gr)

-- TODO We need combination diversity calculation that uses fba3 and all that.

type FBAValue = (Double, Double, Double)

fba3 :: FBAValue -> FBAValue -> FBAValue -> Double -> Double
fba3 (x,bx,rx) (y,by,ry) (z,bz,rz) sl = if all (\v -> v <= sl) $ map abs [a-b, a-c, b-c]
    then avg [a,b,c]
    else fba3 (a,ba,ra) (b,bb,rb) (c,bc,rc) sl
        where
            a = tugofwar (x,bx) (y,by)
            (vbx,vby) = sortpairs2 (bx,rx) (by,ry)
            ba = tugofwar' vbx vby
            ra = similarpos' ba vbx vby
            b = tugofwar (x,bx) (z,bz)
            (vbx',vbz) = sortpairs2 (bx,rx) (bz,rz)
            bb = tugofwar' vbx' vbz
            rb = similarpos' bb vbx' vbz
            c = tugofwar (y,by) (z,bz)
            (vby',vbz') = sortpairs2 (by,ry) (bz,rz)
            bc = tugofwar' vby' vbz'
            rc = similarpos' bc vby' vbz'

fba3_T :: FBAValue -> FBAValue -> FBAValue -> (Double, Int) -> IO Double
fba3_T (x,bx,rx) (y,by,ry) (z,bz,rz) (sl,stage) =
    if (all (\v -> v <= sl) $ map abs [x-y, x-z, y-z]) -- || (all (\v -> v == 0) [bx,by,bz]) || (all (\v -> v == 0) [rx,ry,rz])
    then do
        let res = (avg [a,b,c],stage)
        putStrLn $ show res
        return $ fst res
    else do
        putStrLn $ show (x,bx,rx)
        putStrLn $ show (y,by,ry)
        putStrLn $ show (z,bz,rz)
        putStrLn $ show $ map abs [a-b, a-c, b-c]
        putStrLn $ show "info: " ++ show (sl,stage)
        putStrLn ""
        fba3_T (a,ba,ra) (b,bb,rb) (c,bc,rc) (sl,stage+1)
        where
            a = tugofwar (x,bx) (y,by)
            (vbx,vby) = sortpairs2 (bx,rx) (by,ry)
            ba = tugofwar' vbx vby
            ra = similarpos' ba vbx vby
            b = tugofwar (x,bx) (z,bz)
            (vbx',vbz) = sortpairs2 (bx,rx) (bz,rz)
            bb = tugofwar' vbx' vbz
            rb = similarpos' bb vbx' vbz
            c = tugofwar (y,by) (z,bz)
            (vby',vbz') = sortpairs2 (by,ry) (bz,rz)
            bc = tugofwar' vby' vbz'
            rc = similarpos' bc vby' vbz'

type Logger = String -> IO ()
type ErrorLogger = Logger

tolog :: FilePath -> String -> IO ()
tolog logpath str = flip onException (return ()) $ do
    ctime <- getZonedTime
    B.appendFile logpath $ B.pack $ (show ctime)++" - "++str++"\n"

logerrors :: ErrorLogger -> Int -> String -> IO ()
logerrors errorlog nerrs msg = if nerrs > 0 then errorlog $ "("++(show nerrs)++") "++msg else return ()
