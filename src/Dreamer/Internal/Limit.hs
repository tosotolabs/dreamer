module Dreamer.Internal.Limit
(
    comboelemlim,
    fba3depthlim,
    fba3stoplim,
    probelim,
    themin
) where

comboelemlim :: Int
comboelemlim = 10

fba3depthlim :: Int
fba3depthlim = 10

fba3stoplim :: Double
fba3stoplim = 5e-5

probelim :: Int
probelim = 5

themin :: Double
themin = 1e-20
