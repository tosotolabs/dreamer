{- Distributed lab collaboration -}

{-# LANGUAGE DeriveGeneric #-}

module Dreamer.Lab.Crew
(
    Chief(..),
    ChiefMessage(..),
    Crew(..),
    CrewAuth,
    Growth,
    GrowthShare,
    Worker(..),
    WorkerID,
    WorkerMessage(..),

    chiefserve,
    startworker
) where

import           Control.Concurrent                                (threadDelay)
import           Control.Concurrent.STM
import           Control.Distributed.Process                       (DiedReason (..),
                                                                    NodeId (..),
                                                                    PortMonitorNotification (..),
                                                                    Process (..),
                                                                    ProcessId (..),
                                                                    ReceivePort,
                                                                    SendPort,
                                                                    WhereIsReply (..),
                                                                    expectTimeout,
                                                                    link,
                                                                    monitorPort,
                                                                    processNodeId,
                                                                    receiveChan,
                                                                    register,
                                                                    sendPortId,
                                                                    spawnLocal,
                                                                    whereisRemoteAsync)
import           Control.Distributed.Process.Async                 (AsyncTask (..),
                                                                    async, task)
import           Control.Distributed.Process.Extras.Time           (Delay (..))
import           Control.Distributed.Process.ManagedProcess        (ActionHandler,
                                                                    CastHandler,
                                                                    ChannelHandler,
                                                                    InitResult (..),
                                                                    ProcessDefinition (..),
                                                                    defaultProcess,
                                                                    serve)
import           Control.Distributed.Process.ManagedProcess.Client (callChan,
                                                                    cast)
import           Control.Distributed.Process.ManagedProcess.Server (continue,
                                                                    handleCast,
                                                                    handleInfo,
                                                                    handleRpcChan,
                                                                    replyChan)
import           Control.Distributed.Process.Node                  (initRemoteTable,
                                                                    newLocalNode,
                                                                    runProcess)
import           Control.Monad                                     (forM_,
                                                                    forever,
                                                                    void)
import           Control.Monad.IO.Class                            (liftIO)
import           Data.Binary                                       hiding
                                                                    (decode,
                                                                    encode)
import qualified Data.ByteString.Char8                             as B (pack)
import qualified Data.Map.Strict                                   as M
import           Data.Typeable                                     hiding (cast)
import           Data.UUID                                         (toString)
import           Data.UUID.V4                                      (nextRandom)
import           Dreamer.Internal.Operation
import           GHC.Generics
import           Network.Transport                                 (EndPointAddress (..))
import           Network.Transport.TCP                             (createTransport,
                                                                    defaultTCPParameters)
import           System.Environment                                (getArgs,
                                                                    lookupEnv)

type ChiefID = String
type WorkerID = String
type CrewAuth = String -- environment variable
type LabLocation = String
-- | This is a measure of a worker's processing workercapability relative
-- to other workers.
type GrowthShare = Int
type Offset = Int
type Growth = Int
type Token = String
type Error = String
type Age_ = Double
type Host = String
type Port = Int
type Manifest = (Chief, Crew, CrewAuth)
type Jobs = Int

type Crew = TMVar (M.Map WorkerID (Token, SendPort ChiefMessage, GrowthShare, Jobs))

data Chief = Chief {
    chiefid          :: String,
    chiefgrowthshare :: GrowthShare,
    chiefhost        :: String,
    chiefport        :: Int
}
data Worker = Worker {
    workerid          :: String,
    password          :: String,
    workergrowthshare :: GrowthShare
} deriving (Generic, Typeable, Show)

data ChiefMessage = Adapt Age_ (Maybe Growth) | Coordinate Age_ | General {code :: Int, message:: String} deriving (Generic, Typeable, Show)
data WorkerMessage = JoinCrew Worker | DidAdapt {age :: Age_, growth :: Maybe Growth, wrkrid :: WorkerID, token ::  Token} | DidCoordinate {age :: Age_, wkrid :: WorkerID, token ::  Token} | JobsUpdate {jobs :: Jobs, wrkrid :: WorkerID, token :: Token} deriving (Generic, Typeable, Show)
errorcode = 500 :: Int
okcode = 200 :: Int
signedincode = 201 :: Int

instance Binary ChiefMessage
instance Binary Worker
instance Binary WorkerMessage

authworker :: String -> CrewAuth -> IO (Maybe String)
authworker pword env = do
    mbhword <- lookupEnv env
    case mbhword of
        Nothing -> return Nothing
        Just hword ->
            if hword == pword
                then do
                    tokuuid <- nextRandom
                    return $ Just $ toString tokuuid
                else return Nothing

joincrewhandler :: CrewAuth -> (Logger,ErrorLogger) -> ChannelHandler Crew WorkerMessage ChiefMessage
joincrewhandler crewauth (logger,elogger) sendport = handler
    where
        handler :: ActionHandler Crew WorkerMessage
        handler crewstate joincrew@(JoinCrew (Worker wid pword capb)) = do
            crew <- liftIO $ atomically $ takeTMVar crewstate
            if wid `M.member` crew
                then continue crewstate
                else do
                    void $ monitorPort sendport
                    mbauth <- liftIO $ authworker pword crewauth
                    case mbauth of
                        Nothing -> do
                            liftIO $ atomically $ putTMVar crewstate crew
                            replyChan sendport (General errorcode "Failed to authorize you.")
                            continue crewstate
                        Just tok -> do
                            let crew' = M.insert wid (tok,sendport,capb,0) crew
                            liftIO $ atomically $ putTMVar crewstate crew'
                            replyChan sendport (General signedincode tok)
                            liftIO $ logger $ wid ++ " has joined the lab."
                            continue crewstate
        handler crewstate _ = replyChan sendport (General errorcode "This gate is for those looking to join the crew.") >> continue crewstate

messagehandler :: ChiefID -> (WorkerMessage -> IO ()) -> (Logger,ErrorLogger) -> CastHandler Crew WorkerMessage
messagehandler chid act (logger,elogger) = handler
    where
        handler :: ActionHandler Crew WorkerMessage
        handler crewstate (JoinCrew {}) = continue crewstate
        handler crewstate (JobsUpdate {}) = continue crewstate
        handler crewstate wkrmsg@_ = do
            crew <- liftIO $ atomically $ readTMVar crewstate
            let wid = wrkrid wkrmsg
            case M.lookup wid crew of
                Nothing -> continue crewstate
                Just (tok,sp,_,_) -> if tok == (token wkrmsg)
                    then do
                        liftIO $ act wkrmsg
                        continue crewstate
                    else do
                        replyChan sp (General errorcode "Bad authorization token given.")
                        continue crewstate

disconnecthandler :: (Logger,ErrorLogger) -> ActionHandler Crew PortMonitorNotification
disconnecthandler (logger,elogger) crewstate (PortMonitorNotification _ spid reason) = do
    crew <- liftIO $ atomically $ takeTMVar crewstate
    let search = M.filter (\(_,sp,_,_) -> sendPortId sp == spid) crew
    case (null search, reason) of
        (False, DiedDisconnect) -> do
            let (wid,(_,_,_,js)) = M.elemAt 0 search
                crew' = M.delete wid crew
            liftIO $ do
                logger $ wid ++ " left the lab "++(if js>0 then "with "++(show js)++" jobs yet to be finished." else "with all jobs completed.")
                atomically $ putTMVar crewstate crew'
            continue crewstate
        _ -> do
            liftIO $ atomically $ putTMVar crewstate crew
            continue crewstate

launchnabu :: ChiefID -> Crew -> CrewAuth -> (WorkerMessage -> IO ()) -> (Logger,ErrorLogger) -> Process ProcessId
launchnabu chid crewstate crewauth wkrmsgact loggers = let
    server = defaultProcess {
                    apiHandlers = [handleRpcChan $ joincrewhandler crewauth loggers, handleCast $ messagehandler chid wkrmsgact loggers],
                    infoHandlers = [handleInfo $ disconnecthandler loggers]
                }
    in spawnLocal $ serve () (const (return $ InitOk crewstate Infinity)) server

chiefserve :: Chief -> Crew -> CrewAuth -> (WorkerMessage -> IO ()) -> (Logger,ErrorLogger) -> IO (Either Error (SendPort ChiefMessage -> ChiefMessage -> IO ()))
chiefserve chief crewstate crewauth wkrmsgact (logger,elogger) = do
    let (host,port) = (chiefhost chief, chiefport chief)
    mt <- createTransport host (show port) (\_ -> (host,show port)) defaultTCPParameters
    case mt of
        Right transport -> do
            node <- newLocalNode transport initRemoteTable
            runProcess node $ void $ async $ task $ do
                    pid <- launchnabu (chiefid chief) crewstate crewauth wkrmsgact (logger,elogger)
                    liftIO $ logger $ "The lab chief is serving at: " ++ show (nodeAddress . processNodeId $ pid)
                    register (chiefid chief) pid
                    void $ forever $ liftIO $ threadDelay 500000
            return $ Right $ \sp chiefmsg -> runProcess node $ replyChan sp chiefmsg
        Left err -> do
            elogger $ show err
            print err
            return $ Left $ show err

searchchief :: ChiefID -> LabLocation -> Process ProcessId
searchchief chid labloc = do
    let loc = EndPointAddress (B.pack labloc)
        labid = NodeId loc
    whereisRemoteAsync labid chid
    reply <- expectTimeout 1000
    -- liftIO $ putStrLn "Searching for lab and chief"
    case reply of
        Just (WhereIsReply _ (Just cpid)) -> return cpid
        Just t                                 -> do liftIO $ print t; searchchief chid labloc
        _ -> searchchief chid labloc

startworker :: Worker
            -> Host
            -> Port
            -> (Maybe Growth -> IO (Maybe Growth))
            -> LabLocation
            -> ChiefID
            -> (Logger,ErrorLogger)
            -> IO ()
startworker worker@(Worker wid pword capb) host port wkradapt labloc chid (logger,elogger) = do
    mt <- createTransport host (show port) (\_ -> (host,(show port))) defaultTCPParameters
    case mt of
        Left err -> do
            elogger $ show err
            print $ show err
        Right transport -> do
            node <- newLocalNode transport initRemoteTable
            runProcess node $ do
                chiefpid <- searchchief chid labloc
                link chiefpid
                liftIO $ logger $ wid ++ " is attempting to join the lab at "++labloc++" with chief, "++chid
                rp <- callChan chiefpid (JoinCrew worker) :: Process (ReceivePort ChiefMessage)
                wkrstate <- liftIO $ atomically $ newTMVar Nothing
                tokenstate <- liftIO $ atomically $ newTMVar ""
                jobstate <- liftIO $ atomically $ newTMVar 0
                void $ forever $ do
                    capmsg <- receiveChan rp
                    case capmsg of
                        General 500 err -> liftIO $ elogger err >> putStrLn err
                        General 201 tok -> do
                            void $ liftIO $ atomically $ swapTMVar tokenstate tok
                            let okmsg = wid ++ " was authorized to join the lab."
                            liftIO $ putStrLn okmsg
                            liftIO $ logger okmsg
                        Adapt age mbgrowth -> do
                            tok <- liftIO $ atomically $ readTMVar tokenstate
                            js <- liftIO $ atomically $ takeTMVar jobstate
                            void $ liftIO $ atomically $ putTMVar jobstate $ js+1
                            cast chiefpid $ JobsUpdate (js+1) wid tok
                            void $ async $ task $ do
                                mbgrowth' <- liftIO $ wkradapt mbgrowth
                                js <- liftIO $ atomically $ takeTMVar jobstate
                                void $ liftIO $ atomically $ putTMVar jobstate $ js-1
                                cast chiefpid $ DidAdapt age mbgrowth' wid tok
                                cast chiefpid $ JobsUpdate (js-1) wid tok
                        Coordinate _ -> return () -- TODO support this later
