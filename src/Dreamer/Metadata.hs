{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeSynonymInstances  #-}

module Dreamer.Metadata
(
    Metadata(..),
    MetaID,
    QMetaID,
    String255,

    mkstring255,
    mkQMetaID,
    unString255
) where

import           Dreamer.Atmosphere.Quanta

newtype String255 = String255 {unString255 :: String} deriving (Show, Read)

mkstring255 :: String -> String255
mkstring255 s = String255 $ take 255 s

class Metadata m where
    mdid :: m -> MetaID
    friendly :: m -> String255
    author :: m -> String255
    {-# MINIMAL mdid | friendly | author #-}

-- Use the array of values to store different IDs for the same item.
-- For example, an item can have a DB-associated ID and a lab-associate ID.
type MetaID = String
type QMetaID qb = Quantum String qb [Double]

instance (QuantaBounded qb) => Quanta String qb [Double] where
    data Quantum String qb [Double] = QMetaID String qb [Double] deriving (Show, Read)
    qtype (QMetaID s _ _) = s
    qspan (QMetaID _ qb _) = qb
    qval (QMetaID _ _ vs) = vs
    mkQ s qb vs = QMetaID s qb (map (qbound qb) vs)

mkQMetaID :: MetaID -> QMetaID QPosInf
mkQMetaID s = mkQ s QPosInf [0]

instance (QuantaBounded qb) => Eq (QMetaID qb) where
    q1 == q2 = (qtype q1) == (qtype q2)
    q1 /= q2 = (qtype q1) /= (qtype q2)

instance (QuantaBounded qb) => Ord (QMetaID qb) where
    q1 `compare` q2 = (qtype q1) `compare` (qtype q2)
    q1 <= q2 = (qtype q1) <= (qtype q2)
