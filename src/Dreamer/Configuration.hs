module Dreamer.Configuration
(
    Complexity,
    ConfigLog(..),
    DreamerConfig(..),
    DreamerInfo(..),
    FGLComplexity,
    TransmissionMeta(..)
) where

import           Dreamer.Academy
import           Dreamer.Atmosphere.Atom
import           Dreamer.Atmosphere.Molecule
import           Dreamer.Atmosphere.ProboCorpus
import           Dreamer.Internal.Operation
import           Dreamer.Metadata
import           Dreamer.Mufori
import           Dreamer.Neural.Enzyme
import           Dreamer.Neural.NTF
import           Dreamer.Neural.Sense
import           Dreamer.Neural.Signature
import           Dreamer.Neural.Type
import           System.IO                      (FilePath)

data ConfigLog = ConfigIOLogger {unConfigIOLogger :: Logger} | ConfigFileLog {unConfigFileLog :: FilePath} | VoidLog

data DreamerInfo = DreamerInfo {
    drmrinfo'age       :: QAtomPosInf,
    drmrinfo'trueage   :: QAtomPosInf,
    drmrinfo'agegrowth :: Int,
    drmrinfo'birthtime :: Timestamp,
    drmrinfo'accesslog :: ConfigLog,
    drmrinfo'errorlog  :: ConfigLog
}

type Complexity = [(QMetaID QPosInf, QMetaID QPosInf, QAtomPos)]
type FGLComplexity = ([(Int, QMetaID QPosInf)], [(Int, Int, QAtomPos)])

data DreamerConfig itd md = DreamerConfig {
    drmrcfg'senses :: Senses itd md,
    drmrcfg'neuronntfs :: NTFs itd md,
    drmrcfg'axonntfs :: NTFs itd md,

    drmrcfg'aspects :: Aspects itd md, -- ^ only lead aspects
    drmrcfg'invsenses :: [(QMetaID QPosInf, DynSense itd md)],
    drmrcfg'invaspects :: [(QMetaID QPosInf, DynAspect itd md)], -- ^ Includes lead aspects and subaspects
    drmrcfg'invneuronntfs :: [(QMetaID QPosInf, DynNTF itd md)],
    drmrcfg'invaxonntfs :: [(QMetaID QPosInf, DynNTF itd md)],

    drmrcfg'enzymes :: [(QMetaID QPosInf, DynEnzyme (NeuronState itd md))],
    drmrcfg'molpospos :: [(QMetaID QPosInf, MoleculePosInfPos (NeuronState itd md) (DynAspect itd md))],
    drmrcfg'molposneg :: [(QMetaID QPosInf, MoleculePosInfNeg (NeuronState itd md) (DynAspect itd md))],
    drmrcfg'molposfull :: [(QMetaID QPosInf, MoleculePosInfFull (NeuronState itd md) (DynAspect itd md))],
    drmrcfg'probopos :: [(QMetaID QPosInf, ProboCorpusPos (NeuronState itd md))],

    drmrcfg'qaspects :: [QAspect itd md],  -- ^ only lead aspects
    -- ^ can be used as is to run molecules and bonds

    drmrcfg'sensecomplexity :: Complexity,
    drmrcfg'enzymecomplexity :: Complexity,
    drmrcfg'tmuampcomplexity :: Complexity,
    drmrcfg'tfoampcomplexity :: Complexity,
    drmrcfg'triampcomplexity :: Complexity,
    drmrcfg'taspscalecomplexity :: Complexity,
    drmrcfg'temphcomplexity :: Complexity,
    drmrcfg'tdemphcomplexity :: Complexity,
    drmrcfg'tbiascomplexity :: Complexity,
    drmrcfg'trankcomplexity :: Complexity,
    drmrcfg'tcontactlimcomplexity :: Complexity,

    drmrcfg'sensecomplexity'fglrep :: FGLComplexity,
    drmrcfg'enzymecomplexity'fglrep :: FGLComplexity,
    drmrcfg'tmuampcomplexity'fglrep :: FGLComplexity,
    drmrcfg'tfoampcomplexity'fglrep :: FGLComplexity,
    drmrcfg'triampcomplexity'fglrep :: FGLComplexity,
    drmrcfg'taspscalecomplexity'fglrep :: FGLComplexity,
    drmrcfg'temphcomplexity'fglrep :: FGLComplexity,
    drmrcfg'tdemphcomplexity'fglrep :: FGLComplexity,
    drmrcfg'tbiascomplexity'fglrep :: FGLComplexity,
    drmrcfg'trankcomplexity'fglrep :: FGLComplexity,
    drmrcfg'tcontactlimcomplexity'fglrep :: FGLComplexity,

    drmrcfg'adaptinterval :: QAtomPosInf,
    drmrcfg'aginginterval :: QAtomPosInf,
    drmrcfg'agingincrement :: QAtomPosInf,
    -- drmrcfg'maxage :: QAtomPosInf, -- TODO use this to conditionally terminate aging timer and call only simple-adapt
    drmrcfg'coordinateinterval :: QAtomPosInf,

    drmrcfg'drmrinfo :: DreamerInfo,

    drmrcfg'academies     :: [(AcademyID, AcademyConfig itd md)],

    drmrcfg'accesslogger :: Logger,
    drmrcfg'errorlogger :: ErrorLogger
}

data TransmissionMeta itd md = TMeta {
    tmeta'stageindex :: StageIndex,
    tmeta'timestamp :: Timestamp,
    tmeta'maxstages :: MaxStages,
    tmeta'maxcontact :: MaxContact,
    tmeta'aspecttune :: [(DynSense itd md, [(DynAspect itd md, Maybe (Tuning QAtomPos QAtomFull))])],
    tmeta'senseirctune :: Maybe (Tuning QAtomPos QAtomFull),
    tmeta'enzymeirctune :: Maybe (Tuning QAtomPos QAtomFull),
    tmeta'tmuampirctune :: Maybe (Tuning QAtomPos QAtomFull),
    tmeta'tfoampirctune :: Maybe (Tuning QAtomPos QAtomFull),
    tmeta'triampirctune :: Maybe (Tuning QAtomPos QAtomFull),
    tmeta'taspscaleirctune :: Maybe (Tuning QAtomPos QAtomFull),
    tmeta'temphirctune :: Maybe (Tuning QAtomPos QAtomFull),
    tmeta'tdemphirctune :: Maybe (Tuning QAtomPos QAtomFull),
    tmeta'tbiasirctune :: Maybe (Tuning QAtomPos QAtomFull),
    tmeta'trankirctune :: Maybe (Tuning QAtomPos QAtomFull),
    tmeta'tcontactlimirctune :: Maybe (Tuning QAtomPos QAtomFull)
}
