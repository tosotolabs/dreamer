{-# LANGUAGE KindSignatures #-}

module Dreamer.Neural.Type
(
    -- ADAPT
    ADAPTInterval,
    ADAPTRatioNum,
    Age,
    AgingInterval,
    AgingIncrement,
    CoordinateInterval,
    -- Growth,

    -- Identification
    NeuralID(..),

    -- Transmission
    Stimulus,

    ---- Transmission staging
    MaxStages,
    StageIndex,

    ---- Transmission contact Limiting
    Bias,
    ContactAllowanceScore,
    ContactLimit,
    Contribution,
    ContactLimitScale,
    ContactNovelty,
    CorrelationScore,
    MaxContact,
    Rank,

    -- Experience
    Experience,

    -- Meta
    Timestamp,
    mkstring255,
) where

import           Dreamer.Atmosphere.Atom
import           Dreamer.Atmosphere.ProboCorpus
import           Dreamer.Metadata
import           Dreamer.Neural.Sense

data NeuralID = NeuralIDI {unNeuralIDI :: Int} | NeuralIDS {unNeuralIDS :: String} deriving (Eq, Ord, Show)

type ADAPTInterval = QAtomPosInf
type ADAPTRatioNum = QAtomPosInf
type Age = QAtomPosInf
type AgingInterval = QAtomPosInf
type AgingIncrement = QAtomPosInf
type CoordinateInterval = QAtomPosInf
-- type Growth = QAtomPosInf

type MaxStages = QAtomPosInf
type StageIndex = QAtomPosInf

type MaxContact = QAtomPosInf
type ContactLimit = QAtomPosInf
type ContactLimitScale = QProbeRPos
type ContactAllowanceScore = QAtomPos
type ContactNovelty = QAtomFull

type CorrelationScore = QAtomPos
type Contribution = QAtomFull
type Bias = QAtomPos
type Rank = QAtomPos

type Experience = QAtomPos

type Stimulus itd md = (DynAspect itd md, (QAtomFull, [(SubAspect itd md, QAtomFull)]))

type K_ADAPT (t :: ((((* -> *) -> * -> *) -> (* -> *) -> *) -> (((* -> *) -> * -> *) -> (* -> *) -> *) -> ((* -> *) -> * -> *) -> (* -> *) -> *)
                -> (((* -> *) -> * -> *) -> (* -> *) -> *)
                -> (((* -> *) -> * -> *) -> (* -> *) -> *)
                -> ((* -> *) -> * -> *)
                -> (* -> *)
                -> *)
                = t
type K_Cerebrum (c :: (((* -> *) -> * -> *) -> (* -> *) -> *)
                -> (((* -> *) -> * -> *) -> (* -> *) -> *)
                -> ((* -> *) -> * -> *)
                -> (* -> *)
                -> *)
                = c
type K_Structure (s :: ((* -> *) -> * -> *) -> (* -> *) -> *) = s
type K_Neuron n = K_Structure n
type K_Axon a = K_Structure a
