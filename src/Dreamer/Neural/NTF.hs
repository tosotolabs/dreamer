{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE KindSignatures            #-}
{-# LANGUAGE LiberalTypeSynonyms       #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeSynonymInstances      #-}

{- NTF module Dreamer.in the Psychosphere
    This module Dreamer.gets crazy. We just have to make sure to create all the
    exact instances of SigType that we need and, of course, specify the
    exact type of Sig we need at any time. Thankfully, nothing about Sigs
    and SigTypes are alterable by clients.
-}

module Dreamer.Neural.NTF
(
    NTF(..),
    DynNTF(..),
    NTFSig,
    NTFSigs,
    NTFs,
    Auxiliary(..),
    DynAux(..),
    AuxSig,
    AuxSigs,
    AxonState(..),
    NeuronState(..),
    ATA(..),
    PostMorph(..),

    -- Signature
    Signal,
    Signature(..),

    Metadata(..)
) where

import           Dreamer.Atmosphere.Atom
import           Dreamer.Atmosphere.Molecule
import           Dreamer.Metadata
import           Dreamer.Neural.Signature
import           Dreamer.Neural.Type
import           Dreamer.Process

data DynNTF (itd :: (* -> *) -> * -> *) (md :: * -> *) = forall n. (NTF n itd md) => DynNTF (n itd md)
instance Metadata (DynNTF itd md) where
    mdid (DynNTF n) = mdid n
    friendly (DynNTF n) = friendly n
    author (DynNTF n) = author n
instance Eq (DynNTF itd md) where
    (==) (DynNTF a) (DynNTF b) = (mdid a) == (mdid b)
    (/=) (DynNTF a) (DynNTF b) = (mdid a) /= (mdid b)
instance Ord (DynNTF itd md) where
    (DynNTF a) `compare` (DynNTF b) = (mdid a) `compare` (mdid b)
    (DynNTF a) <= (DynNTF b) = (mdid a) <= (mdid b)
instance Show (DynNTF itd md) where
    show (DynNTF n) = "DynNTF " ++ (show n)

data DynAux (itd :: (* -> *) -> * -> *) (md :: * -> *) = forall a. (Auxiliary a itd md) => DynAux (a itd md)
instance Metadata (DynAux itd md) where
    mdid (DynAux a) = mdid a
    friendly (DynAux a) = friendly a
    author (DynAux a) = author a
instance Eq (DynAux itd md) where
    (==) (DynAux a) (DynAux b) = (mdid a) == (mdid b)
    (/=) (DynAux a) (DynAux b) = (mdid a) /= (mdid b)
instance Ord (DynAux itd md) where
    (DynAux a) `compare` (DynAux b) = (mdid a) `compare` (mdid b)
    (DynAux a) <= (DynAux b) = (mdid a) <= (mdid b)
instance Show (DynAux itd md) where
    show (DynAux n) = "DynAux " ++ (show n)

type NTFs itd md = [(DynNTF itd md, [DynAux itd md])]
type NTFSig itd md = (DynNTF itd md, (Signature itd md QAtom3, AuxSigs itd md))
type NTFSigs itd md = [NTFSig itd md]
type AuxSig itd md = (DynAux itd md, Signature itd md QAtom3)
type AuxSigs itd md = [AuxSig itd md]

data ATA itd md = ATA {
    atasenses ::  Signal itd md QAtomPos,
    atantfs   ::  NTFSigs itd md
}

data NeuronState (itd :: (* -> *) -> * -> *) (md :: * -> *) = NeuronState {
    neustateid     :: NeuralID,
    neustatesenses :: Signal itd md QAtomPos,
    neustatentfs   :: NTFSigs itd md,
    neustateata    :: ATA itd md
}

data AxonState (itd :: (* -> *) -> * -> *) (md :: * -> *) = AxonState {
    axonstateid        :: NeuralID,
    axonstatentfs      :: NTFSigs itd md,
    axonstateneusenses :: Signal itd md QAtomPos,
    axonstateneuntfs   :: NTFSigs itd md
}

instance State (AxonState itd md) where
instance State (NeuronState itd md) where

data PostMorph itd md = PostMorph {
    postmorph'prevneustate :: NeuronState itd md,
    postmorph'aspscalemol  :: SMoleculePosInfFull (DynAspect itd md),
    postmorph'muampmol     :: SMoleculePosInfFull (DynAspect itd md),
    postmorph'foampmol     :: SMoleculePosInfFull (DynAspect itd md),
    postmorph'riampmol     :: SMoleculePosInfFull (DynAspect itd md),
    postmorph'contactlim   :: (ContactAllowanceScore, ContactLimitScale)
}

-- We don't want NTFs directly accessing the DB, so we have them
-- generate their sigs and we'll update the axon/neuron with the data.
class (Show (n itd md), Read (n itd md), Metadata (n itd md)) => NTF n (itd :: (* -> *) -> * -> *) (md :: * -> *) where
    {-# MINIMAL ntf'issignal | ntf'neudefsig | ntf'axondefsig | ntf'runneuvisit | ntf'runneupostmorph | ntf'runneupostcontact | ntf'runaxonpostcontact #-}

    data NTFAux n itd md :: *

    ntf'issignal :: n itd md -> Bool

    ntf'neudefsig :: n itd md -> Senses itd md -> Timestamp -> (Signature itd md QAtom3, AuxSigs itd md)
    ntf'axondefsig :: n itd md -> Senses itd md -> Timestamp -> (Signature itd md QAtom3, AuxSigs itd md)

    -- TODO come up with a way to strongly associate regular processes with their process2 counterparts
    -- check out type families and matching on types
    ntf'neudefsig2 :: (IntuitiveProcess2 itd md QAtom3) => n itd md -> Senses itd md -> Timestamp -> (Signature itd md QAtom3, AuxSigs itd md)
    ntf'axondefsig2 :: (IntuitiveProcess2 itd md QAtom3) => n itd md -> Senses itd md -> Timestamp -> (Signature itd md QAtom3, AuxSigs itd md)

    -- TODO pass in the axon(s) that led to this neuron
    ntf'runneuvisit :: n itd md -> NTFSig itd md -> NeuronState itd md -> Timestamp -> NTFSig itd md
    ntf'runneupostmorph :: n itd md -> NTFSig itd md -> PostMorph itd md -> Timestamp -> NTFSig itd md

    -- TODO gather post correlation data for neuronal NTFs
    ntf'runneupostcontact :: n itd md
                            -> NTFSig itd md
                            -> NeuronState itd md
                            -> [AxonState itd md] -- ^ axons the neuron chose
                            -> Timestamp
                            -> NTFSig itd md

    ntf'runaxonpostcontact :: n itd md -- ^ the NTF
                            -> NTFSig itd md
                            -- ^ the NTF's current signature state in the axon
                            -> AxonState itd md -- ^ state of the axon propagated
                            -> (NeuronState itd md, NeuronState itd md)
                            -- ^ states of the initiating and terminating neurons
                            -> (CorrelationScore, [(DynAspect itd md, (Contribution, Bias, Rank))])
                            -- ^ correlation score of the axon and factors used to influence
                            -- correlation-calculation results
                            -> Timestamp -- ^ current timestamp
                            -> NTFSig itd md
                            -- ^ updated NTF signature

    ntf'axondefsig = ntf'neudefsig
    ntf'neudefsig = ntf'axondefsig
    ntf'axondefsig2 = ntf'neudefsig2
    ntf'neudefsig2 = ntf'axondefsig2
    ntf'runneuvisit _ n _ _ = n
    ntf'runneupostmorph _ n _ _ = n
    ntf'runneupostcontact _ n _ _ _ = n
    ntf'runaxonpostcontact _ n _ _ _ _ = n

    -- adaptSig ::

class (Show (a itd md), Read (a itd md), Metadata (a itd md)) => Auxiliary a (itd :: (* -> *) -> * -> *) (md :: * -> *) where
    {-# MINIMAL aux'issignal #-}

    aux'issignal :: a itd md -> Bool
