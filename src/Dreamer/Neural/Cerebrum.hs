{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies          #-}

module Dreamer.Neural.Cerebrum
(
    Cerebrum(..),
    ContactResult(..),
    DynEnzyme(..),
    Enzyme(..),
    EnzymeBreed(..),

    -- Meta
    DreamerConfig(..),
    TransmissionMeta(..),

    module Dreamer.Neural.Cerebrum.Structure.Axon,
    module Dreamer.Neural.Cerebrum.Structure.Neuron,
    module Dreamer.Neural.Type,

    -- State
    AxonState(..),
    NeuronState(..)
) where

import qualified Data.Set                                 as S
import           Dreamer.Academy
import           Dreamer.Atmosphere.Atom
import           Dreamer.Atmosphere.Bond
import           Dreamer.Lab.Type
import           Dreamer.Metadata
import           Dreamer.Neural.Cerebrum.Structure.Axon
import           Dreamer.Neural.Cerebrum.Structure.Neuron
import           Dreamer.Neural.Enzyme
import           Dreamer.Neural.Itinerary
import           Dreamer.Neural.NTF
import           Dreamer.Neural.Sense
import           Dreamer.Neural.Signature
import           Dreamer.Neural.Transmitter
import           Dreamer.Neural.Type
import           Dreamer.Process

class (Neuron n itd md, Axon a itd md) => Cerebrum c n a itd md where
    sensorycontact :: c n a itd md -- ^ the cerebrum
                    -> n itd md -- ^ the neuron making contact
                    -> NeuronState itd md
                    -- ^ static representation of the neuron's current sense and
                    -- ntfs signals
                    -> ContactLimit
                    -- ^ the maximum number of neighbors the neuron can contact
                    -> S.Set NeuralID -- ^ all sensory neuron IDs
                    -> DreamerConfig itd md
                    -> TransmissionMeta itd md
                    -> IO (ContactResult n a itd md)
                    -- ^ the neuron, its chosen and axons and correlation
                    -- factors applied for each aspect

    limitcontacts :: c n a itd md -- ^ the cerebrum
                    -> [(NeuralID, (ContactAllowanceScore, ContactLimitScale))]
                    -- ^ neurons, their allowance scores for transmission, and
                    -- their suggested scale for the maximum number of neighbors
                    -- to be contacted throughout the stage
                    -> (MaxStages, MaxContact)
                    -- ^ max number of stages per transmission and max number of
                    -- neurons to contact per stage
                    -> DreamerConfig itd md
                    -> IO [(NeuralID, ContactLimit)]
                    -- ^ the neurons and the maximum number of neighbors each
                    -- can contact

    contact :: c n a itd md -- ^ the cerebrum
            -> n itd md -- ^ the neuron making contact
            -> NeuronState itd md
            -- ^ static representation of the neuron's current sense and
            -- ntfs signals
            -> ContactLimit
            -- ^ the maximum number of neighbors the neuron can contact
            -> (S.Set NeuralID, S.Set NeuralID)
            -- --^ IDs of all referring axons and all current and previous
            -- stage neuron IDs
            -> [TransmCorr itd md]
            -- ^ transmitters suggesting correlation-calculation
            -> DreamerConfig itd md
            -> TransmissionMeta itd md
            -> IO (ContactResult n a itd md)
            -- ^ the neuron, its chosen and axons and correlation
            -- factors applied for each aspect

    -- | Retrieve a neuron from the database.
    getneuron :: c n a itd md -> NeuralID -> DreamerConfig itd md -> IO (n itd md)

    -- | Retrieve a specific side of an axon, given a start neuron NeuralID.
    getaxonside :: c n a itd md -- ^ the cerebrum
                -> NeuralID -- ^ the axon neural ID
                -> NeuralID -- ^ the start neuron neural ID
                -> DreamerConfig itd md
                -> IO (Maybe (a itd md))

    -- Retrieve a specific side of an axon, given a terminal neuron NeuralID.
    -- Params: Cerebrum, axon NeuralID, terminal neuron NeuralID
    -- getaxonside' :: c n a itd md -> NeuralID -> NeuralID -> DreamerConfig itd md -> IO (Maybe (a itd md))

    -- | Retrieve an axon from the database.
    getaxon :: c n a itd md -> NeuralID -> DreamerConfig itd md -> IO (Maybe (a itd md, a itd md))

    -- | Given a cerebrum, return all sensory neurons and their education (academy and tutor)
    sensoryneurons :: c n a itd md -> DreamerConfig itd md -> IO [(n itd md, Education)]

    refreshneuron :: c n a itd md
                    -> n itd md
                    -> NeuronState itd md
                    -> (STransmMorph itd md, STransmMorph itd md)
                    -> [(CorrelationScore, [(DynAspect itd md, (Contribution, Bias, Rank))])]
                    -> ((ContactAllowanceScore, ContactLimitScale), ContactNovelty)
                    -- ^ Use these to update the Enzyme IRC process. For example, average the novelties
                    -- of the three parts of transmitter using the above results of running using the
                    -- neuron's current transmitter.
                    -> TransmissionMeta itd md
                    -> DreamerConfig itd md
                    -> IO (n itd md)

    -- | Update a neuron in the db.
    updateneuron :: c n a itd md -> n itd md -> NeuronState itd md -> DreamerConfig itd md -> IO ()

    -- | Update an axon in the db.
    updateaxon :: c n a itd md -> a itd md -> AxonState itd md -> DreamerConfig itd md -> IO ()

    -- | Create a neuron and save it in the db.
    createneuron :: c n a itd md -> n itd md -> DreamerConfig itd md -> IO (n itd md)

    -- Create an axon between two neurons.
    -- link :: c n a itd md -> n itd md -> n itd md -> DreamerConfig itd md -> IO ((n itd md, n itd md), a itd md)

    -- | Removes the axon between two neurons.
    -- unlink :: c n a itd md -> a itd md -> IO ()

    neurongrowth :: c n a itd md -> Age -> QAtomPosInf
    axondecrease :: c n a itd md -> Age -> (QAtomPosInf, QAtomPosInf)

    {-# MINIMAL limitcontacts | contact | sensorycontact | getneuron | getaxonside | sensoryneurons | refreshneuron | updateneuron | updateaxon | neurongrowth | axondecrease #-}

data ContactResult n a itd md = ContactResult {
    contactres'neuron :: n itd md,
    -- ^ the neuron that made contact
    contactres'axons :: [(a itd md, (CorrelationScore, [(DynAspect itd md, (Contribution, Bias, Rank))]))],
    -- ^ axons chosen by the neuron, its correlation score, the contribution,
    -- bias, and rank used for each aspect when calculating correlation
    -- between the neuron's sense signal and the axon's average sense
    -- signal of its two neurons i.e. from the perspective of the neuron
    contactres'transmitter :: Transmitter itd md -- ^ the contacting neuron's transmitter
}

instance (Neuron n itd md, Axon a itd md) => Show (ContactResult n a itd md) where
    show (ContactResult n axs t) = (show $ uniqueid n)++" | "++(show $ map (\(a,c) -> (uniqueid a, c)) axs)++" | "++(show t)
