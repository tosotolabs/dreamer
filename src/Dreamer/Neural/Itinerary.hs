{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Dreamer.Neural.Itinerary
(
    EnzymeItinerary(..),
) where

import qualified Data.Graph.Inductive.PatriciaTree as G
import           Dreamer.Atmosphere.Atom
import           Dreamer.Atmosphere.Bond
import           Dreamer.Metadata

-- TODO use actual item in place of QMetaID
data EnzymeItinerary = EnzymeItinerary {
    -- TODO use TemplateHaskell to generate allowed aspects and NTFs and pass those in as concrete types
    enzymeitinerary'aspntf :: G.Gr (QMetaID QPosInf, G.Gr (QMetaID QPosInf) QBondSPos) QBondSPos,
    enzymeitinerary'ntfasp :: G.Gr (QMetaID QPosInf, G.Gr (QMetaID QPosInf) QBondSPos) QBondSPos,
    enzymeitinerary'vfreq  :: QAtomPos
    -- ^ influence choosing enzymes based on how often an enzyme
    -- is chosen
}

instance (QuantaBounded sp) => Eq (QBondS sp) where
    qb1 == qb2 = (qval qb1) == (qval qb2)
    qb1 /= qb2 = (qval qb1) /= (qval qb2)

instance (QuantaBounded sp) => Ord (QBondS sp) where
    qb1 `compare` qb2 = (qval qb1) `compare` (qval qb2)
    qb1 <= qb2 = (qval qb1) <= (qval qb2)
