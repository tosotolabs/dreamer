{-# LANGUAGE MultiParamTypeClasses #-}

module Dreamer.Neural.ADAPT
(
    ADAPT(..)
) where

import Dreamer.Neural.Cerebrum
import Dreamer.Neural.Type

-- Aspect Diversity and Prriority Training (ADAPT) for/in the Genesis, Apoptosis, (info)Morphosis, Environment (GAME)
-- Synaptogenesis, neurogenesis, adjusting relationships, staging (increasing structure number, limiting)
-- Recalculate signal of axons periodically.

class (Cerebrum c n a itd md) => ADAPT s c n a itd md where
    coordinate :: s c n a itd md -> c n a itd md -> IO ()
    adapt :: s c n a itd md -> c n a itd md -> Maybe Age -> IO ()
