{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Dreamer.Neural.Cerebrum.Structure
(
    Structure(..),

    AxonState(..),
    NeuronState(..),

    -- Neural types
    NeuralID(..),
) where

import Dreamer.Atmosphere.Atom
import Dreamer.Neural.NTF
import Dreamer.Neural.Sense
import Dreamer.Neural.Signature
import Dreamer.Neural.Type
import Dreamer.Process

class (IntuitiveProcess2 itd md QAtomPos, IntuitiveProcess2 itd md QAtom3, Eq (s itd md)) => Structure s itd md where
    uniqueid :: s itd md -> NeuralID
    senses :: s itd md -> Signal itd md QAtomPos
    ntfs :: s itd md -> NTFSigs itd md
    experience :: s itd md -> Experience
    {-# MINIMAL uniqueid | senses | ntfs #-}
