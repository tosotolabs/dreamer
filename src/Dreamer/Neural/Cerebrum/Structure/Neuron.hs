{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Dreamer.Neural.Cerebrum.Structure.Neuron
(
    Neuron(..),
    ATA(..),
    MorphResult(..),
    ReferringAxon,

    -- Structure
    NeuralID(..),
    Structure(..),
    Experience,

    -- Meta
    DreamerConfig(..),
    TransmissionMeta(..),
    Metadata(..),
    Timestamp,
    mkstring255,
) where

import           Dreamer.Atmosphere.Atom
import           Dreamer.Atmosphere.ProboCorpus
import           Dreamer.Configuration
import           Dreamer.Lab.Type
import           Dreamer.Metadata
import           Dreamer.Mufori
import           Dreamer.Neural.Cerebrum.Structure
import           Dreamer.Neural.Enzyme
import           Dreamer.Neural.NTF
import           Dreamer.Neural.Sense
import           Dreamer.Neural.Signature
import           Dreamer.Neural.Transmitter
import           Dreamer.Neural.Type
import           Dreamer.Process

class (Structure n itd md, Metadata (n itd md)) => Neuron n itd md where
    sensorymorph :: n itd md -- ^ the sensory neuron
                    -> NeuronState itd md
                    -- ^ state of the sensory neuron; this is likely more updated than the neuron itself
                    -> [Stimulus itd md]
                    -> DreamerConfig itd md -- ^ the dreamer's configurations
                    -> TransmissionMeta itd md -- ^ info on the current transmission state
                    -> MorphResult n itd md
                    -- ^ the neuron, resolved transmitters likely combined with received transmitters in some manner
                    -- and the associated bias molecule, a score to assist in determining a maximum-
                    -- contact amount for the neuron and the neuron's suggestion for the maximum number
                    -- of neighbors to be contacted throughout the stage

    morph :: n itd md -- ^ the neuron
            -> NeuronState itd md
            -- ^ state of the neuron; this is likely more updated than the neuron itself
            -> [TransmMorph itd md]
            -- ^ infomorphosis-influencing transmitters sent by previous-stage callers
            -> ([STransmMorph itd md], [STransmMorph itd md])
            -- ^ resolved transmission messages from callers and associated bias simple molecules
            -> [TransmContactLim itd md]
            -- ^ probo corpi to be used on the neuron to determine the amount to scale ([0,1])
            -> [ReferringAxon itd md]
            -- ^ dynamic axon states belonging to the axons that led to the neuron containing the
            -- sense and NTF averaged states
            -- of its two neurons
            -> DreamerConfig itd md -- ^ the dreamer's configurations
            -> TransmissionMeta itd md -- ^ info on the current transmission state
            -> MorphResult n itd md
            -- ^ the neuron, resolved transmitters likely combined with received transmitters in some manner
            -- and the associated bias molecule, a score to assist in determining a maximum-
            -- contact amount for the neuron and the neuron's suggestion for the maximum number
            -- of neighbors to be contacted throughout the stage

    modmorph :: n itd md -- ^ the neuron
            -> NeuronState itd md
            -- ^ state of the neuron; this is likely more updated than the neuron itself
            -> TransmMorph itd md
            -- ^ infomorphosis-influencing transmitters sent by previous-stage callers
            -> TransmContactLim itd md
            -- ^ probo corpi to be used on the neuron to determine the amount to scale ([0,1])
            -> DreamerConfig itd md -- ^ the dreamer's configurations
            -> TransmissionMeta itd md -- ^ info on the current transmission state
            -> MorphResult n itd md
            -- ^ the neuron, resolved transmitters likely combined with received transmitters in some manner
            -- and the associated bias molecule, a score to assist in determining a maximum-
            -- contact amount for the neuron and the neuron's suggestion for the maximum number
            -- of neighbors to be contacted throughout the stage

    -- Active Transmission Average
    activetransmavg ::  n itd md -> ATA itd md

    -- Message received from contacting neighbor
    -- recv :: (Recv r) => n -> r

    transmitter ::  n itd md -> Transmitter itd md

    enzyme :: n itd md -> DynEnzyme (NeuronState itd md)

    contactlim ::  n itd md -> CoreProcess QProbeRPos

    -- Defines how the neuron asks for stimulus from receptors in receptive field.
    -- A neuron can be connected to multiple receptors. Each receptor provides a
    -- list of aspect values for aspects of only one sense. A neuron can be connected
    -- to sensors of different kinds.
    -- stimulate ::  n itd md -> Senses -> IO (PartialSignal itd md QAtomPos)

    senseirc :: n itd md -> IRCProcess (QMetaID QPosInf) itd md QAtomPos
    enzymeirc :: n itd md -> IRCProcess (QMetaID QPosInf) itd md QAtomPos
    tmuampirc :: n itd md -> IRCProcess (QMetaID QPosInf) itd md QAtomPos
    tfoampirc :: n itd md -> IRCProcess (QMetaID QPosInf) itd md QAtomPos
    triampirc :: n itd md -> IRCProcess (QMetaID QPosInf) itd md QAtomPos
    taspscaleirc :: n itd md -> IRCProcess (QMetaID QPosInf) itd md QAtomPos
    temphirc :: n itd md -> IRCProcess (QMetaID QPosInf) itd md QAtomPos
    tdemphirc :: n itd md -> IRCProcess (QMetaID QPosInf) itd md QAtomPos
    tbiasirc :: n itd md -> IRCProcess (QMetaID QPosInf) itd md QAtomPos
    trankirc :: n itd md -> IRCProcess (QMetaID QPosInf) itd md QAtomPos
    tcontactlimirc :: n itd md -> IRCProcess (QMetaID QPosInf) itd md QAtomPos

    aspecttune :: n itd md -> [(DynSense itd md, [(DynAspect itd md, Maybe RawMod)])]

    senseirctune :: n itd md -> Maybe RawMod

    enzymeirctune :: n itd md -> Maybe RawMod

    tmuampirctune :: n itd md -> Maybe RawMod
    tfoampirctune :: n itd md -> Maybe RawMod
    triampirctune :: n itd md -> Maybe RawMod
    taspscaleirctune :: n itd md -> Maybe RawMod
    temphirctune :: n itd md -> Maybe RawMod
    tdemphirctune :: n itd md -> Maybe RawMod
    tbiasirctune :: n itd md -> Maybe RawMod
    trankirctune :: n itd md -> Maybe RawMod
    tcontactlimirctune :: n itd md -> Maybe RawMod

    {-# MINIMAL sensorymorph | morph | modmorph | aspecttune | senseirctune | enzymeirctune | tmuampirctune | tfoampirctune | triampirctune | taspscaleirctune | temphirctune | tdemphirctune | tbiasirctune | trankirctune | tcontactlimirctune #-}

data MorphResult n (itd :: (* -> *) -> * -> *) (md :: * -> *) = MorphResult {
        morphres'neuron :: n itd md, -- ^ the neuron
        morphres'simpletransms :: (STransmMorph itd md, STransmMorph itd md),
        -- ^ resolved transmitters likely combined with received transmitters in some manner
        -- and the associated bias molecule, a score to assist in determining a maximum-contact
        -- amount for the neuron and the neuron's suggestion for the maximum number of neighbors
        -- to be contacted throughout the stage
        morphres'contactlim :: ((ContactAllowanceScore, ContactLimitScale), ContactNovelty)
        -- ^ score dictating the neuron's freedom to contact, the neuron's suggested
        -- scale of the maximum neighbors to contact for the next stage, and novelty
        -- from updating the neuron's contact-limiting process
}

instance (Neuron n itd md) => Show (MorphResult n itd md) where
    show (MorphResult n stransms cl) = (show $ uniqueid n)++" | "++(show stransms)++" | "++(show cl)

type ReferringAxon itd md = AxonState itd md
