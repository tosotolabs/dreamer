{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Dreamer.Neural.Cerebrum.Structure.Axon
(
    Axon(..),

    -- Structure
    Structure(..),
    NeuralID(..),
    Metadata(..),
    mkstring255
) where

import Dreamer.Atmosphere.Atom
import Dreamer.Metadata
import Dreamer.Neural.Cerebrum.Structure
import Dreamer.Neural.NTF
import Dreamer.Neural.Sense

class (Structure a itd md, Metadata (a itd md)) => Axon a itd md where
    snid :: a itd md -> NeuralID
    enid :: a itd md -> NeuralID
    neusenses :: a itd md -> Signal itd md QAtomPos
    neuntfs :: a itd md -> NTFSigs itd md
    {-# MINIMAL snid | enid | neusenses | neuntfs #-}
