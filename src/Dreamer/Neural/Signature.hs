{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeSynonymInstances  #-}

module Dreamer.Neural.Signature
(
    PartialSignal,
    Signal,
    Signature(..),
    Signature2(..),
    State(..),

    DynSense(..),
    DynAspect(..),
    Senses
) where

import           Dreamer.Neural.Sense

class State s where

type PartialSignal d (md :: * -> *) v = (DynSense d md, [(DynAspect d md, d md v)])
type Signal d (md :: * -> *) v = [(DynSense d md, [(DynAspect d md, d md v)])]
data Signature d (md :: * -> *) v = Single {unSingle :: d md v} | Signal {unSignal :: Signal d md v} deriving Show
data Signature2 d (md :: * -> *) v = Single2 {unSingle2 :: d md v} | Signal2 {unSignal2 :: Signal d md v} deriving Show
