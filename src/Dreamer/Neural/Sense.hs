{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE KindSignatures            #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeSynonymInstances      #-}

module Dreamer.Neural.Sense
(
    Sense(..),
    Aspect(..),
    DynSense(..),
    DynAspect(..),
    Senses,
    SubAspect,
    Aspects,
    QSense,
    QAspect,
    AspectID,
    SenseID,

    mkQSense,
    mkQAspect,

    Metadata(..)
) where

import           Dreamer.Atmosphere.Atom
import           Dreamer.Metadata
import           Dreamer.Process

type SenseID = String
type AspectID = String

class (Show (s itd md), Read (s itd md), Metadata (s itd md)) => Sense s (itd :: (* -> *) -> * -> *) (md :: * -> *) where
    data SenseAspect s itd md :: *
    senseid :: s itd md -> QAtomPosInf

class (Show (a itd md), Read (a itd md), Metadata (a itd md)) => Aspect a (itd :: (* -> *) -> * -> *) (md :: * -> *) where
    aspectid :: a itd md -> QAtomPosInf
    aspectdefproc :: (IntuitiveProcess itd md QAtomPos) => a itd md -> QAtomPos -> Timestamp -> itd md QAtomPos
    aspectdefproc2 :: (IntuitiveProcess2 itd md QAtomPos) => a itd md -> QAtomPos -> Timestamp -> itd md QAtomPos

data DynSense itd md = forall s. (Sense s itd md) => DynSense {unDynSense :: s itd md}
instance Eq (DynSense itd md) where
    (==) (DynSense a) (DynSense b) = (mdid a) == (mdid b)
    (/=) (DynSense a) (DynSense b) = (mdid a) /= (mdid b)
instance Ord (DynSense itd md) where
    (DynSense a) `compare` (DynSense b) = (senseid a) `compare` (senseid b)
    (DynSense a) <= (DynSense b) = (senseid a) <= (senseid b)
instance Show (DynSense itd md) where
    show (DynSense s) = "DynSense " ++ (show s)

type QSense itd md = Quantum (DynSense itd md) QPosInf Double

instance (QuantaBounded b, Num c, Fractional c, Ord c) => Quanta (DynSense itd md) b c where
    data Quantum (DynSense itd md) b c = QSense (DynSense itd md) b c
    mkQ d b c = QSense d b (qbound b c)
    qtype (QSense d _ _) = d
    qspan (QSense _ b _) = b
    qval (QSense _ _ c) = c

mkQSense :: DynSense itd md -> QSense itd md
mkQSense (DynSense s) = mkQ (DynSense s) QPosInf (qval $ senseid s)

instance Eq (QSense itd md) where
    qs1 == qs2 = (qtype qs1) == (qtype qs2)
    qs1 /= qs2 = (qtype qs1) /= (qtype qs2)

type SubAspect itd md = DynAspect itd md
data DynAspect (itd :: (* -> *) -> * -> *) (md :: * -> *) = forall a. (Aspect a itd md) => DynAspect {unDynAspect :: a itd md}

instance Eq (DynAspect itd md) where
    (==) (DynAspect a) (DynAspect b) = (mdid a) == (mdid b)
    (/=) (DynAspect a) (DynAspect b) = (mdid a) /= (mdid b)
instance Ord (DynAspect itd md) where
    (DynAspect a) `compare` (DynAspect b) = (aspectid a) `compare` (aspectid b)
    (DynAspect a) <= (DynAspect b) = (aspectid a) <= (aspectid b)
instance Show (DynAspect itd md) where
    show (DynAspect a) = "DynAspect " ++ (show a)

type QAspect itd md = Quantum (DynAspect itd md) QPosInf Double

instance (QuantaBounded b, Num c, Fractional c, Ord c) => Quanta (DynAspect itd md) b c where
    data Quantum (DynAspect itd md) b c = QAspect (DynAspect itd md) b c deriving (Show)
    mkQ d b c = QAspect d b (qbound b c)
    qtype (QAspect d _ _) = d
    qspan (QAspect _ b _) = b
    qval (QAspect _ _ c) = c

mkQAspect :: DynAspect itd md -> QAspect itd md
mkQAspect (DynAspect d) = mkQ (DynAspect d) QPosInf (qval $ aspectid d)

instance Eq (QAspect itd md) where
    qa1 == qa2 = (qtype qa1) == (qtype qa2)
    qa1 /= qa2 = (qtype qa1) /= (qtype qa2)

type Senses itd md = [(DynSense itd md, Aspects itd md)]
type Aspects itd md = [DynAspect itd md]
