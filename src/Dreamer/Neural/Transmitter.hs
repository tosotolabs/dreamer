{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes       #-}

module Dreamer.Neural.Transmitter
(
    Transmitter(..),
    TransmMorph(..),
    TransmCorr(..),
    TransmContr(..),
    TransmEsteem(..),
    TransmMFRAmp(..),
    TransmAsp(..),
    TransmContactLim(..),
    STransmMorph(..),
    STransmMFRAmp(..),
    STransmAsp(..),
) where

import           Dreamer.Atmosphere.Molecule
import           Dreamer.Atmosphere.ProboCorpus
import           Dreamer.Neural.Cerebrum.Structure
import           Dreamer.Neural.Sense
import           Dreamer.Process

data Transmitter itd md = Transmitter {
    tmorph      :: TransmMorph itd md,
    tcorr       :: TransmCorr itd md,
    tcontactlim :: TransmContactLim itd md
}

-- Infomorphosis
-- These molecules have aspect-number atoms (Positive atoms).
data TransmMorph itd md = TMorph {
    tmframp   :: TransmMFRAmp itd md,
    taspscale :: TransmAsp itd md
}
data STransmMorph itd md = STMorph {
    stmframp   :: STransmMFRAmp itd md,
    staspscale :: STransmAsp itd md
} deriving (Show)
-- Transmitter Mufori amplification
data TransmMFRAmp itd md = TMFRAmp {
    tmuamp :: MoleculePosInfFull (NeuronState itd md) (DynAspect itd md),
    tfoamp :: MoleculePosInfFull (NeuronState itd md) (DynAspect itd md),
    triamp :: MoleculePosInfFull (NeuronState itd md) (DynAspect itd md)
}
data STransmMFRAmp itd md = STMFRAmp {
    stmuamp :: SMoleculePosInfFull (DynAspect itd md),
    stfoamp :: SMoleculePosInfFull (DynAspect itd md),
    striamp :: SMoleculePosInfFull (DynAspect itd md)
} deriving (Show)
-- Aspect-change suggestions
type TransmAsp itd md = MoleculePosInfFull (NeuronState itd md) (DynAspect itd md)
type STransmAsp itd md = SMoleculePosInfFull (DynAspect itd md)

-- Correlation
-- These molecules have aspect-number atoms (Positive atoms).
data TransmCorr itd md = TCorr {
    tcontr  :: TransmContr itd md,
    testeem :: TransmEsteem itd md
}
-- Transmitter Contribution part
data TransmContr itd md = TContr {
    temph  :: MoleculePosInfPos (NeuronState itd md) (DynAspect itd md),
    tdemph :: MoleculePosInfNeg (NeuronState itd md) (DynAspect itd md)
}
-- Transmitter Esteem part
data TransmEsteem itd md = TEsteem {
    tbias :: MoleculePosInfPos (NeuronState itd md) (DynAspect itd md),
    trank :: MoleculePosInfPos (NeuronState itd md) (DynAspect itd md)
}
-- Stage-limit suggestion
data TransmContactLim itd md = TContact {unTContact :: ProboCorpusPos (NeuronState itd md)}

instance Show (Transmitter itd md) where
    show (Transmitter morph corr cl) = "[Transmitter] "++(show morph)++" | "++(show corr)++" | "++(show cl)
instance Show (TransmMorph itd md) where
    show (TMorph mfr aspscale) = (show mfr)++" | [TransmAsp] "++(mdid aspscale)
instance Show (TransmMFRAmp itd md) where
    show t = "[TransmMFRAmp] "++(mdid $ tmuamp t)++" | "++(mdid $ tfoamp t)++" | "++(mdid $ triamp t)
instance Show (TransmCorr itd md) where
    show (TCorr contr esteem) = "[TransmCorr] "++(show contr)++" | "++(show esteem)
instance Show (TransmContr itd md) where
    show t = "[TransmContr] "++(mdid $ temph t)++" | "++(mdid $ tdemph t)
instance Show (TransmEsteem itd md) where
    show t = "[TransmEsteem] "++(mdid $ tbias t)++" | "++(mdid $ trank t)
instance Show (TransmContactLim itd md) where
    show (TContact p) = "[TranmsContactLim] "++(mdid p)
