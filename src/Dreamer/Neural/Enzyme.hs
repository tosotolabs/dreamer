{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE TypeFamilies              #-}

module Dreamer.Neural.Enzyme
(
    DynEnzyme (..),
    Enzyme (..),
    EnzymeBreed(..)
) where

import           Dreamer.Metadata
import           Dreamer.Neural.Itinerary

class (Metadata e) => Enzyme e st where
    data EnzymeBreed e st :: *
    runenzyme :: EnzymeBreed e st -> st -> EnzymeItinerary

data DynEnzyme st = forall e. (Enzyme e st, Metadata (EnzymeBreed e st) ) => DynEnzyme (EnzymeBreed e st)

instance Metadata (DynEnzyme st) where
    mdid (DynEnzyme e) = mdid e
    friendly (DynEnzyme e) = friendly e
    author (DynEnzyme e) = author e
