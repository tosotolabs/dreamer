module Dreamer.Atmosphere.Limit
(
    qcomboelemlim,
    qfba3depthlim,
    qfba3stoplim,
    qprobelim,
    qthemin
) where

import Dreamer.Atmosphere.Atom

qcomboelemlim :: QAtomPosInf
qcomboelemlim = mkQAtomPosInf 10

qfba3depthlim :: QAtomPosInf
qfba3depthlim = mkQAtomPosInf 10

qfba3stoplim :: QAtomPosInf
qfba3stoplim = mkQAtomPosInf 0.00005

qprobelim :: QAtomPosInf
qprobelim = mkQAtomPosInf 5

qthemin :: QAtomPosInf
qthemin = mkQAtomPosInf 0.00000000000000000001
