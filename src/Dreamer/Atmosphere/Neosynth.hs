{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies          #-}

module Dreamer.Atmosphere.Neosynth
(
    Neosynth(..),

    QNsynth,
    QNsynthPos,
    QNsynthNeg,
    QNsynthFull,
    QNsynthInf,
    QNsynthPosInf,
    QNsynthNegInf,

    mkQNsynth,
    mkQNsynthFull,
    mkQNsynthPos,
    mkQNsynthNeg,
    mkQNsynthInf,
    mkQNsynthPosInf,
    mkQNsynthNegInf,

    applySynth
) where

import           Dreamer.Atmosphere.Quanta

data Neosynth = Neosynth deriving (Show, Read)

instance (QuantaBounded b, Num v, Fractional v, Ord v) => Quanta Neosynth b v where
    data Quantum Neosynth b v = MkQSynth b v deriving (Show, Read)
    mkQ _ b v = MkQSynth b (qbound b v)
    qtype _ = Neosynth
    qspan (MkQSynth b _) = b
    qval (MkQSynth _ v) = v

type QNsynth a = Quantum Neosynth a Double
type QNsynthPos = QNsynth QPos
type QNsynthNeg = QNsynth QNeg
type QNsynthFull = QNsynth QFull
type QNsynthInf = QNsynth QInf
type QNsynthPosInf = QNsynth QPosInf
type QNsynthNegInf = QNsynth QNegInf

mkQNsynth :: (QuantaBounded sp) => sp -> Double -> QNsynth sp
mkQNsynth sp v = mkQ Neosynth sp v

mkQNsynthFull :: Double -> QNsynthFull
mkQNsynthFull = mkQNsynth QFull

mkQNsynthPos :: Double -> QNsynthPos
mkQNsynthPos = mkQNsynth QPos

mkQNsynthNeg :: Double -> QNsynthNeg
mkQNsynthNeg = mkQNsynth QNeg

mkQNsynthInf :: Double -> QNsynthInf
mkQNsynthInf = mkQNsynth QInf

mkQNsynthPosInf :: Double -> QNsynth QPosInf
mkQNsynthPosInf = mkQNsynth QPosInf

mkQNsynthNegInf :: Double -> QNsynth QNegInf
mkQNsynthNegInf = mkQNsynth QNegInf

instance Num (Quantum Neosynth QFull Double) where
    a + b = mkQ Neosynth QFull $ (qval a) + (qval b)
    a - b = mkQ Neosynth QFull $ (qval a) - (qval b)
    a * b = mkQ Neosynth QFull $ (qval a) * (qval b)
    abs = (mkQ Neosynth QFull) . abs . qval
    negate = (mkQ Neosynth QFull) . negate . qval
    signum = (mkQ Neosynth QFull) . signum . qval
    fromInteger = (mkQ Neosynth QFull) . fromInteger

instance Num (Quantum Neosynth QPos Double) where
    a + b = mkQ Neosynth QPos $ (qval a) + (qval b)
    a - b = mkQ Neosynth QPos $ (qval a) - (qval b)
    a * b = mkQ Neosynth QPos $ (qval a) * (qval b)
    abs = (mkQ Neosynth QPos) . abs . qval
    negate = (mkQ Neosynth QPos) . negate . qval
    signum = (mkQ Neosynth QPos) . signum . qval
    fromInteger = (mkQ Neosynth QPos) . fromInteger

instance Num (Quantum Neosynth QNeg Double) where
    a + b = mkQ Neosynth QNeg $ (qval a) + (qval b)
    a - b = mkQ Neosynth QNeg $ (qval a) - (qval b)
    a * b = mkQ Neosynth QNeg $ (qval a) * (qval b)
    abs a = mkQ Neosynth QNeg 0.0
    negate = (mkQ Neosynth QNeg) . negate . qval
    signum = (mkQ Neosynth QNeg) . signum . qval
    fromInteger = (mkQ Neosynth QNeg) . fromInteger

instance Fractional (Quantum Neosynth QFull Double) where
    a/b = mkQ Neosynth QFull $ (qval a)/(qval b)
    fromRational = (mkQ Neosynth QFull) . fromRational

instance Fractional (Quantum Neosynth QPos Double) where
    a/b = mkQ Neosynth QPos $ (qval a)/(qval b)
    fromRational = (mkQ Neosynth QPos) . fromRational

instance Fractional (Quantum Neosynth QNeg Double) where
    a/b = mkQ Neosynth QNeg $ (qval a)/(qval b)
    fromRational = (mkQ Neosynth QNeg) . fromRational

applySynth :: (Quanta a b c, Quanta Neosynth d c, Num c) => Quantum a b c -> Quantum Neosynth d c -> Quantum a b c
applySynth a b = mkQuantum (qtype a) (qspan a) (qval a * (1 + (qval b)))
