{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE LiberalTypeSynonyms       #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE TypeFamilies              #-}

module Dreamer.Atmosphere.Molecule
(
    DynMolecule(..),
    GenericMolecule,
    MolecularStructure,
    Molecule(..),
    MoleculeBreed(..),
    MoleculePosInfFull,
    MoleculePosInfNeg,
    MoleculePosInfPos,
    SimpleMolecule,
    SMoleculePosInfFull,
    SMoleculePosInfPos,

    Metadata(..),

    module Dreamer.Atmosphere.Quanta
) where

import           Data.Graph.Inductive.PatriciaTree
import           Dreamer.Atmosphere.Atom
import           Dreamer.Atmosphere.Bond
import           Dreamer.Atmosphere.Neosynth
import           Dreamer.Atmosphere.Quanta
import           Dreamer.Metadata
import           Dreamer.Neural.Signature
import           Dreamer.Process

{- A molecule can have different kinds of bonds, but all bonds in a single molecule
    must have the same kind of bond strength.
-}
{- We allow different breeds of a molecule to be specified. We constrain the identifiers
    for the breeds to the exact instance type (e.g. Molecule M'WRK33 Atom QFull BondStrength QPos Double)
    by using type families.
-}
class (Metadata m, State st, Quanta a asp v, Bond b st bsp v) => Molecule m st a asp b bsp v where
    data MoleculeBreed m st a asp b bsp v :: *
    runmolecule :: MoleculeBreed m st a asp b bsp v -> [Quantum a asp v] -> st -> MolecularStructure st a asp b bsp v

data DynMolecule st a asp bsp v = forall m b. (Molecule m st a asp b bsp v, Metadata (MoleculeBreed m st a asp b bsp v) ) => DynMolecule (MoleculeBreed m st a asp b bsp v)
type MolecularStructure st a asp b bsp v = Gr (Quantum a asp v) (BondBreed b st bsp v)

type MoleculePosInfPos st a = DynMolecule st a QPosInf QPos Double
type MoleculePosInfNeg st a = DynMolecule st a QPosInf QNeg Double
type MoleculePosInfFull st a = DynMolecule st a QPosInf QFull Double

type GenericMolecule a asp bsp v = Gr (Quantum a asp v) (Quantum BondStrength bsp v)
type SimpleMolecule a asp bsp = GenericMolecule a asp bsp Double
type SMoleculePosInfPos a = SimpleMolecule a QPosInf QPos
type SMoleculePosInfFull a = SimpleMolecule a QPosInf QFull

instance Metadata (DynMolecule st a asp bsp v) where
    mdid (DynMolecule m) = mdid m
    friendly (DynMolecule m) = friendly m
    author (DynMolecule m) = author m

instance Show (DynMolecule st a asp bsp v) where
    show = mdid
