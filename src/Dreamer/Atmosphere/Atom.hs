{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies          #-}

module Dreamer.Atmosphere.Atom
(
    Atom(..),
    QAtom,
    QAtomPos,
    QAtomNeg,
    QAtomFull,
    QAtomInf,
    QAtomPosInf,
    QAtomNegInf,
    QAtom3,

    mkQAtom,
    mkQAtomFull,
    mkQAtomPos,
    mkQAtomNeg,
    mkQAtomInf,
    mkQAtomPosInf,
    mkQAtomNegInf,
    mkQAtom3,
    mkQAtom3Pos,
    mkQAtom3Neg,
    mkQAtom3Full,
    zeroAtomPos,
    zeroAtomNeg,
    zeroAtomFull,
    zeroAtomInf,
    zeroAtomPosInf,
    zeroAtomNegInf,
    zeroAtom3Pos,
    zeroAtom3Neg,
    zeroAtom3Full,
    unitAtomPos,
    unitAtomFull,
    unitAtomInf,
    unitAtomPosInf,
    unitAtom3Pos,
    unitAtom3Full,
    minAtomPos,
    minAtomFull,
    minAtomInf,
    minAtomPosInf,
    minAtom3Pos,
    minAtom3Full,

    Timestamp,

    module Dreamer.Atmosphere.Quanta
) where

import           Dreamer.Atmosphere.Quanta
import           Dreamer.Internal.Limit

data Atom = Atom deriving (Show, Read)

instance (QuantaBounded b, Num v, Fractional v, Ord v) => Quanta Atom b v where
    data Quantum Atom b v = MkQAtom b v deriving (Show, Read)
    mkQ _ b v = MkQAtom b (qbound b v)
    qtype _ = Atom
    qspan (MkQAtom b _) = b
    qval (MkQAtom _ v) = v

instance (QuantaBounded b) => Eq (Quantum Atom b Double) where
    a == b = (qval a) == (qval b)
    a /= b = (qval a) /= (qval b)

instance (QuantaBounded b) => Ord (Quantum Atom b Double) where
    a `compare` b = (qval a) `compare` (qval b)
    a <= b = (qval a) <= (qval b)

-- instance Quanta Atom Q3 Double where
--     data Quantum Atom Q3 Double = MkQAtom Q3 Double deriving (Show, Read)
--     mkQ _ = MkQAtom
--     qtype _ = Atom
--     qspan (MkQAtom b _) = b
--     qval (MkQAtom _ v) = v

type QAtom a = Quantum Atom a Double
type QAtomPos = QAtom QPos
type QAtomNeg = QAtom QNeg
type QAtomFull = QAtom QFull
type QAtomInf = QAtom QInf
type QAtomPosInf = QAtom QPosInf
type QAtomNegInf = QAtom QNegInf
type QAtom3 = QAtom Q3

mkQAtom :: (QuantaBounded sp) => sp -> Double -> QAtom sp
mkQAtom sp v = mkQ Atom sp v

mkQAtomFull :: Double -> QAtomFull
mkQAtomFull = mkQAtom QFull

mkQAtomPos :: Double -> QAtomPos
mkQAtomPos = mkQAtom QPos

mkQAtomNeg :: Double -> QAtomNeg
mkQAtomNeg = mkQAtom QNeg

mkQAtomInf :: Double -> QAtomInf
mkQAtomInf = mkQAtom QInf

mkQAtomPosInf :: Double -> QAtomPosInf
mkQAtomPosInf = mkQAtom QPosInf

mkQAtomNegInf :: Double -> QAtomNegInf
mkQAtomNegInf = mkQAtom QNegInf

mkQAtom3 :: Q3 -> Double -> QAtom3
mkQAtom3 sp = mkQAtom sp

mkQAtom3Pos :: Double -> QAtom3
mkQAtom3Pos = mkQAtom Q3Pos

mkQAtom3Neg :: Double -> QAtom3
mkQAtom3Neg = mkQAtom Q3Neg

mkQAtom3Full :: Double -> QAtom3
mkQAtom3Full = mkQAtom Q3Full

zeroAtomPos     = mkQAtomPos 0
zeroAtomNeg     = mkQAtomNeg 0
zeroAtomFull    = mkQAtomFull 0
zeroAtomInf     = mkQAtomInf 0
zeroAtomPosInf  = mkQAtomPosInf 0
zeroAtomNegInf  = mkQAtomNegInf 0
zeroAtom3Pos    = mkQAtom3Pos 0
zeroAtom3Neg    = mkQAtom3Neg 0
zeroAtom3Full   = mkQAtom3Full 0

unitAtomPos     = mkQAtomPos 1
unitAtomFull    = mkQAtomFull 1
unitAtomInf     = mkQAtomInf 1
unitAtomPosInf  = mkQAtomPosInf 1
unitAtom3Pos    = mkQAtom3Pos 1
unitAtom3Full   = mkQAtom3Full 1

minAtomPos     = mkQAtomPos themin
minAtomFull    = mkQAtomFull themin
minAtomInf     = mkQAtomInf themin
minAtomPosInf  = mkQAtomPosInf themin
minAtom3Pos    = mkQAtom3Pos themin
minAtom3Full   = mkQAtom3Full themin

type Timestamp = QAtomPosInf

instance Num (Quantum Atom QFull Double) where
    a + b = mkQ Atom QFull $ (qval a) + (qval b)
    a - b = mkQ Atom QFull $ (qval a) - (qval b)
    a * b = mkQ Atom QFull $ (qval a) * (qval b)
    abs = (mkQ Atom QFull) . abs . qval
    negate = (mkQ Atom QFull) . negate . qval
    signum = (mkQ Atom QFull) . signum . qval
    fromInteger = (mkQ Atom QFull) . fromInteger

instance Num (Quantum Atom QPos Double) where
    a + b = mkQ Atom QPos $ (qval a) + (qval b)
    a - b = mkQ Atom QPos $ (qval a) - (qval b)
    a * b = mkQ Atom QPos $ (qval a) * (qval b)
    abs = (mkQ Atom QPos) . abs . qval
    negate = (mkQ Atom QPos) . negate . qval
    signum = (mkQ Atom QPos) . signum . qval
    fromInteger = (mkQ Atom QPos) . fromInteger

instance Num (Quantum Atom QNeg Double) where
    a + b = mkQ Atom QNeg $ (qval a) + (qval b)
    a - b = mkQ Atom QNeg $ (qval a) - (qval b)
    a * b = mkQ Atom QNeg $ (qval a) * (qval b)
    abs _ = mkQ Atom QNeg 0.0
    negate = (mkQ Atom QNeg) . negate . qval
    signum = (mkQ Atom QNeg) . signum . qval
    fromInteger = (mkQ Atom QNeg) . fromInteger

instance Num (Quantum Atom QInf Double) where
    a + b = mkQ Atom QInf $ (qval a) + (qval b)
    a - b = mkQ Atom QInf $ (qval a) - (qval b)
    a * b = mkQ Atom QInf $ (qval a) * (qval b)
    abs = (mkQ Atom QInf) . abs . qval
    negate = (mkQ Atom QInf) . negate . qval
    signum = (mkQ Atom QInf) . signum . qval
    fromInteger = (mkQ Atom QInf) . fromInteger

instance Num (Quantum Atom QPosInf Double) where
    a + b = mkQ Atom QPosInf $ (qval a) + (qval b)
    a - b = mkQ Atom QPosInf $ (qval a) - (qval b)
    a * b = mkQ Atom QPosInf $ (qval a) * (qval b)
    abs = (mkQ Atom QPosInf) . abs . qval
    negate = (mkQ Atom QPosInf) . negate . qval
    signum = (mkQ Atom QPosInf) . signum . qval
    fromInteger = (mkQ Atom QPosInf) . fromInteger

instance Num (Quantum Atom QNegInf Double) where
    a + b = mkQ Atom QNegInf $ (qval a) + (qval b)
    a - b = mkQ Atom QNegInf $ (qval a) - (qval b)
    a * b = mkQ Atom QNegInf $ (qval a) * (qval b)
    abs _ = mkQ Atom QNegInf 0.0
    negate = (mkQ Atom QNegInf) . negate . qval
    signum = (mkQ Atom QNegInf) . signum . qval
    fromInteger = (mkQ Atom QNegInf) . fromInteger

instance Fractional (Quantum Atom QFull Double) where
    a/b = mkQ Atom QFull $ (qval a)/(qval b)
    fromRational = (mkQ Atom QFull) . fromRational

instance Fractional (Quantum Atom QPos Double) where
    a/b = mkQ Atom QPos $ (qval a)/(qval b)
    fromRational = (mkQ Atom QPos) . fromRational

instance Fractional (Quantum Atom QNeg Double) where
    a/b = mkQ Atom QNeg $ (qval a)/(qval b)
    fromRational = (mkQ Atom QNeg) . fromRational

instance Fractional (Quantum Atom QInf Double) where
    a/b = mkQ Atom QInf $ (qval a)/(qval b)
    fromRational = (mkQ Atom QInf) . fromRational

instance Fractional (Quantum Atom QPosInf Double) where
    a/b = mkQ Atom QPosInf $ (qval a)/(qval b)
    fromRational = (mkQ Atom QPosInf) . fromRational

instance Fractional (Quantum Atom QNegInf Double) where
    a/b = mkQ Atom QNegInf $ (qval a)/(qval b)
    fromRational = (mkQ Atom QNegInf) . fromRational
