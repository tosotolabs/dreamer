{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE TypeFamilies          #-}

module Dreamer.Atmosphere.Bond
(
    Bond(..),
    BondBreed(..),
    BondStrength(..),

    QBondS,
    QBondSPos,
    QBondSNeg,
    QBondSFull,
    QBondSInf,
    QBondSPosInf,
    QBondSNegInf,

    -- BondProcess,

    mkQBondS,
    mkQBondSFull,
    mkQBondSPos,
    mkQBondSNeg,
    mkQBondSInf,
    mkQBondSPosInf,
    mkQBondSNegInf,

    module Dreamer.Atmosphere.Quanta
) where

import           Dreamer.Atmosphere.Atom
import           Dreamer.Atmosphere.Neosynth
import           Dreamer.Atmosphere.Quanta
import           Dreamer.Metadata
import           Dreamer.Neural.Signature
import           Dreamer.Process

data BondStrength = BondStrength deriving (Show, Read)

instance (QuantaBounded b, Num v, Fractional v, Ord v) => Quanta BondStrength b v where
    data Quantum BondStrength b v = MkQBondS b v deriving (Show, Read)
    mkQ _ b v = MkQBondS b (qbound b v)
    qtype _ = BondStrength
    qspan (MkQBondS b _) = b
    qval (MkQBondS _ v) = v

type QBondS a = Quantum BondStrength a Double
type QBondSPos = QBondS QPos
type QBondSNeg = QBondS QNeg
type QBondSFull = QBondS QFull
type QBondSInf = QBondS QInf
type QBondSPosInf = QBondS QPosInf
type QBondSNegInf = QBondS QNegInf

-- We need to specify the span that a bond works with. This can be different
-- from the span of the bond strength itself if so wish. The spans of the atoms
-- it works with can also differ from that of the neosynth. However, the span
-- of the old atom value and the new atom value have to be the same.
-- type BondProcess q = NeuralState -> q


class (Metadata b, State st, Quanta BondStrength bsp v) => Bond b st bsp v where
    data BondBreed b st bsp v :: *
    runbond :: BondBreed b st bsp v -> st -> Quantum BondStrength bsp v

-- instance Show (BondProcess (Quantum bs bsp v)) where
--     show bp = "BondProcess"

mkQBondS :: (QuantaBounded sp) => sp -> Double -> QBondS sp
mkQBondS sp v = mkQ BondStrength sp v

mkQBondSFull :: Double -> QBondSFull
mkQBondSFull = mkQBondS QFull

mkQBondSPos :: Double -> QBondSPos
mkQBondSPos = mkQBondS QPos

mkQBondSNeg :: Double -> QBondSNeg
mkQBondSNeg = mkQBondS QNeg

mkQBondSInf :: Double -> QBondSInf
mkQBondSInf = mkQBondS QInf

mkQBondSPosInf :: Double -> QBondS QPosInf
mkQBondSPosInf = mkQBondS QPosInf

mkQBondSNegInf :: Double -> QBondS QNegInf
mkQBondSNegInf = mkQBondS QNegInf

instance Num (Quantum BondStrength QFull Double) where
    a + b = mkQ BondStrength QFull $ (qval a) + (qval b)
    a - b = mkQ BondStrength QFull $ (qval a) - (qval b)
    a * b = mkQ BondStrength QFull $ (qval a) * (qval b)
    abs = (mkQ BondStrength QFull) . abs . qval
    negate = (mkQ BondStrength QFull) . negate . qval
    signum = (mkQ BondStrength QFull) . signum . qval
    fromInteger = (mkQ BondStrength QFull) . fromInteger

instance Num (Quantum BondStrength QPos Double) where
    a + b = mkQ BondStrength QPos $ (qval a) + (qval b)
    a - b = mkQ BondStrength QPos $ (qval a) - (qval b)
    a * b = mkQ BondStrength QPos $ (qval a) * (qval b)
    abs = (mkQ BondStrength QPos) . abs . qval
    negate = (mkQ BondStrength QPos) . negate . qval
    signum = (mkQ BondStrength QPos) . signum . qval
    fromInteger = (mkQ BondStrength QPos) . fromInteger

instance Num (Quantum BondStrength QNeg Double) where
    a + b = mkQ BondStrength QNeg $ (qval a) + (qval b)
    a - b = mkQ BondStrength QNeg $ (qval a) - (qval b)
    a * b = mkQ BondStrength QNeg $ (qval a) * (qval b)
    abs a = mkQ BondStrength QNeg 0.0
    negate = (mkQ BondStrength QNeg) . negate . qval
    signum = (mkQ BondStrength QNeg) . signum . qval
    fromInteger = (mkQ BondStrength QNeg) . fromInteger

instance Fractional (Quantum BondStrength QFull Double) where
    a/b = mkQ BondStrength QFull $ (qval a)/(qval b)
    fromRational = (mkQ BondStrength QFull) . fromRational

instance Fractional (Quantum BondStrength QPos Double) where
    a/b = mkQ BondStrength QPos $ (qval a)/(qval b)
    fromRational = (mkQ BondStrength QPos) . fromRational

instance Fractional (Quantum BondStrength QNeg Double) where
    a/b = mkQ BondStrength QNeg $ (qval a)/(qval b)
    fromRational = (mkQ BondStrength QNeg) . fromRational
