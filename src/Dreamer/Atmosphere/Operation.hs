{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE ScopedTypeVariables #-}

-- TODO - Come up with various molecularization and averaging methods.
-- There's average. Then there's (TODO) fusion which is tied to (TODO) probing.
{- Probing requires that molecule nodes be instances of Ord so they can be sorted and the subject
    can be quickly matched with exact nodes or simple ranges between nodes. A range is defined by
    two nodes and the edge between them. We try to match the subject directly with ranges first
    and not the actual nodes themselves. This means we treat edges as first-class, gathering them
    at the start.
    If the molecule is empty or has one node, there is zero flow.
    Find nodes with value(s) closest to the subject's. Begin molecule propagation from those nodes.
    More propagations increases accuracy (justification). Propagation is done one instigator at a time.
    If subject falls between two nodes, propagate from the two nodes separately then perform a tug of
    war calculation with their flow scores. Otherwise, use flow score of single node. FOr each stage
    during propagation, take the average of each node's outgoing links then average those results. TODO look in journal for more
    Average all stage results for final flow score.
-}

{- Fusion is an attempt to perform an average with molecules even with knowledge that they may differ
    greatly. For that reason, fusion requires a model molecule to mold the process around.
    -- Minimal fusion
    In performing minimal fusion, for each model node, we probe each molecule for the node's flow
    value. We take the average of the flow values. When all model nodes are probed for, we create a
    balanced molecule with their flow score averages, creating bonds as specified by the model. This
    final process is called unbiased stabilization.
    An alternative is biased stabilization, we could match model nodes with their probe counterparts
    in each molecule. When assigning values to bonds in the final molecule, take the average of the
    bonds outgoing from the counterparts of the subject model node to the counterparts of the subject's
    dependents in the model and factor that in to how the subject's flow value is shared with the bond
    outgoing to the subject's dependents.
    -- Inter-fusion
    Inter-fusion involves creating inter-probing molecules from the given molecules to be probed by
    nodes of the model molecule. For each node in each molecule, perform minimal fusion with the node
    as the probe and all molecules besides its own as the probing subjects. The result molecules of
    probing with each molecule will be probed by each node in the model in the same manner. Proceed with
    a form of stabilization.
-}

module Dreamer.Atmosphere.Operation
(
    avgedges,
    avgedges',
    qavg,
    qbiasedavg,
    qcomplete,
    qcorrelation,
    qcorrelationgraph,
    qdiversity,
    qdiversity',
    qdiversitygraph,
    qfba3,
    qmin,
    qmax,
    qmolavg,
    qmolavg',
    qmolbiasedavg,
    qmolbiasedavg',
    qmolecularize,
    qprobemolecule,
    qsum,
    qtoscale,
    runbonds,
    sumedges,

    -- Testing
    qcorrelationgraph_T,
    qdiversity_T,
    qfba3_T,
    qprobemolecule_T,

    -- Limits
    qcomboelemlim,
    qfba3depthlim,
    qfba3stoplim,
    qprobelim,
    qthemin
) where

import           Control.Lens
import           Control.Monad                     (foldM)
import qualified Data.Graph.Inductive.Graph        as G
import qualified Data.Graph.Inductive.PatriciaTree as G
import qualified Data.List                         as L
import qualified Data.Set                          as S
import           Data.Tuple
import           Dreamer.Atmosphere.Atom
import           Dreamer.Atmosphere.Bond
import           Dreamer.Atmosphere.Limit
import           Dreamer.Atmosphere.Molecule
import           Dreamer.Atmosphere.Quanta
import           Dreamer.Internal.Operation
import           Dreamer.Neural.Signature
import           Dreamer.Process

-- Scale
-- Note: will produce NaN infinite bounds. How do you scale infinity?
qtoscale :: (QuantaBounded sp, Quanta a sp1 Double, Quanta a sp Double) => sp -> Quantum a sp1 Double -> Quantum a sp Double
qtoscale sp q = let
    sp1 = qspan q
    val = qval q
    (min1,max1) = (qminbound sp1, qmaxbound sp1)
    (min2,max2) = (qminbound sp, qmaxbound sp)
    in mkQ (qtype q) sp $ (val-min1)*(max2-min2)/(max1-min1)+min2

qcomplete :: (Quanta a sp Double) => [Quantum a sp Double] -> b -> G.Gr (Quantum a sp Double) b
qcomplete qns b = G.mkGraph ns es
    where
        ns = [(n, qns !! n) | n <- [0..(length qns)-1]]
        es = foldl (\acc nid ->
                foldl (\acc' enid ->
                    if nid == enid
                        then acc'
                        else (nid,enid,b):acc'
                    ) acc [0..(length qns)-1]
                ) [] [0..(length qns)-1]

--
-- Diversity/Correlation
-- TODO normalize nodes
qdiversitygraph :: (Quanta a sp Double) => [Quantum a sp Double] -> SimpleMolecule a sp QPos
qdiversitygraph nodes = G.mkGraph ns es
    where
        len = length nodes
        ns = [(n,nodes !! n) | n <- [0..(len-1)]]
        es = foldl (\acc nid ->
                foldl (\acc' enid ->
                    if nid == enid
                        then acc'
                        else (nid,enid,mkQBondSPos $ abs $ (qval $ nodes !! nid)-(qval $ nodes !! enid)):acc'
                    ) acc [0..len-1]
                ) [] [0..len-1]

-- TODO normalize lists, meaning bring to scale; simply dividing by
-- the largest value is less than ideal; the nature of the result
-- depends on the scale of the quanta
qcorrelationgraph :: (Quanta a sp Double) => [Quantum a sp Double] -> [Quantum a sp Double] -> SimpleMolecule a sp QPosInf
qcorrelationgraph qxs qys = G.mkGraph ns es
    where
        len = min (length qxs) (length qys)
        ns = [(n,qxs !! n) | n <- [0..(len-1)]]
        es = [(n,a,mkQBondSPosInf $ abs $ ((qval $ qxs !! n)-(qval $ qxs !! a)) - ((qval $ qys !! n)-(qval $ qys !! a)))
                |   n <- [0..(len-1)],
                    a <- [(n+1)..(len-1)]]

type QEmphasis b sp = Quantum b sp Double
type QDemphasis b sp = Quantum b sp Double
type QBias b sp = Quantum b sp Double
type QRank b sp = Quantum b sp Double
-- TODO NORMALIZE!!! below makes sense only if we first normalize
qdiversity :: forall a b c d e sp sp1 sp2 sp3 sp4 bsp.
                (Quanta a sp Double, Quanta b sp1 Double, Quanta c sp2 Double, Quanta d sp3 Double, Quanta e sp4 Double, QuantaBounded bsp)
                => SimpleMolecule a sp bsp
                -> [(QEmphasis b sp1, QDemphasis c sp2, QBias d sp3, QRank e sp4)]
                -> (QAtomPos, [(QAtomFull, Quantum d sp3 Double, Quantum e sp4 Double)])
qdiversity gr probevs = let
    gr' = (G.mkGraph [(n,((probevs !! n) ^._3, (probevs !! n) ^._4)) | (n,_) <- G.labNodes gr] [let
        (nemph,ndemph,_,_) = probevs !! sid
        emphavg = ((qval nemph) + (qval ndemph))/2
        in (sid,eid,mkQBondSInf $ (qval bv)*(1 + emphavg))
                        |   (sid,eid,bv) <- G.labEdges gr]) :: G.Gr (Quantum d sp3 Double, Quantum e sp4 Double) QBondSInf
    n'val'trio = map (\(nid,(nbias,nrank)) -> let
        (essum,escount) = foldl (\(bvacc,countacc) (_,qb) -> (bvacc + qval qb, countacc+1)) (0,0) (G.lsuc gr' nid)
        esavg = mkQAtomFull $ essum/(if escount == 0 then 1 else fi escount)
        in (esavg,nbias,nrank)
                                    ) $ G.labNodes gr'
    fba3trios = combinations 3 n'val'trio
    fba3avg = mkQAtomPos $ 1 - (abs $ qval $ qavg $ map (\trioarr -> qfba3 (trioarr !! 0) (trioarr !! 1) (trioarr !! 2) qfba3stoplim) fba3trios)
    in (fba3avg, n'val'trio)

-- qdiversity' :: (Quanta a sp Double, Quanta b sp1 Double, Quanta c sp2 Double, QuantaBounded bsp)
--             => SimpleMolecule a sp bsp
--             -> (QAtomPos, [(QAtomFull, Quantum b sp1 Double, Quantum c sp2 Double)])
qdiversity' sm = qdiversity sm $ replicate (G.order sm) (zeroAtomPos, zeroAtomNeg, zeroAtomPos, zeroAtomPos)

-- We assume the two molecules have the same numbering of nodes.
-- TODO normalize lists
qcorrelation :: (Quanta a sp Double) => [Quantum a sp Double] -> [Quantum a sp Double] -> QAtomPos
qcorrelation qxs qys = let
    len = min (length qxs) (length qys)
    dgr = (G.mkGraph [(n,-1) | n <- [0..(len-1)]] [(n,a,abs $ ((qval $ qxs !! n)-(qval $ qxs !! a)) - ((qval $ qys !! n)-(qval $ qys !! a)))
                        |   n <- [0..(len-1)],
                            a <- [(n+1)..(len-1)]]) :: G.Gr Double Double
    subAvgs = [(sum $ map G.edgeLabel $ (G.out dgr n) ++ (G.inn dgr n))/(fi $ (G.order dgr)-1) | n <- [0..(fi $ (G.order dgr)-1)]]
    in mkQAtomPos $ 1 - (abs $ (sum subAvgs)/(fi $ G.order dgr))

--
-- Molecularization

-- Product is a bidirectional, complete, and equally-stabilized molecule
qmolecularize :: (QuantaBounded bsp, Quanta b xp Double) => bsp -> [(Quantum a asp Double, Quantum b xp Double)] -> SimpleMolecule a asp bsp
qmolecularize bsp qns = G.mkGraph ns es
    where
        len = length qns
        ns = [(n,fst $ qns !! n) | n <- [0..len-1]]
        es = foldl (\acc nid ->
                let qb = mkQBondS bsp (qval $ snd $ qns !! nid)
                in foldl (\acc' enid -> if nid == enid then acc' else (nid,enid,qb):acc') acc [0..len-1]
                ) [] [0..len-1]

--
-- Probing

-- | Sequential, unguided, free flow
-- Returns 0 for connection-less molecule
qprobemolecule :: (G.Graph gr, QuantaBounded bsp, Quanta a asp Double, Quanta a QInf Double) => gr (Quantum a asp Double) (QBondS bsp) -> Quantum a asp Double -> QAtomPosInf -> QAtomInf
qprobemolecule gr q lim = let
    grnodes = G.labNodes gr
    exacts = L.filter (\(_,q') -> (qval q) == (qval q')) grnodes
    in if null exacts
        then let
            es = G.labEdges gr
            mbes = map (\(sid,eid,_) -> let
                sv = qval $ getmaybe (G.lab gr sid) q
                ev = qval $ getmaybe (G.lab gr eid) q
                (sid',eid') = if sv<ev then (sid,eid) else (eid,sid)
                in if inrange (sv,ev) (qval q)
                    then Just ([sid',eid'],(sid,eid))
                    else Nothing
                    ) es
            (insts,rngs) = foldl (\(sacc,eacc) mb -> case mb of
                Just (ns',e) -> (sacc++ns', eacc++[e])
                otherwise    -> (sacc,eacc)
                        ) ([],[]) mbes

            probes = map (\inst -> probe_ gr inst (floor $ qval lim)) insts
            probes' = map (\(iavg,oavg,davg) -> if davg == 0 then 0 else (iavg+oavg)/davg) probes
            probes'' = zip insts probes'
            rngscores = map (\(sid,eid) -> let
                            sv = qval $ getmaybe (G.lab gr sid) q
                            ev = qval $ getmaybe (G.lab gr eid) q
                            in tugofwar (getmaybe (lookup sid probes'') 0, (qval q) - sv) (getmaybe (lookup eid probes'') 0, ev - (qval q))
                            ) rngs
            in if null rngs then zeroAtomInf else mkQAtomInf ((sum rngscores)/(fi $ length rngs))
        else mkQAtomInf $ qval $ qavg $ map snd exacts

--
-- Averaging

type FBA3Value a asp b bsp c csp = (Quantum a asp Double, Quantum b bsp Double, Quantum c csp Double)

-- Bounds value only at the end of all fractal processing.
qfba3 :: (Quanta a asp Double, Quanta b bsp Double, Quanta c csp Double, Quanta d dsp Double)
        => FBA3Value a asp b bsp c csp
        -> FBA3Value a asp b bsp c csp
        -> FBA3Value a asp b bsp c csp
        -> Quantum d dsp Double
        -> Quantum a asp Double
qfba3 (x,bx,rx) (y,by,ry) (z,bz,rz) sl = mkQ (qtype x) (qspan x) $ fba3 (qval x,qval bx,qval rx) (qval y,qval by,qval ry) (qval z,qval bz,qval rz) $ qval sl

qmolavg :: (Eq (Quantum a asp Double), QuantaBounded asp, QuantaBounded bsp) => [SimpleMolecule a asp bsp] -> SimpleMolecule a asp bsp
qmolavg (xg:ms) = let
    xnodes = map swap $ G.labNodes xg
    msnodes = map ((map swap) . G.labNodes) ms
    gr'mnodes = zip ms msnodes -- [(gr, [(QuantumAspect, Node)])]

    xbs = map (\xnode -> qmolavg_ xnode xg gr'mnodes) xnodes

    in G.mkGraph (G.labNodes xg) (concat xbs)
qmolavg _ = G.empty

qmolavg' :: (Eq (Quantum a asp Double), QuantaBounded asp, QuantaBounded bsp) => [SimpleMolecule a asp bsp] -> SimpleMolecule a asp bsp
qmolavg' (xg:ms) = let
    xnodes = map swap $ G.labNodes xg
    msnodes = map ((map swap) . G.labNodes) ms
    gr'mnodes = zip ms msnodes

    xbs = map (\xnode -> qmolavg_' xnode xg gr'mnodes) xnodes

    in G.mkGraph (G.labNodes xg) (concat xbs)

-- Final molecule will be constructed with all and only the nodes of the first molecule
-- irregardless of differences in molecule size. Bonds are averaged individually here.
qmolbiasedavg :: (Eq (Quantum a asp Double), QuantaBounded asp, QuantaBounded bsp, QuantaBounded bsp1) => SimpleMolecule a asp bsp -> SimpleMolecule a asp bsp -> SimpleMolecule a asp bsp1 -> SimpleMolecule a asp bsp
qmolbiasedavg xg yg bg = let
    xnodes = map swap $ G.labNodes xg -- (G.Node, QuantumAspect) -> (QuantumAspect, G.Node)
    -- Go through xnodes. For each node, get the quantum aspect node in the other two graphs.
    ynodes = map swap $ G.labNodes yg
    bnodes = map swap $ G.labNodes bg
    xbs = map (\xnode -> qmolbiasedavg_ xnode xg (yg,ynodes) (bg,bnodes)) xnodes
    in G.mkGraph (G.labNodes xg) (concat xbs)

-- Final molecule will be constructed with all and only the nodes of the first molecule
-- irregardless of differences in molecule size. Bonds are averaged per atom here.
-- Molecule is stabilized i.e. each aspect's bond average is shared amongst neighbors equally.
qmolbiasedavg' :: (Eq (Quantum a asp Double), QuantaBounded asp, QuantaBounded bsp, QuantaBounded bsp1) => SimpleMolecule a asp bsp -> SimpleMolecule a asp bsp -> SimpleMolecule a asp bsp1 -> SimpleMolecule a asp bsp
qmolbiasedavg' xg yg bg = let
    xnodes = map swap $ G.labNodes xg -- (G.Node, QuantumAspect) -> (QuantumAspect, G.Node)
    -- Go through xnodes. For each node, get the quantum aspect node in the other two graphs.
    ynodes = map swap $ G.labNodes yg
    bnodes = map swap $ G.labNodes bg
    xbs = map (\xnode -> qmolbiasedavg_' xnode xg (yg,ynodes) (bg,bnodes)) xnodes
    in G.mkGraph (G.labNodes xg) (concat xbs)

qmin :: (Quanta q sp Double) => Quantum q sp Double -> Quantum q sp Double -> Quantum q sp Double
qmin q1 q2 = if (qval q1) <= (qval q2) then q1 else q2

qmax :: (Quanta q sp Double) => Quantum q sp Double -> Quantum q sp Double -> Quantum q sp Double
qmax q1 q2 = if (qval q1) >= (qval q2) then q1 else q2

qavg :: (Quanta q sp Double) => [Quantum q sp Double] -> Quantum q sp Double
qavg qs = if length qs == 0
    then error "Cannot take average of empty list of quanta."
    else let
        q = head qs
        avg = (sum $ map qval qs)/(fromIntegral $ length qs)
        in mkQ (qtype q) (qspan q) avg

qsum :: (Quanta q sp Double) => [Quantum q sp Double] -> Quantum q sp Double
qsum qs = if length qs == 0
    then error "Cannot take sum of empty list of quanta"
    else let
        hq = head qs
        sumq = sum $ map qval qs
        in mkQ (qtype hq) (qspan hq) sumq

qbiasedavg :: (Quanta q sp Double, Quanta q1 sp1 Double, Quanta q2 sp2 Double) => Quantum q sp Double -> Quantum q1 sp1 Double -> Quantum q2 sp2 Double -> Quantum q sp Double
qbiasedavg q1 q2 qb = case (qval q1, qval q2, qval qb) of
    (0,0,_) -> q1
    (0,y,b) -> mkQ (qtype q1) (qspan q1) $ y/(2+b)
    (x,y,b) -> mkQ (qtype q1) (qspan q1) $ (x+y)/(2+(((y/x)-1)*b))

--
-- Bonds
runbonds :: (Bond b st bsp v, State st) => MolecularStructure st a asp b bsp v -> st -> GenericMolecule a asp bsp v
runbonds gr ns = let
    bonds = map (\(sid,eid,breed) -> (sid,eid,runbond breed ns)) $ G.labEdges gr
    in G.mkGraph (G.labNodes gr) bonds

--
-- Debugging versions of operations

qcorrelationgraph_T :: (Quanta a sp Double) => [Quantum a sp Double] -> [Quantum a sp Double] -> IO (SimpleMolecule a sp QPosInf)
qcorrelationgraph_T qxs qys = do
    let len = min (length qxs) (length qys)
        ns = [(n,qxs !! n) | n <- [0..(len-1)]]
    putStrLn "qxs"
    mapM_ (\qn -> print $ qval qn) qxs
    putStrLn "qys"
    mapM_ (\qn -> print $ qval qn) qys
    es <- mapM (\nid ->
            mapM (\enid -> do
                let v = abs $ ((qval $ qxs !! nid)-(qval $ qxs !! enid)) - ((qval $ qys !! nid)-(qval $ qys !! enid))
                print (nid,enid,mkQBondSPosInf v)
                return $ (nid,enid,mkQBondSPosInf v)
                ) [(nid+1)..len-1]
            ) [0..len-1] >>= return . concat
    return $ G.mkGraph ns es

qdiversity_T :: forall a b c d e sp sp1 sp2 sp3 sp4 bsp.
                (Quanta a sp Double, Quanta b sp1 Double, Quanta c sp2 Double, Quanta d sp3 Double, Quanta e sp4 Double, QuantaBounded bsp)
                => SimpleMolecule a sp bsp
                -> [(QEmphasis b sp1, QDemphasis c sp2, QBias d sp3, QRank e sp4)]
                -> IO (QAtomPos, [(QAtomFull, Quantum d sp3 Double, Quantum e sp4 Double)])
qdiversity_T gr probevs = do
    putStrLn $ show $ map (\(a,b,c,d) -> (qval a, qval b, qval c, qval d)) probevs
    let gr' =  (G.mkGraph [(n,((probevs !! n) ^._3, (probevs !! n) ^._4)) | (n,_) <- G.labNodes gr] [let
            (nemph,ndemph,_,_) = probevs !! sid
            emphavg = ((qval nemph) + (qval ndemph))/2
            in (sid,eid,mkQBondSInf $ (qval bv)*(1 + emphavg))
                            |   (sid,eid,bv) <- G.labEdges gr]) :: G.Gr (Quantum d sp3 Double, Quantum e sp4 Double) QBondSInf
    -- gr' <-
    putStrLn "n'val'trio"
    n'val'trio <- mapM (\(nid,(nbias,nrank)) -> do
                    (essum,escount) <- foldM (\(bvacc,countacc) (_,qb) -> do
                                        print (bvacc,countacc,qval qb)
                                        return (bvacc + qval qb, countacc+1)
                                        ) (0,0) (G.lsuc gr' nid)
                    let esavg = mkQAtomFull $ essum/(if escount == 0 then 1 else fi escount)
                    print essum
                    return (esavg,nbias,nrank)
                    ) $ G.labNodes gr'
    let fba3trios = combinations 3 n'val'trio

    putStr "calculating fba3 with "
    fba3avg_ <- mapM (\trioarr -> do
                    print $ map (\(a,b,c) -> show (qval a, qval b, qval c)) trioarr
                    qfba3_T (trioarr !! 0) (trioarr !! 1) (trioarr !! 2) qfba3stoplim
                    ) fba3trios
    let fba3avg = mkQAtomPos $ 1 - (abs $ qval $ qavg fba3avg_)
    putStrLn $ "done calculating fba3: "++(show fba3avg)
    return (fba3avg, n'val'trio)

qprobemolecule_T :: (G.Graph gr, QuantaBounded bsp, Quanta a asp Double, Quanta a QInf Double) => gr (Quantum a asp Double) (QBondS bsp) -> Quantum a asp Double -> QAtomPosInf -> IO QAtomInf
qprobemolecule_T gr q lim = do
    putStrLn "qprobemolecule_T"
    let grnodes = G.labNodes gr
    let exacts = L.filter (\(_,q') -> (qval q) == (qval q')) grnodes
    print $ map (qval . snd) grnodes
    if null exacts
        then do
            putStrLn "empty exacts"
            let es = G.labEdges gr
                mbes = map (\(sid,eid,_) -> let
                    sv = qval $ getmaybe (G.lab gr sid) q
                    ev = qval $ getmaybe (G.lab gr eid) q
                    (sid',eid') = if sv<ev then (sid,eid) else (eid,sid)
                    in if inrange (sv,ev) (qval q)
                        then Just ([sid',eid'],(sid,eid))
                        else Nothing
                        ) es
                (insts,rngs) = foldl (\(sacc,eacc) mb -> case mb of
                    Just (ns',e) -> (sacc++ns', eacc++[e])
                    otherwise    -> (sacc,eacc)
                            ) ([],[]) mbes

            let probes = map (\inst -> probe_ gr inst (floor $ qval lim)) insts
            putStrLn $ "probes: "++(show probes)
            let probes' = map (\(iavg,oavg,davg) -> if davg == 0 then 0 else (iavg+oavg)/davg) probes
            putStrLn $ "probes': "++(show probes')
            let probes'' = zip insts probes'
            putStrLn $ "probes'': "++(show probes'')
            rngscores <- mapM (\(sid,eid) -> let
                            sv = qval $ getmaybe (G.lab gr sid) q
                            ev = qval $ getmaybe (G.lab gr eid) q
                            res = tugofwar (getmaybe (lookup sid probes'') 0, (qval q) - sv) (getmaybe (lookup eid probes'') 0, ev - (qval q))
                            in do
                                putStrLn $ "tow: "++(show res)
                                return res
                            ) rngs
            return $ if null rngs then zeroAtomInf else mkQAtomInf ((sum rngscores)/(fi $ length rngs))
        else do
            putStrLn "got exact(s)"
            return $ mkQAtomInf $ qval $ qavg $ map snd exacts

qfba3_T :: (Quanta a asp Double, Quanta b bsp Double, Quanta c csp Double, Quanta d dsp Double)
        => FBA3Value a asp b bsp c csp
        -> FBA3Value a asp b bsp c csp
        -> FBA3Value a asp b bsp c csp
        -> Quantum d dsp Double
        -> IO (Quantum a asp Double)
qfba3_T (x,bx,rx) (y,by,ry) (z,bz,rz) sl = fba3_T (qval x,qval bx,qval rx) (qval y,qval by,qval ry) (qval z,qval bz,qval rz) (qval sl,0) >>= return . (mkQ (qtype x) (qspan x))

--
-- Internal

probe_ :: (G.Graph gr, QuantaBounded sp) => gr a (QBondS sp) -> G.Node -> Int -> (Double, Double, Double)
probe_ gr nid lim = if lim <= 0
    then (0,0,0)
    else let
        nes = G.lsuc gr nid
        esavg = if length nes == 0 then 0 else (foldl (\acc (_,qb) -> acc + (qval qb)) 0 nes) / (fi $ length nes)
        probes = map (\(eid,_) -> probe_ gr eid (lim-1)) nes
        (oavg,savg,davg) = foldl (\(oacc,sacc,dacc) (oavg',savg',davg') -> (oacc+oavg',sacc+savg',dacc+davg')) (0,0,0) probes
        oavg' = oavg/(fi $ length nes)
        probeavg = if davg == 0 then 0 else (oavg+savg)/davg
        in (esavg, probeavg, davg+1)

qmolavg_ :: (Eq a, G.Graph xg, G.Graph gr, QuantaBounded sp) => (a, G.Node) -> xg a (QBondS sp) -> [(gr a (QBondS sp), [(a, G.Node)])] -> [(G.Node, G.Node, QBondS sp)]
qmolavg_ (asp, xnodeid) xg gr'mnodes = let
    xins = G.lpre xg xnodeid
    xinaspedges = promoteaspedge xg xins
    xouts = G.lsuc xg xnodeid
    xoutaspedges = promoteaspedge xg xouts

    -- Get inaspedges and outaspedges of other graphs
    aspedges = map (\(gr, mnodes) -> getaspedges gr asp mnodes) gr'mnodes -- [([(a,(Node,Bond))], [(a,(Node,Bond))])]
    (inaspedges, outaspedges) = foldl (\(ins',outs') (ins,outs) -> (ins'++[ins], outs'++[outs])) ([],[]) aspedges
    -- ^ ([[(a,(Node,Bond))]], [[(a,(Node,Bond))]])

    xedges1 = map (\xinaspedge@(_,(startnodeid,qb)) -> let
                qbonds = map (\ins -> getqbond xinaspedge ins) inaspedges
                inavg = qavg $ qbonds++[mkQBondSInf $ qval qb]
                inavg' = mkQBondS (qspan qb) (qval inavg)
                in (startnodeid, xnodeid, inavg')
                ) xinaspedges

    xedges2 = map (\xoutaspedge@(_,(endnodeid,qb)) -> let
                qbonds = map (getqbond xoutaspedge) outaspedges
                outavg = qavg $ qbonds++[mkQBondSInf $ qval qb]
                outavg' = mkQBondS (qspan qb) (qval outavg)
                in (xnodeid, endnodeid, outavg')
                ) xoutaspedges
    in xedges1++xedges2

qmolavg_' :: (Eq a, G.Graph xg, G.Graph gr, QuantaBounded sp) => (a, G.Node) -> xg a (QBondS sp) -> [(gr a (QBondS sp), [(a, G.Node)])] -> [(G.Node, G.Node, QBondS sp)]
qmolavg_' (asp, xnodeid) xg gr'mnodes = let
    xouts = G.lsuc xg xnodeid
    in if length xouts == 0
        then []
        else let
            (_,abond) = head xouts
            xoutavg = avgedges xouts

            -- Go through gr'mnodes. Find the node in each and avg up edges.
            allavg = ((foldl (\acc (gr,mnodes) -> acc + (avgedges' gr asp mnodes True)) 0 gr'mnodes) + xoutavg) / (fromIntegral $ (length gr'mnodes) + 1)
            avgshare = mkQBondS (qspan abond) $ allavg/(fromIntegral $ length xouts)
            in map (\(endnodeid,_) -> (xnodeid,endnodeid,avgshare)) xouts

qmolbiasedavg_ :: (Eq a, QuantaBounded sp, QuantaBounded sp1, QuantaBounded sp2, G.Graph xg, G.Graph yg, G.Graph bg) => (a, G.Node) -> xg a (QBondS sp) -> (yg a (QBondS sp1), [(a, G.Node)]) -> (bg a (QBondS sp2), [(a, G.Node)]) -> [(G.Node, G.Node, QBondS sp)]
qmolbiasedavg_ (asp, xnodeid) xg (yg,ynodes) (bg,bnodes) = let
    -- Query for the links of xnodeid and get their values and the node values at the other ends.
    xins = G.lpre xg xnodeid -- [(G.Node, QBondS sp)]
    xinaspedges = promoteaspedge xg xins -- [(QuantumAspect, (G.Node, QBondS sp))]
    xouts = G.lsuc xg xnodeid
    xoutaspedges = promoteaspedge xg xouts

    -- Issue is that the same QuantumAspects in xg, yg, and bg may have differing G.Node IDs.
    -- We need to go through the inverted edges of xins and xouts. Before that, we need to
    -- gather ins and outs of the same subject QuantumAspect in yg and bg. This is the reason
    -- we include the swapped list of nodes of yg and bg as part of our parameters.

    (yinaspedges, youtaspedges) = getaspedges yg asp ynodes
    (binaspedges, boutaspedges) = getaspedges bg asp bnodes

    xedges1 = map (\xinaspedge@(_,(startnodeid,qb)) -> let
                    inbavg = qbiasedavg qb (getqbond xinaspedge yinaspedges) (getqbond xinaspedge binaspedges)
                    in (startnodeid, xnodeid, inbavg)
                    ) xinaspedges
    xedges2 = map (\xoutaspedge@(_,(endnodeid,qb)) -> let
                    outbavg = qbiasedavg qb (getqbond xoutaspedge youtaspedges) (getqbond xoutaspedge boutaspedges)
                    in (xnodeid, endnodeid, outbavg)
                    ) xoutaspedges

    in xedges1++xedges2

qmolbiasedavg_' :: (Eq a, QuantaBounded sp, QuantaBounded sp1, QuantaBounded sp2, G.Graph xg, G.Graph yg, G.Graph bg) => (a, G.Node) -> xg a (QBondS sp) -> (yg a (QBondS sp1), [(a, G.Node)]) -> (bg a (QBondS sp2), [(a, G.Node)]) -> [(G.Node, G.Node, QBondS sp)]
qmolbiasedavg_' (asp, xnodeid) xg (yg,ynodes) (bg,bnodes) = let
    xouts = G.lsuc xg xnodeid
    in if length xouts == 0
        then []
        else let
            (_,abond) = head xouts
            xoutavg = avgedges xouts
            youtavg = avgedges' yg asp ynodes True
            boutavg = avgedges' bg asp bnodes True
            mkbond = mkQBondS $ qspan abond
            bvg = qbiasedavg (mkbond xoutavg) (mkbond youtavg) (mkbond boutavg)
            bvgshare = mkbond $ (qval bvg)/(fromIntegral $ length xouts)
            in map (\(endnodeid,_) -> (xnodeid,endnodeid,bvgshare)) xouts

avgedges' :: (G.Graph gr, Eq a, QuantaBounded sp) => gr a (QBondS sp) -> a -> [(a, G.Node)] -> Bool -> Double
avgedges' gr asp nodes isout = case lookup asp nodes of
    Just nodeid -> let
        es = (if isout then G.lsuc gr nodeid else G.lpre gr nodeid)
        in avgedges es
    otherwise -> 0

avgedges :: (QuantaBounded sp) => [(G.Node, QBondS sp)] -> Double
avgedges es = (sumedges es)/(fromIntegral $ length es)

sumedges :: (QuantaBounded sp) => [(G.Node, QBondS sp)] -> Double
sumedges es = foldl (\acc (_,qb) -> acc + (qval qb)) 0 es

getaspedges ::(Eq a, G.Graph gr, QuantaBounded sp) => gr a (QBondS sp) -> a -> [(a,G.Node)] -> ([(a, (G.Node, QBondS sp))], [(a, (G.Node, QBondS sp))])
getaspedges gr a nodes = case lookup a nodes of
    Just nodeid -> (promoteaspedge gr $ G.lpre gr nodeid, promoteaspedge gr $ G.lsuc gr nodeid)
    otherwise -> ([],[])

getqbond :: (Eq a, QuantaBounded bsp, QuantaBounded bsp1) => (a, (G.Node, QBondS bsp)) -> [(a, (G.Node, QBondS bsp1))] -> QBondSInf
getqbond (asp, (_,qbs)) asps = case lookup asp asps of
    Just (_,x) -> mkQBondSInf (qval x)
    otherwise  -> mkQBondSInf 0

promoteaspedge :: (G.Graph gr) => gr a v -> [(G.Node, v)] -> [(a, (G.Node, v))]
promoteaspedge gr es = map (\(n,v) -> let
                                Just other = G.lab gr n
                                in (other, (n,v))
                            ) es
