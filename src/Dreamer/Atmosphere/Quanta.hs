{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE TypeFamilies              #-}

module Dreamer.Atmosphere.Quanta
(
    Quanta(..),
    QuantaBounded(..),
    QFull(..),
    QPos(..),
    QNeg(..),
    QInf(..),
    QNegInf(..),
    QPosInf(..),
    Q3(..)
) where

class (QuantaBounded b) => Quanta a b c where
    data Quantum a b c :: *
    mkQ :: a -> b -> c -> Quantum a b c
    mkQuantum :: a -> b -> c -> Quantum a b c
    mkQuantum = mkQ
    mkQ = mkQuantum
    qtype :: (Quantum a b c) -> a
    qspan :: (Quantum a b c) -> b
    qval :: (Quantum a b c) -> c

class (Show qb, Read qb) => QuantaBounded qb where
    qminbound :: (Num v, Fractional v, Ord v) => qb -> v
    qmaxbound :: (Num v, Fractional v, Ord v) => qb -> v
    qbound :: (Num v, Fractional v, Ord v) => qb -> v -> v

    qbound qb a | (qminbound qb) > a = (qminbound qb)
                | (qmaxbound qb) < a = (qmaxbound qb)
                | otherwise = a

data QFull = QFull deriving (Show, Read)
data QPos = QPos deriving (Show, Read)
data QNeg = QNeg deriving (Show, Read)
data QInf = QInf deriving (Show, Read)
data QPosInf = QPosInf deriving (Show, Read)
data QNegInf = QNegInf deriving (Show, Read)
data Q3 = Q3Pos | Q3Neg | Q3Full deriving (Show, Read)

instance QuantaBounded QFull where
    qminbound QFull = (-1.0)
    qmaxbound QFull = 1.0

instance QuantaBounded QPos where
    qminbound QPos = 1e-20
    qmaxbound QPos = 1.0

instance QuantaBounded QNeg where
    qminbound QNeg = (-1.0)
    qmaxbound QNeg = -1e-20

instance QuantaBounded QInf where
    qminbound QInf = -(1/0)
    qmaxbound QInf = (1/0)

instance QuantaBounded QNegInf where
    qminbound QNegInf = -(1/0)
    qmaxbound QNegInf = -1e-20

instance QuantaBounded QPosInf where
    qminbound QPosInf = 1e-20
    qmaxbound QPosInf = (1/0)

instance QuantaBounded Q3 where
    qminbound Q3Pos  = 1e-20
    qminbound Q3Neg  = (-1.0)
    qminbound Q3Full = (-1.0)

    qmaxbound Q3Pos  = 1.0
    qmaxbound Q3Neg  = -1e-20
    qmaxbound Q3Full = 1.0

-- TODO: Write instances for Quantum objects to be mathematically compatible with raw numbers.
