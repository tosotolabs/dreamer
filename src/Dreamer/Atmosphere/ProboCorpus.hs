{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE TypeFamilies              #-}

module Dreamer.Atmosphere.ProboCorpus
(
    ProboCorpus(..), ProboBreed(..),
    ProbeResult(..),
    DynProboCorpus(..),
    QProbeR,
    QProbeRPos,
    QProbeRNeg,
    QProbeRFull,
    QProbeRInf,
    QProbeRPosInf,
    QProbeRNegInf,
    ProboCorpusPos,

    mkQProbeR,
    mkQProbeRFull,
    mkQProbeRPos,
    mkQProbeRNeg,
    mkQProbeRInf,
    mkQProbeRPosInf,
    mkQProbeRNegInf,

    module Dreamer.Atmosphere.Quanta
) where

import           Dreamer.Atmosphere.Atom
import           Dreamer.Atmosphere.Quanta
import           Dreamer.Metadata
import           Dreamer.Neural.Signature
import           Dreamer.Process

data ProbeResult = ProbeResult deriving (Show, Read)

instance (QuantaBounded b, Num v, Fractional v, Ord v) => Quanta ProbeResult b v where
    data Quantum ProbeResult b v = MkQProbeR b v deriving (Show, Read)
    mkQ _ b v = MkQProbeR b (qbound b v)
    qtype _ = ProbeResult
    qspan (MkQProbeR b _) = b
    qval (MkQProbeR _ v) = v

type QProbeR a = Quantum ProbeResult a Double
type QProbeRPos = QProbeR QPos
type QProbeRNeg = QProbeR QNeg
type QProbeRFull = QProbeR QFull
type QProbeRInf = QProbeR QInf
type QProbeRPosInf = QProbeR QPosInf
type QProbeRNegInf = QProbeR QNegInf

class (Metadata p, State st, Quanta ProbeResult sp v) => ProboCorpus p st sp v where
    data ProboBreed p st sp v :: *
    -- A probo process takes some stateful data and a previous quantum state to produce a new quantum state.
    runprobo :: ProboBreed p st sp v -> st -> Quantum ProbeResult sp v -> Quantum ProbeResult sp v

data DynProboCorpus st sp v = forall p. (ProboCorpus p st sp v, Metadata (ProboBreed p st sp v)) => DynProboCorpus (ProboBreed p st sp v)

type ProboCorpusPos st = DynProboCorpus st QPos Double

instance Metadata (DynProboCorpus st sp v) where
    mdid (DynProboCorpus p) = mdid p
    friendly (DynProboCorpus p) = friendly p
    author (DynProboCorpus p) = author p

mkQProbeR :: (QuantaBounded sp) => sp -> Double -> QProbeR sp
mkQProbeR sp v = mkQ ProbeResult sp v

mkQProbeRFull :: Double -> QProbeRFull
mkQProbeRFull = mkQProbeR QFull

mkQProbeRPos :: Double -> QProbeRPos
mkQProbeRPos = mkQProbeR QPos

mkQProbeRNeg :: Double -> QProbeRNeg
mkQProbeRNeg = mkQProbeR QNeg

mkQProbeRInf :: Double -> QProbeRInf
mkQProbeRInf = mkQProbeR QInf

mkQProbeRPosInf :: Double -> QProbeRPosInf
mkQProbeRPosInf = mkQProbeR QPosInf

mkQProbeRNegInf :: Double -> QProbeRNegInf
mkQProbeRNegInf = mkQProbeR QNegInf

instance Num (Quantum ProbeResult QFull Double) where
    a + b = mkQ ProbeResult QFull $ (qval a) + (qval b)
    a - b = mkQ ProbeResult QFull $ (qval a) - (qval b)
    a * b = mkQ ProbeResult QFull $ (qval a) * (qval b)
    abs = (mkQ ProbeResult QFull) . abs . qval
    negate = (mkQ ProbeResult QFull) . negate . qval
    signum = (mkQ ProbeResult QFull) . signum . qval
    fromInteger = (mkQ ProbeResult QFull) . fromInteger

instance Num (Quantum ProbeResult QPos Double) where
    a + b = mkQ ProbeResult QPos $ (qval a) + (qval b)
    a - b = mkQ ProbeResult QPos $ (qval a) - (qval b)
    a * b = mkQ ProbeResult QPos $ (qval a) * (qval b)
    abs = (mkQ ProbeResult QPos) . abs . qval
    negate = (mkQ ProbeResult QPos) . negate . qval
    signum = (mkQ ProbeResult QPos) . signum . qval
    fromInteger = (mkQ ProbeResult QPos) . fromInteger

instance Num (Quantum ProbeResult QNeg Double) where
    a + b = mkQ ProbeResult QNeg $ (qval a) + (qval b)
    a - b = mkQ ProbeResult QNeg $ (qval a) - (qval b)
    a * b = mkQ ProbeResult QNeg $ (qval a) * (qval b)
    abs a = mkQ ProbeResult QNeg 0.0
    negate = (mkQ ProbeResult QNeg) . negate . qval
    signum = (mkQ ProbeResult QNeg) . signum . qval
    fromInteger = (mkQ ProbeResult QNeg) . fromInteger

instance Fractional (Quantum ProbeResult QFull Double) where
    a/b = mkQ ProbeResult QFull $ (qval a)/(qval b)
    fromRational = (mkQ ProbeResult QFull) . fromRational

instance Fractional (Quantum ProbeResult QPos Double) where
    a/b = mkQ ProbeResult QPos $ (qval a)/(qval b)
    fromRational = (mkQ ProbeResult QPos) . fromRational

instance Fractional (Quantum ProbeResult QNeg Double) where
    a/b = mkQ ProbeResult QNeg $ (qval a)/(qval b)
    fromRational = (mkQ ProbeResult QNeg) . fromRational
