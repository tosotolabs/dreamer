{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Dreamer.Lab
(
    ADAPT(..),
    Deg3Range(..),
    Dim3,
    EngramSignal(..),
    Lab(..),
    LabConfig(..),
    LabID,
    Mod(..),
    ModdingError,
    RawTune(..),
    Tune(..),
    TuningMod(..),

    getmod,
    getmodsignal,
    gettune,

    -- Crew re-exports
    CrewAuth,
    Growth,
    GrowthShare,
    WorkerID
) where

import qualified Data.Map.Strict            as M (Map)
import           Dreamer.Academy
import           Dreamer.Atmosphere.Atom
import           Dreamer.Configuration
import           Dreamer.Conscious
import           Dreamer.Lab.Crew
import           Dreamer.Lab.Type
import           Dreamer.Metadata
import           Dreamer.Mufori
import           Dreamer.Neural.Cerebrum    hiding (link)
import           Dreamer.Neural.NTF
import           Dreamer.Neural.Sense
import           Dreamer.Neural.Transmitter
import           Dreamer.Neural.Type
import           Dreamer.Process
import           GHC.Generics

type ModdingError = String
type Name = String

data RawTune = RawTune RawMod | RawSignalTune [(SenseID, [(AspectID, RawMod)])] deriving Read
data Tune m itd md = Tune m | SignalTune [(DynSense itd md, [(DynAspect itd md, m)])]

data TuningMod m itd md
    = AspectTune (Tune m itd md)
    | SenseIRCTune (Tune m itd md)
    | EnzymeIRCTune (Tune m itd md)
    | TransmMuAmpIRCTune (Tune m itd md)
    | TransmFoAmpIRCTune (Tune m itd md)
    | TransmRIAmpIRCTune (Tune m itd md)
    | TransmAspectScaleIRCTune (Tune m itd md)
    | TransmEmphIRCTune (Tune m itd md)
    | TransmDemphIRCTune (Tune m itd md)
    | TransmBiasIRCTune (Tune m itd md)
    | TransmRankIRCTune (Tune m itd md)
    | TransmContactLimIRCTune (Tune m itd md)

data Mod
    = TuningMod RawMod Deg3Range
    | MkSensoryMod Academy TutorID Deg3Range
    | GraduateMod Deg3Range
    | TransmitterMod Name RawMod Deg3Range MaxStages MaxContact
    | MaxContactMod AcademyID MaxContact
    | MaxStagesMod AcademyID MaxStages
    | RemoveWorker WorkerID
    | GetWorkers
    | DreamerPersist
    | DreamerSleep
    deriving Read

gettune :: TuningMod m itd md -> Tune m itd md
gettune (AspectTune t)               = t
gettune (SenseIRCTune t)             = t
gettune (EnzymeIRCTune t)            = t
gettune (TransmMuAmpIRCTune t)       = t
gettune (TransmFoAmpIRCTune t)       = t
gettune (TransmRIAmpIRCTune t)       = t
gettune (TransmAspectScaleIRCTune t) = t
gettune (TransmEmphIRCTune t)        = t
gettune (TransmDemphIRCTune t)       = t
gettune (TransmBiasIRCTune t)        = t
gettune (TransmRankIRCTune t)        = t
gettune (TransmContactLimIRCTune t)  = t

getmod :: Tune m itd md -> m
getmod (Tune m) = m

getmodsignal :: Tune m itd md -> [(DynSense itd md, [(DynAspect itd md, m)])]
getmodsignal (SignalTune sm) = sm

type LabID = MetaID

type Dim3 = (Double, Double, Double)
data Deg3Range = Deg3Range Dim3 Int deriving (Read, Show)

data EngramSignal v = EngramSignal {engrams :: M.Map MetaID (M.Map MetaID v), metadata :: M.Map String String} deriving (Generic)

data LabConfig = LabConfig {
    labcfg'streambrokers  :: [String],
    labcfg'engramstreamid :: String,
    labcfg'modroomids     :: [String],
    labcfg'modresponseid  :: String,
    labcfg'modgroupid     :: String, -- ^ group ID for the dedicated mod-processing kafka consumer
    labcfg'propagateasync ::Bool,
    -- ^ If this is on, then any number of propagations by the lab are allowed. Otherwise,
    -- only one propagation is allowed at a time. This is of use to the lab only if lab'wakeupasync
    -- is on as the wakeup function will run forever and the lab propagation function we provide
    -- returns at the end of the wakeup function.
    labcfg'fromtheshadows :: Bool,
    -- ^ This enables modding from the outside - courtesy of Apache Kafka
    labcfg'distributed    :: Maybe (Chief, CrewAuth)
}

class (Metadata (l s c n a itd md), ADAPT s c n a itd md) => Lab l s c n a itd md where
    -- | This will setup the environment for the cerebrum to grow i.e. Adam neuron(s) and persistent metadata
    lab'genesis :: l s c n a itd md -> c n a itd md -> DreamerInfo -> IO ()

    -- TODO use CFG file and periodically read and replace DreamerConfig state
    lab'dreamerconfig :: l s c n a itd md -> c n a itd md -> DreamerInfo -> IO (DreamerConfig itd md)

    lab'config :: l s c n a itd md -> c n a itd md -> IO LabConfig

    -- | Aspect Diversity and Aspect Priority Training of the dreamer's neural structures
    lab'training :: l s c n a itd md -> c n a itd md -> (DreamerConfig itd md, LabConfig) -> IO (s c n a itd md)

    -- | Setup in preparation for a dreamer awakening i.e. print welcome message, analytics, etc
    lab'prepawaken :: (Metadata dr) => l s c n a itd md -> c n a itd md -> dr -> (DreamerConfig itd md, LabConfig) -> IO ()

    lab'fromrawtransm :: l s c n a itd md -> c n a itd md -> RawMod -> Deg3Range -> (DreamerConfig itd md, LabConfig) -> IO (Either ModdingError (Transmitter itd md, [NeuralID]))
    lab'fromrawtuning :: l s c n a itd md -> c n a itd md -> TuningMod (Maybe RawMod) itd md -> (DreamerConfig itd md, LabConfig) -> IO (Either ModdingError (TuningMod (Maybe (Tuning QAtomPos QAtomFull)) itd md))

    lab'applytuningmod :: l s c n a itd md -> c n a itd md -> TuningMod RawMod itd md -> Deg3Range -> (DreamerConfig itd md, LabConfig) -> IO (Maybe ModdingError)

    lab'enroll :: l s c n a itd md -> c n a itd md -> Academy -> TutorID -> MaxTutors -> Deg3Range -> (DreamerConfig itd md, LabConfig) -> IO (Maybe ModdingError)
    lab'graduate :: l s c n a itd md -> c n a itd md -> Deg3Range -> (DreamerConfig itd md, LabConfig) -> IO (Maybe ModdingError)

    lab'countinrange :: l s c n a itd md -> c n a itd md -> Deg3Range -> (DreamerConfig itd md, LabConfig) -> IO Int

    -- | Optionally perform last-minute prepping of transmission stage data for engram streaming.
    -- | The sense signals of the result neuron states will be streamed out.
    lab'prepfacets :: l s c n a itd md -> c n a itd md -> [NeuronState itd md] -> (DreamerConfig itd md, LabConfig) -> IO [EngramSignal Double]

    -- | Save the dreamer's state
    lab'persistdreamer :: l s c n a itd md -> c n a itd md -> DreamerInfo -> (DreamerConfig itd md, LabConfig) -> IO ()

class (Cerebrum c n a itd md) => ADAPT s c n a itd md where
    coordinate :: s c n a itd md -> c n a itd md -> Maybe [(WorkerID,GrowthShare)] -> (DreamerConfig itd md, LabConfig) -> IO ()
    adapt :: s c n a itd md -> c n a itd md -> Maybe (Age,Growth) -> (DreamerConfig itd md, LabConfig) -> IO (Maybe Growth)
