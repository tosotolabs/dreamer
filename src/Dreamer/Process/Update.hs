{-# LANGUAGE FlexibleContexts #-}

module Dreamer.Process.Update
(
    ProcessUpdate(..),
    UpdateBareProcess(..),
    UpdateCoreProcess(..),
    UpdateCoreProcess2(..),
    UpdateComboProcess2(..),
    UpdateMFRProcess2(..),
    UpdateIntuitiveProcess2(..),

    runupdcombo,
    runupdcombo2,

    module Dreamer.Process,

    Timestamp,
) where

import           Dreamer.Atmosphere.Atom
import           Dreamer.Internal.Operation
import           Dreamer.Process

data ProcessUpdate d v = PUpd {
    pudetails :: d v,
    punovelty :: QAtomFull
} deriving (Show)

type UpdateBareProcess v = BareProcess v -> v -> ProcessUpdate BareProcess v
type UpdateCoreProcess v = CoreProcess v -> v -> ProcessUpdate CoreProcess v

data UpdateCoreProcess2 v = UpdCore2 {
    updcorevalue2 :: CoreProcess v -> v -> ProcessUpdate CoreProcess v,
    updcoretimes2 :: BareProcess QAtomPosInf -> QAtomPosInf -> ProcessUpdate BareProcess QAtomPosInf,
    updcorestamp2 :: BareProcess Timestamp -> Timestamp -> ProcessUpdate BareProcess Timestamp,
    updcoretotal2 :: BareProcess QAtomPosInf -> QAtomPosInf -> ProcessUpdate BareProcess QAtomPosInf,
    updcorechange2 :: BareProcess QAtomPos -> QAtomPos -> ProcessUpdate BareProcess QAtomPos
}

data UpdateComboProcess2 v a = UpdCombo2 {
    updcomboambig2 :: BareProcess a -> a -> ProcessUpdate BareProcess a,
    updcombocore2 :: UpdateCoreProcess2 v,
    updcombocurrpos2 :: BareProcess QAtomPosInf -> QAtomPosInf -> ProcessUpdate BareProcess QAtomPosInf
}

data UpdateMFRProcess2 v a = UpdMFR2 {
    updmfrcurrval2 :: BareProcess v -> v -> ProcessUpdate BareProcess v,
    updmframp2     :: UpdateCoreProcess2 QAtomFull,
    updmfrcombo2   :: UpdateComboProcess2 v a
}

data UpdateIntuitiveProcess2 v a = UpdIntuitive2 {
    updintuicurrval2 :: UpdateBareProcess v,
    updintuicore2    :: UpdateCoreProcess2 v,
    updintuimfr2     :: UpdateMFRProcess2 v a
}

type ComboElemLim = Int
type Q a sp = Quantum a sp Double

runupdcombo :: (Quanta a sp Double) => ComboProcess (Q a sp) (QAtomPos) -> Q a sp -> ComboElemLim -> CoreProcess (Q a sp) -> UpdateCoreProcess (Q a sp) -> ProcessUpdate (ComboProcess (Q a sp)) QAtomPos
runupdcombo combo q elemlim defcore updcore = let
    ambig' = ambig combo
    (elems', novelty) = if (length $ elems combo) < elemlim
        then ((elems combo) ++ [defcore], 0)
        else let
            splitpoint = ((floor . qval . currpos) combo) `mod` elemlim
            (aelems, belems) = splitAt splitpoint $ elems combo
            modpd = (elems combo) !! splitpoint
            (PUpd core novelty') = updcore modpd q
            in ((take ((length aelems)-1) aelems) ++ [core] ++ belems, novelty')
    in PUpd (combo {ambig=mkQ (qtype ambig') (qspan ambig') $ atempdiversity $ map (qval . value) elems', elems=elems', currpos=mkQAtomPosInf $ fi $ (((floor . qval . currpos) combo) + 1) `mod` elemlim}) novelty

runupdcombo2 :: (Quanta a sp Double) => ComboProcess2 v (Q a sp) -> ComboProcess v (Q a sp) -> UpdateComboProcess2 v (Q a sp) -> ProcessUpdate (ComboProcess2 v) (Q a sp)
runupdcombo2 combo2 combo updc = let
    PUpd currpos2' _ = updcombocurrpos2 updc (currpos2 combo2) (currpos combo)
    comboelems = elems combo
    updcc = updcombocore2 updc
    elems2' = foldl (\acc (i,cd2) ->
                    if i >= (length comboelems)
                        then acc ++ [cd2]
                        else
                            let cd = comboelems !! i
                                PUpd value2' _ = updcorevalue2 updcc (value2 cd2) (value cd)
                                PUpd times2' _ = updcoretimes2 updcc (times2 cd2) (times cd)
                                PUpd stamp2' _ = updcorestamp2 updcc (stamp2 cd2) (stamp cd)
                                PUpd total2' _ = updcoretotal2 updcc (total2 cd2) (total cd)
                                PUpd change2' _ = updcorechange2 updcc (change2 cd2) (bvalue $ change cd)
                                cd2' = cd2 {value2=value2', times2=times2', stamp2=stamp2', total2=total2', change2=change2'}
                                in acc ++ [cd2']
                        ) [] (zip ([0..] :: [Int]) (elems2 combo2))
    in PUpd (combo2 {elems2=elems2', currpos2=currpos2'}) $ mkQAtomFull 0
