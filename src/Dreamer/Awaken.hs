{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}

-- TODO use C pointers and C arrays to manage transmission

module Dreamer.Awaken
(
    EngramSignal(..),
    wakeup
) where

import           Control.Concurrent
import           Control.Concurrent.Async
import           Control.Concurrent.STM
import           Control.Concurrent.Suspend
import           Control.Concurrent.Timer
import           Control.Distributed.Process              (SendPort)
import           Control.Exception                        (catch,
                                                           displayException)
import           Control.Lens
import           Control.Monad                            (foldM, foldM_, forM_,
                                                           forever, mapM, void,
                                                           zipWithM)
import           Data.Aeson
import qualified Data.ByteString.Char8                    as B (ByteString,
                                                                pack, unpack)
import qualified Data.ByteString.Lazy.Char8               as BL (unpack)
import qualified Data.List                                as L
import qualified Data.Map.Lazy                            as M
import qualified Data.Set                                 as S
import           Data.Word                                (Word8)
import           Dreamer.Academy
import           Dreamer.Atmosphere.Atom
import           Dreamer.Atmosphere.Molecule
import           Dreamer.Atmosphere.ProboCorpus
import           Dreamer.Configuration
import           Dreamer.Conscious
import           Dreamer.Internal.Operation
import           Dreamer.Lab
import           Dreamer.Lab.Crew
import           Dreamer.Lab.Type
import           Dreamer.Metadata
import           Dreamer.Neural.Cerebrum
import           Dreamer.Neural.Cerebrum.Structure.Axon
import           Dreamer.Neural.Cerebrum.Structure.Neuron
import           Dreamer.Neural.NTF
import           Dreamer.Neural.Sense
import           Dreamer.Neural.Signature
import           Dreamer.Neural.Transmitter
import           Dreamer.Neural.Type
import           Dreamer.Process
import           Kafka.Consumer                           as C
import           Kafka.Producer                           as P
import           System.Environment                       (getEnv)
import           System.IO
import           System.IO.Error                          (isDoesNotExistError)
import           Text.Read

data TMVarState = Ready | Waiting | Running
data Runs = Runs Int

instance (ToJSON v) => ToJSON (EngramSignal v) where
    toEncoding = genericToEncoding defaultOptions

instance (FromJSON v) => FromJSON (EngramSignal v) where

-- TODO store lessons on disk
wakeup :: (Conscious dr c n a itd md, Lab l s c n a itd md)
        => dr c n a itd md
        -> l s c n a itd md
        -> DreamerInfo
        -> IO ((MaxStages, MaxContact) -> (Transmitter itd md, [NeuralID]) -> IO (), (TMVar Runs, TMVar (DreamerConfig itd md)))
wakeup dr lab drinfo = do
    let cerebrum = dr'cerebrum dr
    drmrcfg <- lab'dreamerconfig lab cerebrum drinfo
    labcfg <- lab'config lab cerebrum
    training <- lab'training lab cerebrum (drmrcfg,labcfg)

    let drmrlog = drmrcfg'accesslogger drmrcfg
        errorlog = drmrcfg'errorlogger drmrcfg
        brokers = map BrokerAddress $ labcfg'streambrokers labcfg

    (academies,acdm'fchint) <- foldM (\(acdmacc,afacc) (acdmid,acadmcfg) -> do
                                tuts <- atomically $ newTMVar M.empty
                                return (acdmacc++[(acdmid,(tuts,acadmcfg'groupid acadmcfg))], afacc++[(acdmid, acadmcfg'fetchinterval acadmcfg)])
                                ) ([],[]) $ drmrcfg'academies drmrcfg
    tmvardrmrcfg <- atomically $ newTMVar drmrcfg
    lessondrainerinits <- mapM (\(acdmid,(_,groupid)) -> do
                            drainer <- newConsumer (consumerprops groupid brokers) (consumersub [acdmid])
                            return (drainer,"Lesson Drainer for Academy "++acdmid)
                            ) academies
    let fromtheshadows = labcfg'fromtheshadows labcfg
    drainerinits <- if fromtheshadows
                        then do
                            modprocessorinit <- newConsumer (consumerprops (labcfg'modgroupid labcfg) brokers) (consumersub $ labcfg'modroomids labcfg)
                            return $ (modprocessorinit, "Mod Processor"):lessondrainerinits
                        else return lessondrainerinits
    engramstreamerinit <- newProducer $ producerprops brokers
    streamerinits <- if fromtheshadows
                        then do
                            modprocessorinit <- newProducer $ producerprops brokers
                            return [(engramstreamerinit, "Engram Streamer"), (modprocessorinit, "Mod Status Streamer")]
                        else return [(engramstreamerinit, "Engram Streamer")]
    (drainerfailures, drainers) <- foldM (\(fct,dacc) (drainerinit,dname) ->
                                    case drainerinit of
                                        Left e -> do
                                            errorlog $ dname ++ " failed to initialize - " ++ show e
                                            return (fct+1,dacc)
                                        Right drainer -> return (fct,dacc++[(drainer,dname)])
                                    ) (0,[]) drainerinits
    (streamerfailures, streamers) <- foldM (\(fct, sacc) (streamerinit,sname) ->
                                        case streamerinit of
                                            Left e -> do
                                                errorlog $ sname ++ " failed to initialize - " ++ show e
                                                return (fct+1,sacc)
                                            Right streamer -> return (fct,sacc++[(streamer,sname)])
                                        ) (0,[]) streamerinits

    agegrowthstate <- atomically $ newTMVar $ drmrinfo'agegrowth $ drmrcfg'drmrinfo drmrcfg

    (chiefserving,mbcrewstate,mbchiefsend) <- case labcfg'distributed labcfg of
        Nothing -> return (False,Nothing,Nothing)
        Just (chief,crewauth) -> do

            crewstate <- atomically $ newTMVar M.empty
            delwidstate <- atomically $ newTMVar S.empty

            let wkrmsgact = (\wkrmsg -> case wkrmsg of
                                DidAdapt age mbgrowth _ _ -> case mbgrowth of
                                    Just g -> do
                                        agegrowth <- atomically $ takeTMVar agegrowthstate
                                        drmrcfg' <- atomically $ takeTMVar tmvardrmrcfg
                                        let drinfo = drmrcfg'drmrinfo drmrcfg'
                                            drage = drmrinfo'age drinfo
                                            fullgrowth = (neurongrowth cerebrum drage) - (neurongrowth cerebrum $ drage - (drmrcfg'agingincrement drmrcfg'))
                                        if agegrowth + g == (floor $ qval fullgrowth)
                                            then atomically $ putTMVar agegrowthstate 0
                                            else atomically $ putTMVar agegrowthstate $ agegrowth+g
                                        let drtage = (drmrinfo'trueage drinfo) + (mkQAtomPosInf $ (fromIntegral g)/(qval fullgrowth))
                                            drinfo' = drinfo {drmrinfo'trueage=drtage}
                                        atomically $ putTMVar tmvardrmrcfg $ drmrcfg' {drmrcfg'drmrinfo=drinfo'}
                                    otherwise -> return ()
                                JobsUpdate js wid _ -> do
                                    crew <- atomically $ takeTMVar crewstate
                                    case M.lookup wid crew of
                                        Nothing -> atomically $ putTMVar crewstate crew
                                        Just (a,b,c,_) -> do
                                            if js == 0
                                                then do
                                                    delwids <- atomically $ takeTMVar delwidstate
                                                    if wid `S.member` delwids
                                                        then do
                                                            delwids <- atomically $ takeTMVar delwidstate
                                                            atomically $ putTMVar delwidstate $ S.delete wid delwids
                                                            atomically $ putTMVar crewstate $ M.delete wid crew
                                                            drmrlog $ "Removed worker, " ++ wid
                                                        else do
                                                            atomically $ putTMVar delwidstate delwids
                                                            atomically $ putTMVar crewstate $ M.insert wid (a,b,c,js) crew
                                                else atomically $ putTMVar crewstate $ M.insert wid (a,b,c,js) crew
                                DidCoordinate _ _ _ -> return () -- TODO support this later
                                otherwise -> return ()
                                )

            let serve = do
                            void $ getEnv crewauth
                            cs <- chiefserve chief crewstate crewauth wkrmsgact (drmrlog,errorlog)
                            case cs of
                                Left err -> errorlog err >> putStrLn err >> return (False,Nothing,Nothing)
                                Right send -> return (True,Just (chief,crewstate,delwidstate),Just send)

            serve `catch` \e -> if isDoesNotExistError e
                                    then do
                                        errorlog $ "Provided crew authentication environment variable ("++crewauth++") is non-existent"
                                        return (False,Nothing,Nothing)
                                    else error $ displayException e

    if (drainerfailures > 0) || (streamerfailures > 0)
        then do
            mapM_ (closeProducer . fst) streamers
            mapM_ (closeConsumer . fst) drainers -- TODO log close errors
            error "Failed to connect you to the dream stream. Check the error log."
        else do
            case labcfg'distributed labcfg of
                Nothing -> do
                    lab'prepawaken lab cerebrum dr (drmrcfg,labcfg)
                    dream cerebrum training lab drainers streamers (academies,acdm'fchint) agegrowthstate mbcrewstate mbchiefsend tmvardrmrcfg labcfg (drmrlog,errorlog)
                Just _ -> if chiefserving
                    then do
                        lab'prepawaken lab cerebrum dr (drmrcfg,labcfg)
                        dream cerebrum training lab drainers streamers (academies,acdm'fchint) agegrowthstate mbcrewstate mbchiefsend tmvardrmrcfg labcfg (drmrlog,errorlog)
                    else do
                        mapM_ (closeProducer . fst) streamers
                        mapM_ (closeConsumer . fst) drainers -- TODO log close errors
                        error "Failed to start the lab chief's server for distributed lab. Check the error log."
    where
        dream cerebrum training lab drainers streamers (academies,acdm'fchint) agegrowthstate mbcrewstate mbchiefsend tmvardrmrcfg labcfg (drmrlog,errorlog) = do
            drmrcfg <- atomically $ readTMVar tmvardrmrcfg
            adaptstate <- atomically $ newTMVar Ready
            agingstate <- atomically $ newTMVar Waiting
            coordstate <- atomically $ newTMVar Ready
            agingtimer <- repeatedTimer (void $ atomically $ swapTMVar agingstate Ready) $ msDelay $ floor $ qval $ drmrcfg'aginginterval drmrcfg
            -- TODO update timer functions for adapt/coordinate to go through workers and tell them what to do
            adaptationtimer <- repeatedTimer (performadaptation training cerebrum adaptstate agingstate mbcrewstate mbchiefsend agegrowthstate tmvardrmrcfg labcfg (drmrlog,errorlog)) $ msDelay $ floor $ qval $ drmrcfg'adaptinterval drmrcfg
            coordinatortimer <- repeatedTimer (performcoordination training cerebrum coordstate mbcrewstate tmvardrmrcfg labcfg (drmrlog,errorlog)) $ msDelay $ floor $ qval $ drmrcfg'coordinateinterval drmrcfg

            let fromtheshadows = labcfg'fromtheshadows labcfg

                (mbmodprocessor,lessondrainers) = if fromtheshadows
                    then case drainers of mp:lds -> (Just $ fst mp, map fst lds)
                    else (Nothing, map fst drainers)
                (mbmodstatusstreamer,engramstreamer) = if fromtheshadows
                then case streamers of es:ms:[] -> (Just $ fst ms, fst es)
                    else case streamers of es:[] -> (Nothing, fst es)

            fetchtimers <- zipWithM (\drainer (acdmid,fchint) -> repeatedTimer (void $ async $ fetchlessons (acdmid, fst $ getmaybe' $ lookup acdmid academies) drainer tmvardrmrcfg (drmrlog,errorlog)) $ msDelay $ floor $ qval $ fchint) lessondrainers acdm'fchint

            if fromtheshadows
                then do
                    let modprocessor = getmaybe' mbmodprocessor
                        modstatusstreamer = getmaybe' mbmodstatusstreamer
                    tmvarscdprpgstate <- atomically $ newTMVar Ready
                    initmodprocessor
                        modprocessor
                        lab
                        tmvarscdprpgstate
                        tmvardrmrcfg
                        labcfg
                        modstatusstreamer
                        mbcrewstate
                        (cerebrum, engramstreamer)
                        (\drinfo -> do drmrcfg' <- atomically $ readTMVar tmvardrmrcfg; lab'persistdreamer lab cerebrum drinfo (drmrcfg',labcfg))
                        (sleep (\drinfo drmrcfg' -> lab'persistdreamer lab cerebrum drinfo (drmrcfg',labcfg)) tmvardrmrcfg (streamers,drainers) (agingtimer:adaptationtimer:coordinatortimer:fetchtimers) (drmrlog,errorlog))
                        (drmrlog,errorlog)
                else return ()

            mapM_ (\(acdmid,(tuts,_)) -> void $ async $ forever $ propagate lab cerebrum (acdmid,tuts) tmvardrmrcfg labcfg engramstreamer (drmrlog,errorlog) ) academies

            drmrlog $ (unString255 $ friendly dr)++" has awakened."

            tmvarlabprpgstate <- atomically $ newTMVar $ Runs 0
            return (labpropagate lab cerebrum tmvarlabprpgstate tmvardrmrcfg labcfg engramstreamer (drmrlog,errorlog) (labcfg'propagateasync labcfg), (tmvarlabprpgstate, tmvardrmrcfg))
        performadaptation :: ADAPT s c n a itd md
                            => s c n a itd md
                            -> c n a itd md
                            -> TMVar TMVarState
                            -> TMVar TMVarState
                            -> Maybe (Chief,Crew,TMVar (S.Set WorkerID))
                            -> Maybe (SendPort ChiefMessage -> ChiefMessage -> IO ())
                            -> TMVar Int
                            -> TMVar (DreamerConfig itd md)
                            -> LabConfig
                            -> (Logger, ErrorLogger)
                            -> IO ()
        performadaptation training cerebrum tmvaradaptstate tmvaragingstate mbcrewstate mbchiefsend agegrowthstate tmvardrmrcfg labcfg (drmrlog,errorlog) = do

            adaptstate <- atomically $ readTMVar tmvaradaptstate
            case adaptstate of
                Waiting -> drmrlog "|Adaptation overlap"
                otherwise -> do
                    void $ atomically $ swapTMVar tmvaradaptstate Waiting
                    agingstate <- atomically $ readTMVar tmvaragingstate
                    drmrcfg <- atomically $ readTMVar tmvardrmrcfg
                    void $ async $ do
                        case agingstate of
                            Waiting -> do
                                drmrlog "Performing adaptation"
                                case mbcrewstate of
                                    Nothing -> return ()
                                    Just (_,crewstate,_) -> do
                                        crew <- atomically $ readTMVar crewstate
                                        let drage = drmrinfo'age $ drmrcfg'drmrinfo drmrcfg
                                            chiefsend = getmaybe' mbchiefsend
                                        mapM_ (\(_,sp,_,_) -> chiefsend sp (Adapt (qval drage) Nothing)) $ M.elems crew

                                void $ adapt training cerebrum Nothing (drmrcfg,labcfg)

                            otherwise -> do

                                agegrowth <- atomically $ readTMVar agegrowthstate

                                if agegrowth > 0
                                    then do
                                        drmrlog "Waiting for all workers to be finished with age-adapt. Will perform simple-adapt instead."
                                        case mbcrewstate of
                                            Nothing -> return ()
                                            Just (_,crewstate,_) -> do
                                                crew <- atomically $ readTMVar crewstate
                                                let drage = drmrinfo'age $ drmrcfg'drmrinfo drmrcfg
                                                    chiefsend = getmaybe' mbchiefsend
                                                mapM_ (\(_,sp,_,_) -> chiefsend sp (Adapt (qval drage) Nothing)) $ M.elems crew
                                        void $ adapt training cerebrum Nothing (drmrcfg,labcfg)
                                    else do
                                        drmrlog "Performing age-adapt"
                                        void $ atomically $ swapTMVar tmvaragingstate Waiting
                                        drmrcfg' <- atomically $ takeTMVar tmvardrmrcfg

                                        let drinfo = drmrcfg'drmrinfo drmrcfg'
                                            drage = mkQAtomPosInf $ (qval $ drmrinfo'age drinfo) + (qval $ drmrcfg'agingincrement drmrcfg')
                                            ngrowth = (neurongrowth cerebrum drage) - (neurongrowth cerebrum $ drmrinfo'age drinfo)
                                            drinfo' = drinfo {drmrinfo'age=drage}
                                            drmrcfg'' = drmrcfg' {drmrcfg'drmrinfo=drinfo'}

                                        atomically $ putTMVar tmvardrmrcfg drmrcfg''

                                        mbgrowth <- case mbcrewstate of
                                            Nothing -> adapt training cerebrum (Just (drage,floor $ qval ngrowth)) (drmrcfg'',labcfg)
                                            Just (chief,crewstate,_) -> do
                                                crew <- atomically $ readTMVar crewstate
                                                let chiefsend = getmaybe' mbchiefsend
                                                    crew' = M.elems crew
                                                    crewgs = (chiefgrowthshare chief):(map (\(_,_,gs,_) -> gs) crew')
                                                    gssum_ = sum crewgs
                                                    gssum = if gssum_ <= 0 then 1 else gssum_
                                                    lencrew = (length crew) + 1 -- +1 for chief
                                                    ngrowth_ = fromIntegral $ floor $ qval ngrowth
                                                    growths = L.unfoldr (\(i,ng) ->
                                                                            if i == lencrew
                                                                                then Nothing
                                                                                else if i == (lencrew-1)
                                                                                        then Just (ng,(i+1, 0))
                                                                                        else let
                                                                                            gs = max 0 $ crewgs !! i
                                                                                            g = round $ ((fromIntegral gs)/(fromIntegral gssum)) * ngrowth_
                                                                                            in Just (if ng-g < 0 then ng else g, (i+1, max 0 $ ng-g))
                                                                            ) (0, floor $ qval ngrowth)

                                                mapM_ (\((_,sp,_,_),growth) -> chiefsend sp (Adapt (qval drage) (Just growth))) $ zip crew' $ tail growths

                                                adapt training cerebrum (Just (drage, head growths)) (drmrcfg'',labcfg)

                                        case mbgrowth of
                                            Nothing -> return ()
                                            Just g -> do
                                                agegrowth' <- atomically $ takeTMVar agegrowthstate
                                                drmrcfg''' <- atomically $ takeTMVar tmvardrmrcfg
                                                let drinfo' = drmrcfg'drmrinfo drmrcfg'''
                                                    drage' = drmrinfo'age drinfo'
                                                    ngrowth' = (neurongrowth cerebrum drage') - (neurongrowth cerebrum $ drage' - (drmrcfg'agingincrement drmrcfg'''))
                                                if agegrowth'+g >= (floor $ qval ngrowth')
                                                    then atomically $ putTMVar agegrowthstate 0
                                                    else atomically $ putTMVar agegrowthstate $ agegrowth'+g
                                                let drtage = (drmrinfo'trueage drinfo') + (mkQAtomPosInf $ (fromIntegral g)/(qval ngrowth'))
                                                    drinfo'' = drinfo' {drmrinfo'trueage=drtage}
                                                atomically $ putTMVar tmvardrmrcfg $ drmrcfg''' {drmrcfg'drmrinfo=drinfo''}

                                        void $ atomically $ swapTMVar tmvaragingstate Ready

                        void $ atomically $ swapTMVar tmvaradaptstate Ready
        performcoordination :: ADAPT s c n a itd md
                            => s c n a itd md
                            -> c n a itd md
                            -> TMVar TMVarState
                            -> Maybe (Chief,Crew,TMVar (S.Set WorkerID))
                            -> TMVar (DreamerConfig itd md)
                            -> LabConfig
                            -> (Logger, ErrorLogger)
                            -> IO ()
        performcoordination training cerebrum tmvarcoordstate mbcrewstate tmvardrmrcfg labcfg (drmrlog,errorlog) = do
            coordstate <- atomically $ readTMVar tmvarcoordstate
            case coordstate of
                Waiting -> drmrlog "|Coordination overlap"
                otherwise -> do
                    drmrcfg <- atomically $ readTMVar tmvardrmrcfg
                    drmrlog "Performing coordination"
                    void $ atomically $ swapTMVar tmvarcoordstate Waiting
                    case mbcrewstate of
                        Nothing -> coordinate training cerebrum Nothing (drmrcfg,labcfg)
                        Just (chief,crewstate,_) -> do
                            crew <- atomically $ readTMVar crewstate
                            flip (coordinate training cerebrum) (drmrcfg,labcfg) $ Just $ (chiefid chief,chiefgrowthshare chief):(map (\(wid,(_,_,gs,_)) -> (wid,gs)) $ M.assocs crew)
                    void $ atomically $ swapTMVar tmvarcoordstate Ready

sleep :: (IntuitiveProcess itd md QAtomPos)
        => (DreamerInfo -> DreamerConfig itd md -> IO ())
        -> TMVar (DreamerConfig itd md)
        -> ([(KafkaProducer, String)], [(KafkaConsumer, String)])
        -> [TimerIO]
        -> (Logger, ErrorLogger)
        -> IO ()
sleep savedr tmvardrmrcfg (streamers, drainers) timers (drmrlog,errorlog) = do
    drmrcfg <- atomically $ readTMVar tmvardrmrcfg
    savedr (drmrcfg'drmrinfo drmrcfg) drmrcfg
    mapM_ (\(streamer, sname) -> do
        closeProducer streamer
        drmrlog $ "Closed streamer, "++sname
        ) streamers
    mapM_ (\(drainer, dname) -> do
        mberr <- closeConsumer drainer
        case mberr of
            Nothing -> drmrlog $ "Closed drainer, "++dname
            Just err -> errorlog $ "Failed to close drainer, "++dname++": "++(show err)
        ) drainers
    mapM_ stopTimer timers

type EngramStreamer = KafkaProducer

propagate :: Lab l s c n a itd md
            => l s c n a itd md
            -> c n a itd md
            -> (AcademyID, TMVar (M.Map TutorID Lesson))
            -> TMVar (DreamerConfig itd md)
            -> LabConfig
            -> EngramStreamer
            -> (Logger, ErrorLogger)
            -> IO ()
propagate lab cerebrum (academyid,tmvarlessons) tmvardrmrcfg labcfg streamer (drmrlog,errorlog) = do
    ctime <- getnanotime
    drmrcfg <- atomically $ readTMVar tmvardrmrcfg
    sns <- sensoryneurons cerebrum drmrcfg
    let sns' = filter (((==) academyid) . (fst . fst . snd)) sns
        snids = S.fromList $ map (uniqueid . fst) sns'
        tstamp = mkQAtomPosInf ctime
        maxstages = acadmcfg'maxstages $ getmaybe' $ lookup academyid $ drmrcfg'academies drmrcfg
        maxcontact = acadmcfg'maxcontact $ getmaybe' $ lookup academyid $ drmrcfg'academies drmrcfg
        academyaspects = acadmcfg'aspects $ getmaybe' $ lookup academyid $ drmrcfg'academies drmrcfg
        engresid = acadmcfg'engramstreamid $ getmaybe' $ lookup academyid $ drmrcfg'academies drmrcfg
        deflesson = acadmcfg'deflesson $ getmaybe' $ lookup academyid $ drmrcfg'academies drmrcfg
    snmorphresults <- mapConcurrently (\(sn,((acdmid,senseid),tutorid)) -> do

                        lessons <- atomically $ takeTMVar tmvarlessons

                        ((aid,(av,subasps_)),parts_) <- case M.lookup tutorid lessons of
                            Nothing -> do
                                atomically $ putTMVar tmvarlessons $ M.insert tutorid deflesson lessons
                                return deflesson
                            Just l@((aid',(_,subasps')),parts') -> do
                                -- Sensory neurons will gradually forget the last lesson at times it is not
                                -- taught, hence the zero replacement. Silence/darkness is stimulating.
                                -- TODO create a dreamer whose neurons constantly study the last lesson until
                                -- a new one is taught in the classroom.
                                atomically $ putTMVar tmvarlessons $ M.insert tutorid ((aid',(0, map (\(aspid,_) -> (aspid,0)) subasps')), map (\(aid'',(_,subasps'')) -> (aid',(0, map (\(aspid,_) -> (aspid,0)) subasps''))) parts') lessons
                                return l

                        stimuli <- foldM (\acc (aspid,(aspv,subasps)) -> do
                                        case lookup aspid academyaspects of
                                            Nothing -> do
                                                errorlog "Unknown content (bad aspect id)"
                                                return acc
                                            Just asp -> do
                                                subasps' <- foldM (\acc' (aspid',aspv') -> do
                                                                case lookup aspid' academyaspects of
                                                                    Nothing -> do
                                                                        errorlog "Unknown content (bad subaspect id)"
                                                                        return acc'
                                                                    Just subasp -> return $ (subasp, mkQAtomFull aspv'):acc'
                                                                ) [] subasps >>= return . reverse
                                                return $ (asp, (mkQAtomFull aspv, subasps')):acc
                                        ) [] ((aid,(av,subasps_)):parts_) >>= return . reverse

                        aspecttune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (AspectTune $ SignalTune $ aspecttune sn) >>= either (\moderr -> do errorlog moderr; return $ map (\(sen,asps) -> (sen,zip asps $ cycle [Nothing])) $ drmrcfg'senses drmrcfg) (return . getmodsignal . gettune)
                        senseirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (SenseIRCTune $ Tune $ senseirctune sn) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                        enzymeirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (EnzymeIRCTune $ Tune $ enzymeirctune sn) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                        tmuampirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmMuAmpIRCTune $ Tune $ tmuampirctune sn) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                        tfoampirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmFoAmpIRCTune $ Tune $ tfoampirctune sn) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                        triampirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmRIAmpIRCTune $ Tune $ triampirctune sn) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                        taspscaleirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmAspectScaleIRCTune $ Tune $ taspscaleirctune sn) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                        temphirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmEmphIRCTune $ Tune $ temphirctune sn) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                        tdemphirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmDemphIRCTune $ Tune $ tdemphirctune sn) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                        tbiasirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmBiasIRCTune $ Tune $ tbiasirctune sn) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                        trankirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmRankIRCTune $ Tune $ trankirctune sn) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                        tcontactlimirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmContactLimIRCTune $ Tune $ tcontactlimirctune sn) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)

                        let stgidx = mkQAtomPosInf 0
                            tmeta = TMeta {
                                        tmeta'stageindex = stgidx,
                                        tmeta'timestamp = tstamp,
                                        tmeta'maxstages = maxstages,
                                        tmeta'maxcontact = maxcontact,
                                        tmeta'aspecttune = aspecttune',
                                        tmeta'senseirctune = senseirctune',
                                        tmeta'enzymeirctune = enzymeirctune',
                                        tmeta'tmuampirctune = tmuampirctune',
                                        tmeta'tfoampirctune = tfoampirctune',
                                        tmeta'triampirctune = triampirctune',
                                        tmeta'taspscaleirctune = taspscaleirctune',
                                        tmeta'temphirctune = temphirctune',
                                        tmeta'tdemphirctune = tdemphirctune',
                                        tmeta'tbiasirctune = tbiasirctune',
                                        tmeta'trankirctune = trankirctune',
                                        tmeta'tcontactlimirctune = tcontactlimirctune'
                                    }
                            nvisitstate = NeuronState (uniqueid sn) (senses sn) (ntfs sn) (activetransmavg sn)
                            neuntfs = neustatentfs nvisitstate
                            ntfsvisit = map (\(DynNTF ntf,auxs) -> ntf'runneuvisit ntf (DynNTF ntf, getmaybe' $ lookup (DynNTF ntf) neuntfs) nvisitstate tstamp) $ drmrcfg'neuronntfs drmrcfg
                            neupremorphstate = nvisitstate {neustatentfs=ntfsvisit}
                            morphres@(MorphResult sn' (STMorph stmframp' staspscale',_) (mrcl,_)) = sensorymorph sn neupremorphstate stimuli drmrcfg tmeta
                            neupostmorphstate = NeuronState (uniqueid sn') (senses sn') ntfsvisit (activetransmavg sn') -- morphosis shouldn't cause NTF state change
                            postmorph = PostMorph neupostmorphstate staspscale' (stmuamp stmframp') (stfoamp stmframp') (striamp stmframp') mrcl
                            ntfspostmorph = map (\(DynNTF ntf,auxs) -> ntf'runneupostmorph ntf (DynNTF ntf, getmaybe' $ lookup (DynNTF ntf) ntfsvisit) postmorph tstamp) $ drmrcfg'neuronntfs drmrcfg
                            neuprecorrstate = neupostmorphstate {neustatentfs=ntfspostmorph}

                        return (uniqueid sn',(morphres,neuprecorrstate,tmeta))
                        ) sns

    void $ async $ do
        facets <- flip (lab'prepfacets lab cerebrum) (drmrcfg,labcfg) $ map (\(_,(_,nstate,_)) -> nstate) snmorphresults
        sendengrams facets streamer engresid (drmrlog,errorlog)

    sncontactlimits <- limitcontacts cerebrum (map (\(snid,(MorphResult _ _ (mrcl,_),_,_)) -> (snid,mrcl)) snmorphresults) (maxstages,maxcontact) drmrcfg

    sncontacts <- mapConcurrently (\(snid,cl) -> do
                    let (MorphResult sn mrsimp (mrcl,clnov),neustate,tmeta) = getmaybe' $ lookup snid snmorphresults
                    ContactResult sn_ axons'scores transm <- sensorycontact cerebrum sn neustate cl snids drmrcfg tmeta
                    sn' <- refreshneuron cerebrum sn_ neustate mrsimp (map snd axons'scores) (mrcl,clnov) tmeta drmrcfg
                    let axsts = map (\(ax,_) -> AxonState (uniqueid ax) (ntfs ax) (neusenses ax) (neuntfs ax)) axons'scores
                        ntfspostcontact = map (\(DynNTF ntf,auxs) -> ntf'runneupostcontact ntf (DynNTF ntf, getmaybe' $ lookup (DynNTF ntf) (neustatentfs neustate)) neustate axsts tstamp) $ drmrcfg'neuronntfs drmrcfg
                        neustatepostcontact = neustate {neustatentfs=ntfspostcontact}
                    updateneuron cerebrum sn' neustatepostcontact drmrcfg
                    snendneus <- mapConcurrently (\(ax,corrinfo) -> do
                                    let axntfs = ntfs ax
                                        axstate = AxonState (uniqueid ax) axntfs (neusenses ax) (neuntfs ax)
                                    endneuron <- getneuron cerebrum (enid ax) drmrcfg
                                    let endneuronstate = NeuronState (uniqueid endneuron) (senses endneuron) (ntfs endneuron) (activetransmavg endneuron)
                                        axntfspostcontact = map (\(DynNTF ntf,auxs) -> ntf'runaxonpostcontact ntf (DynNTF ntf, getmaybe' $ lookup (DynNTF ntf) axntfs) axstate (neustatepostcontact,endneuronstate) corrinfo tstamp) $ drmrcfg'axonntfs drmrcfg
                                        axstatepostcontact = axstate {axonstatentfs=axntfspostcontact}
                                    updateaxon cerebrum ax axstatepostcontact drmrcfg
                                    return (endneuronstate,axstatepostcontact)
                                    ) axons'scores
                    return (snendneus,mrsimp,transm)
                    ) sncontactlimits

    -- :: M.Map NeuralID (NeuronState itd md, [Transmitter itd md], ([STransmMorph], [STransmMorph]), [AxonState itd md])
    let sncontacts' = foldl (\neuacc (endneus,mrsimp,transm) ->
                        foldl (\neuacc2 (endneustate,refax) ->
                            let endneuid = neustateid endneustate
                            in case M.lookup endneuid neuacc2 of
                                Just (_,transms,(stransms,stransmsb),refaxs) -> M.insert endneuid (endneustate,transm:transms,((fst mrsimp):stransms,(snd mrsimp):stransmsb), refax:refaxs) neuacc2
                                otherwise -> M.insert endneuid (endneustate,[transm],([fst mrsimp],[snd mrsimp]), [refax]) neuacc2
                            ) neuacc endneus
                        ) M.empty sncontacts

    foldM_ (\(currneus,prevnids) stgidx_ -> do
        (morphresults,axonids,nids) <-
            foldM (\(morphacc,axidacc,nidacc) (nvisitstate,transms,stransms,refaxs) -> do

                let neuntfs = neustatentfs nvisitstate
                    ntfsvisit = map (\(DynNTF ntf,auxs) -> ntf'runneuvisit ntf (DynNTF ntf, getmaybe' $ lookup (DynNTF ntf) neuntfs) nvisitstate tstamp) $ drmrcfg'neuronntfs drmrcfg
                    neupremorphstate = nvisitstate {neustatentfs=ntfsvisit}
                n <- getneuron cerebrum (neustateid neupremorphstate) drmrcfg

                aspecttune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (AspectTune $ SignalTune $ aspecttune n) >>= either (\moderr -> do errorlog moderr; return $ map (\(sen,asps) -> (sen,zip asps $ cycle [Nothing])) $ drmrcfg'senses drmrcfg) (return . getmodsignal . gettune)
                senseirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (SenseIRCTune $ Tune $ senseirctune n) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                enzymeirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (EnzymeIRCTune $ Tune $ enzymeirctune n) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                tmuampirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmMuAmpIRCTune $ Tune $ tmuampirctune n) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                tfoampirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmFoAmpIRCTune $ Tune $ tfoampirctune n) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                triampirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmRIAmpIRCTune $ Tune $ triampirctune n) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                taspscaleirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmAspectScaleIRCTune $ Tune $ taspscaleirctune n) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                temphirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmEmphIRCTune $ Tune $ temphirctune n) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                tdemphirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmDemphIRCTune $ Tune $ tdemphirctune n) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                tbiasirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmBiasIRCTune $ Tune $ tbiasirctune n) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                trankirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmRankIRCTune $ Tune $ trankirctune n) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                tcontactlimirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmContactLimIRCTune $ Tune $ tcontactlimirctune n) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)

                let stgidx = mkQAtomPosInf stgidx_
                    tmeta = TMeta {
                                tmeta'stageindex = stgidx,
                                tmeta'timestamp = tstamp,
                                tmeta'maxstages = maxstages,
                                tmeta'maxcontact = maxcontact,
                                tmeta'aspecttune = aspecttune',
                                tmeta'senseirctune = senseirctune',
                                tmeta'enzymeirctune = enzymeirctune',
                                tmeta'tmuampirctune = tmuampirctune',
                                tmeta'tfoampirctune = tfoampirctune',
                                tmeta'triampirctune = triampirctune',
                                tmeta'taspscaleirctune = taspscaleirctune',
                                tmeta'temphirctune = temphirctune',
                                tmeta'tdemphirctune = tdemphirctune',
                                tmeta'tbiasirctune = tbiasirctune',
                                tmeta'trankirctune = trankirctune',
                                tmeta'tcontactlimirctune = tcontactlimirctune'
                            }
                    (tmorphs,tcls,tcorrs) = foldl (\(tmorphacc,tclacc,tcorracc) transm -> ((tmorph transm):tmorphacc,(tcontactlim transm):tclacc,(tcorr transm):tcorracc)) ([],[],[]) transms
                    morphres@(MorphResult n' (STMorph stmframp' staspscale',_) (mrcl,_)) = morph n neupremorphstate tmorphs stransms tcls refaxs drmrcfg tmeta
                    neupostmorphstate = NeuronState (uniqueid n') (senses n') ntfsvisit (activetransmavg n') -- morphosis shouldn't cause NTF state change
                    postmorph = PostMorph neupostmorphstate staspscale' (stmuamp stmframp') (stfoamp stmframp') (striamp stmframp') mrcl
                    ntfspostmorph = map (\(DynNTF ntf,auxs) -> ntf'runneupostmorph ntf (DynNTF ntf, getmaybe' $ lookup (DynNTF ntf) ntfsvisit) postmorph tstamp) $ drmrcfg'neuronntfs drmrcfg
                    neuprecorrstate = neupostmorphstate {neustatentfs=ntfspostmorph}
                    axidacc' = foldl (\acc axstate -> S.insert (axonstateid axstate) acc) axidacc refaxs
                    nidacc' = S.insert (uniqueid n') nidacc
                    morphacc' = morphacc ++ [(uniqueid n',(morphres,neuprecorrstate,tmeta,tcorrs))]

                return (morphacc',axidacc',nidacc')
                ) ([],S.empty,S.empty) $ M.elems currneus

        void $ async $ do
            facets <- flip (lab'prepfacets lab cerebrum) (drmrcfg,labcfg) $ map (\(_,(_,nstate,_,_)) -> nstate) morphresults
            sendengrams facets streamer engresid (drmrlog,errorlog)

        if stgidx_ == (fromIntegral $ floor $ (qval maxstages)-1)
            then return (M.empty, S.empty)
            else do
                contactlimits <- limitcontacts cerebrum (map (\(nid,(MorphResult _ _ (mrcl,_),_,_,_)) -> (nid,mrcl)) morphresults) (maxstages,maxcontact) drmrcfg

                contacts <- mapConcurrently (\(nid,cl) -> do
                                let (MorphResult n mrsimp (mrcl,clnov),neustate,tmeta,tcorrs) = getmaybe' $ lookup nid morphresults
                                ContactResult n_ axons'scores transm <- contact cerebrum n neustate cl (axonids,S.union nids prevnids) tcorrs drmrcfg tmeta
                                n' <- refreshneuron cerebrum n_ neustate mrsimp (map snd axons'scores) (mrcl,clnov) tmeta drmrcfg
                                let axsts = map (\(ax,_) -> AxonState (uniqueid ax) (ntfs ax) (neusenses ax) (neuntfs ax)) axons'scores
                                    ntfspostcontact = map (\(DynNTF ntf,auxs) -> ntf'runneupostcontact ntf (DynNTF ntf, getmaybe' $ lookup (DynNTF ntf) (neustatentfs neustate)) neustate axsts tstamp) $ drmrcfg'neuronntfs drmrcfg
                                    neustatepostcontact = neustate {neustatentfs=ntfspostcontact}
                                updateneuron cerebrum n' neustatepostcontact drmrcfg
                                endneus <- foldM (\neuacc (ax,corrinfo) -> do
                                                            let axntfs = ntfs ax
                                                                axstate = AxonState (uniqueid ax) axntfs (neusenses ax) (neuntfs ax)
                                                            endneuron <- getneuron cerebrum (enid ax) drmrcfg
                                                            let endneuronstate = NeuronState (uniqueid endneuron) (senses endneuron) (ntfs endneuron) (activetransmavg endneuron)
                                                                axntfspostcontact = map (\(DynNTF ntf,auxs) -> ntf'runaxonpostcontact ntf (DynNTF ntf, getmaybe' $ lookup (DynNTF ntf) axntfs) axstate (neustatepostcontact,endneuronstate) corrinfo tstamp) $ drmrcfg'axonntfs drmrcfg
                                                                axstatepostcontact = axstate {axonstatentfs=axntfspostcontact}
                                                                neuacc' = (endneuronstate,axstatepostcontact):neuacc
                                                            updateaxon cerebrum ax axstatepostcontact drmrcfg
                                                            return (neuacc')
                                                            ) [] axons'scores
                                return (endneus,mrsimp,transm)
                                ) contactlimits

                -- :: M.Map NeuralID (Neuron itd md, NeuronState itd md, [Transmitter itd md], ([STransmMorph], [STransmMorph]), [AxonState itd md])
                return $ (foldl (\neuacc (endneus,mrsimp,transm) ->
                    foldl (\neuacc2 (endneustate,refax) ->
                        let endneuid = neustateid endneustate
                        in case M.lookup endneuid neuacc2 of
                            Just (_,transms,(stransms,stransmsb),refaxs) -> M.insert endneuid (endneustate,transm:transms,((fst mrsimp):stransms,(snd mrsimp):stransmsb), refax:refaxs) neuacc2
                            otherwise -> M.insert endneuid (endneustate,[transm],([fst mrsimp],[snd mrsimp]), [refax]) neuacc2
                        ) neuacc endneus
                    ) M.empty contacts, nids)

        ) (sncontacts', snids) [1,2..((qval maxstages)-1)]

labpropagate lab cerebrum tmvarlabprpgstate tmvardrmrcfg labcfg streamer loggers pasync maxinfo pinfo = do
    Runs rs <- atomically $ takeTMVar tmvarlabprpgstate
    tmvarscdprpgstate <- atomically $ newTMVar $ Waiting
    if pasync
        then do
            putStrLn $ "Propagating.\nCurrently running "++(show $ rs+1)++" propagations."
            void $ atomically $ putTMVar tmvarlabprpgstate $ Runs $ rs+1
            void $ async $ do
                secondarypropagate lab cerebrum pinfo tmvarscdprpgstate tmvardrmrcfg labcfg streamer maxinfo loggers
                Runs rs' <- atomically $ takeTMVar tmvarlabprpgstate
                atomically $ putTMVar tmvarlabprpgstate $ Runs $ rs'-1
        else
            if rs == 1
                then do
                    void $ atomically $ putTMVar tmvarlabprpgstate $ Runs rs
                    putStrLn "| Wait for the current propagation to finish."
                else do
                    void $ atomically $ putTMVar tmvarlabprpgstate $ Runs $ rs+1
                    secondarypropagate lab cerebrum pinfo tmvarscdprpgstate tmvardrmrcfg labcfg streamer maxinfo loggers
                    Runs rs' <- atomically $ takeTMVar tmvarlabprpgstate
                    void $ atomically $ putTMVar tmvarlabprpgstate $ Runs $ rs'-1

secondarypropagate :: Lab l s c n a itd md
                    => l s c n a itd md
                    -> c n a itd md
                    -> (Transmitter itd md, [NeuralID])
                    -> TMVar TMVarState
                    -> TMVar (DreamerConfig itd md)
                    -> LabConfig
                    -> EngramStreamer
                    -> (MaxStages, MaxContact)
                    -> (Logger, ErrorLogger)
                    -> IO ()
secondarypropagate lab cerebrum (sntransm, snidlist) tmvarscdprpgstate tmvardrmrcfg labcfg streamer (maxstages,maxcontact) (drmrlog,errorlog) = do
    drmrcfg <- atomically $ readTMVar tmvardrmrcfg
    void $ atomically $ swapTMVar tmvarscdprpgstate Running
    ctime <- getnanotime
    startneus <- mapM (\nid -> getneuron cerebrum nid drmrcfg) snidlist
    engresid <- lab'config lab cerebrum >>= return . labcfg'engramstreamid
    let tstamp = mkQAtomPosInf ctime
        snids = S.fromList snidlist
    snmorphresults <- mapConcurrently (\sn -> do

                        aspecttune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (AspectTune $ SignalTune $ aspecttune sn) >>= either (\moderr -> do errorlog moderr; return $ map (\(sen,asps) -> (sen,zip asps $ cycle [Nothing])) $ drmrcfg'senses drmrcfg) (return . getmodsignal . gettune)
                        senseirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (SenseIRCTune $ Tune $ senseirctune sn) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                        enzymeirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (EnzymeIRCTune $ Tune $ enzymeirctune sn) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                        tmuampirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmMuAmpIRCTune $ Tune $ tmuampirctune sn) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                        tfoampirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmFoAmpIRCTune $ Tune $ tfoampirctune sn) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                        triampirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmRIAmpIRCTune $ Tune $ triampirctune sn) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                        taspscaleirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmAspectScaleIRCTune $ Tune $ taspscaleirctune sn) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                        temphirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmEmphIRCTune $ Tune $ temphirctune sn) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                        tdemphirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmDemphIRCTune $ Tune $ tdemphirctune sn) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                        tbiasirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmBiasIRCTune $ Tune $ tbiasirctune sn) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                        trankirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmRankIRCTune $ Tune $ trankirctune sn) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                        tcontactlimirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmContactLimIRCTune $ Tune $ tcontactlimirctune sn) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)

                        let stgidx = mkQAtomPosInf 0
                            tmeta = TMeta {
                                        tmeta'stageindex = stgidx,
                                        tmeta'timestamp = tstamp,
                                        tmeta'maxstages = maxstages,
                                        tmeta'maxcontact = maxcontact,
                                        tmeta'aspecttune = aspecttune',
                                        tmeta'senseirctune = senseirctune',
                                        tmeta'enzymeirctune = enzymeirctune',
                                        tmeta'tmuampirctune = tmuampirctune',
                                        tmeta'tfoampirctune = tfoampirctune',
                                        tmeta'triampirctune = triampirctune',
                                        tmeta'taspscaleirctune = taspscaleirctune',
                                        tmeta'temphirctune = temphirctune',
                                        tmeta'tdemphirctune = tdemphirctune',
                                        tmeta'tbiasirctune = tbiasirctune',
                                        tmeta'trankirctune = trankirctune',
                                        tmeta'tcontactlimirctune = tcontactlimirctune'
                                    }
                            nvisitstate = NeuronState (uniqueid sn) (senses sn) (ntfs sn) (activetransmavg sn)
                            neuntfs = neustatentfs nvisitstate
                            ntfsvisit = map (\(DynNTF ntf,auxs) -> ntf'runneuvisit ntf (DynNTF ntf, getmaybe' $ lookup (DynNTF ntf) neuntfs) nvisitstate tstamp) $ drmrcfg'neuronntfs drmrcfg
                            neupremorphstate = nvisitstate {neustatentfs=ntfsvisit}
                            morphres@(MorphResult sn' (STMorph stmframp' staspscale',_) (mrcl,_)) = modmorph sn' neupremorphstate (tmorph sntransm) (tcontactlim sntransm) drmrcfg tmeta
                            neupostmorphstate = NeuronState (uniqueid sn') (senses sn') ntfsvisit (activetransmavg sn') -- morphosis shouldn't cause NTF state change
                            postmorph = PostMorph neupostmorphstate staspscale' (stmuamp stmframp') (stfoamp stmframp') (striamp stmframp') mrcl
                            ntfspostmorph = map (\(DynNTF ntf,auxs) -> ntf'runneupostmorph ntf (DynNTF ntf, getmaybe' $ lookup (DynNTF ntf) ntfsvisit) postmorph tstamp) $ drmrcfg'neuronntfs drmrcfg
                            neuprecorrstate = neupostmorphstate {neustatentfs=ntfspostmorph}

                        return (uniqueid sn',(morphres,neuprecorrstate,tmeta,tcorr sntransm))
                        ) startneus

    void $ async $ do
        facets <- flip (lab'prepfacets lab cerebrum) (drmrcfg,labcfg) $ map (\(_,(_,nstate,_,_)) -> nstate) snmorphresults
        sendengrams facets streamer engresid (drmrlog,errorlog)

    sncontactlimits <- limitcontacts cerebrum (map (\(snid,(MorphResult _ _ (mrcl,_),_,_,_)) -> (snid,mrcl)) snmorphresults) (maxstages,maxcontact) drmrcfg

    sncontacts <- mapConcurrently (\(snid,cl) -> do
                    let (MorphResult sn mrsimp (mrcl,clnov),neustate,tmeta,tcorrs) = getmaybe' $ lookup snid snmorphresults
                    ContactResult sn_ axons'scores transm <- contact cerebrum sn neustate cl (S.empty, snids) [tcorr sntransm] drmrcfg tmeta
                    sn' <- refreshneuron cerebrum sn_ neustate mrsimp (map snd axons'scores) (mrcl,clnov) tmeta drmrcfg
                    let axsts = map (\(ax,_) -> AxonState (uniqueid ax) (ntfs ax) (neusenses ax) (neuntfs ax)) axons'scores
                        ntfspostcontact = map (\(DynNTF ntf,auxs) -> ntf'runneupostcontact ntf (DynNTF ntf, getmaybe' $ lookup (DynNTF ntf) (neustatentfs neustate)) neustate axsts tstamp) $ drmrcfg'neuronntfs drmrcfg
                        neustatepostcontact = neustate {neustatentfs=ntfspostcontact}
                    updateneuron cerebrum sn' neustatepostcontact drmrcfg
                    endneus <- foldM (\neuacc (ax,corrinfo) -> do
                                                let axntfs = ntfs ax
                                                    axstate = AxonState (uniqueid ax) axntfs (neusenses ax) (neuntfs ax)
                                                endneuron <- getneuron cerebrum (enid ax) drmrcfg
                                                let endneuronstate = NeuronState (uniqueid endneuron) (senses endneuron) (ntfs endneuron) (activetransmavg endneuron)
                                                    axntfspostcontact = map (\(DynNTF ntf,auxs) -> ntf'runaxonpostcontact ntf (DynNTF ntf, getmaybe' $ lookup (DynNTF ntf) axntfs) axstate (neustatepostcontact,endneuronstate) corrinfo tstamp) $ drmrcfg'axonntfs drmrcfg
                                                    axstatepostcontact = axstate {axonstatentfs=axntfspostcontact}
                                                    neuacc' = (endneuronstate,axstatepostcontact):neuacc
                                                updateaxon cerebrum ax axstatepostcontact drmrcfg
                                                return (neuacc')
                                                ) [] axons'scores
                    return (endneus,mrsimp,transm)
                    ) sncontactlimits

    -- :: M.Map NeuralID (NeuronState itd md, [Transmitter itd md], ([STransmMorph], [STransmMorph]), [AxonState itd md])
    let sncontacts' = foldl (\neuacc (endneus,mrsimp,transm) ->
                        foldl (\neuacc2 (endneustate,refax) ->
                            let endneuid = neustateid endneustate
                            in case M.lookup endneuid neuacc2 of
                                Just (_,transms,(stransms,stransmsb),refaxs) -> M.insert endneuid (endneustate,transm:transms,((fst mrsimp):stransms,(snd mrsimp):stransmsb), refax:refaxs) neuacc2
                                otherwise -> M.insert endneuid (endneustate,[transm],([fst mrsimp],[snd mrsimp]), [refax]) neuacc2
                            ) neuacc endneus
                        ) M.empty sncontacts

    foldM_ (\(currneus,prevnids) stgidx_ -> do
        (morphresults,axonids,nids) <-
            foldM (\(morphacc,axidacc,nidacc) (nvisitstate,transms,stransms,refaxs) -> do

                let neuntfs = neustatentfs nvisitstate
                    ntfsvisit = map (\(DynNTF ntf,auxs) -> ntf'runneuvisit ntf (DynNTF ntf, getmaybe' $ lookup (DynNTF ntf) neuntfs) nvisitstate tstamp) $ drmrcfg'neuronntfs drmrcfg
                    neupremorphstate = nvisitstate {neustatentfs=ntfsvisit}
                n <- getneuron cerebrum (neustateid neupremorphstate) drmrcfg

                aspecttune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (AspectTune $ SignalTune $ aspecttune n) >>= either (\moderr -> do errorlog moderr; return $ map (\(sen,asps) -> (sen,zip asps $ cycle [Nothing])) $ drmrcfg'senses drmrcfg) (return . getmodsignal . gettune)
                senseirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (SenseIRCTune $ Tune $ senseirctune n) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                enzymeirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (EnzymeIRCTune $ Tune $ enzymeirctune n) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                tmuampirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmMuAmpIRCTune $ Tune $ tmuampirctune n) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                tfoampirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmFoAmpIRCTune $ Tune $ tfoampirctune n) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                triampirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmRIAmpIRCTune $ Tune $ triampirctune n) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                taspscaleirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmAspectScaleIRCTune $ Tune $ taspscaleirctune n) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                temphirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmEmphIRCTune $ Tune $ temphirctune n) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                tdemphirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmDemphIRCTune $ Tune $ tdemphirctune n) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                tbiasirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmBiasIRCTune $ Tune $ tbiasirctune n) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                trankirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmRankIRCTune $ Tune $ trankirctune n) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)
                tcontactlimirctune' <- flip (lab'fromrawtuning lab cerebrum) (drmrcfg,labcfg) (TransmContactLimIRCTune $ Tune $ tcontactlimirctune n) >>= either (\moderr -> do errorlog moderr; return Nothing) (return . getmod . gettune)

                let stgidx = mkQAtomPosInf stgidx_
                    tmeta = TMeta {
                                tmeta'stageindex = stgidx,
                                tmeta'timestamp = tstamp,
                                tmeta'maxstages = maxstages,
                                tmeta'maxcontact = maxcontact,
                                tmeta'aspecttune = aspecttune',
                                tmeta'senseirctune = senseirctune',
                                tmeta'enzymeirctune = enzymeirctune',
                                tmeta'tmuampirctune = tmuampirctune',
                                tmeta'tfoampirctune = tfoampirctune',
                                tmeta'triampirctune = triampirctune',
                                tmeta'taspscaleirctune = taspscaleirctune',
                                tmeta'temphirctune = temphirctune',
                                tmeta'tdemphirctune = tdemphirctune',
                                tmeta'tbiasirctune = tbiasirctune',
                                tmeta'trankirctune = trankirctune',
                                tmeta'tcontactlimirctune = tcontactlimirctune'
                            }
                    (tmorphs,tcls,tcorrs) = foldl (\(tmorphacc,tclacc,tcorracc) transm -> ((tmorph transm):tmorphacc,(tcontactlim transm):tclacc,(tcorr transm):tcorracc)) ([],[],[]) transms
                    morphres@(MorphResult n' (STMorph stmframp' staspscale',_) (mrcl,_)) = morph n neupremorphstate tmorphs stransms tcls refaxs drmrcfg tmeta
                    neupostmorphstate = NeuronState (uniqueid n') (senses n') ntfsvisit (activetransmavg n') -- morphosis shouldn't cause NTF state change
                    postmorph = PostMorph neupostmorphstate staspscale' (stmuamp stmframp') (stfoamp stmframp') (striamp stmframp') mrcl
                    ntfspostmorph = map (\(DynNTF ntf,auxs) -> ntf'runneupostmorph ntf (DynNTF ntf, getmaybe' $ lookup (DynNTF ntf) ntfsvisit) postmorph tstamp) $ drmrcfg'neuronntfs drmrcfg
                    neuprecorrstate = neupostmorphstate {neustatentfs=ntfspostmorph}
                    axidacc' = foldl (\acc axstate -> S.insert (axonstateid axstate) acc) axidacc refaxs
                    nidacc' = S.insert (uniqueid n) nidacc
                    morphacc' = (uniqueid n,(morphres,neuprecorrstate,tmeta,tcorrs)):morphacc

                return (morphacc',axidacc',nidacc')
                ) ([],S.empty,S.empty) $ M.elems currneus

        void $ async $ do
            facets <- flip (lab'prepfacets lab cerebrum) (drmrcfg,labcfg) $ map (\(_,(_,nstate,_,_)) -> nstate) morphresults
            sendengrams facets streamer engresid (drmrlog,errorlog)

        if stgidx_ == (fromIntegral $ floor $ (qval maxstages)-1)
            then return (M.empty, S.empty)
            else do
                contactlimits <- limitcontacts cerebrum (map (\(nid,(MorphResult _ _ (mrcl,_),_,_,_)) -> (nid,mrcl)) morphresults) (maxstages,maxcontact) drmrcfg

                contacts <- mapConcurrently (\(nid,cl) -> do
                                let (MorphResult n mrsimp (mrcl,clnov),neustate,tmeta,tcorrs) = getmaybe' $ lookup nid morphresults
                                ContactResult n_ axons'scores transm <- contact cerebrum n neustate cl (axonids,S.union nids prevnids) tcorrs drmrcfg tmeta
                                n' <- refreshneuron cerebrum n_ neustate mrsimp (map snd axons'scores) (mrcl,clnov) tmeta drmrcfg
                                let axsts = map (\(ax,_) -> AxonState (uniqueid ax) (ntfs ax) (neusenses ax) (neuntfs ax)) axons'scores
                                    ntfspostcontact = map (\(DynNTF ntf,auxs) -> ntf'runneupostcontact ntf (DynNTF ntf, getmaybe' $ lookup (DynNTF ntf) (neustatentfs neustate)) neustate axsts tstamp) $ drmrcfg'neuronntfs drmrcfg
                                    neustatepostcontact = neustate {neustatentfs=ntfspostcontact}
                                updateneuron cerebrum n' neustatepostcontact drmrcfg
                                endneus <- foldM (\neuacc (ax,corrinfo) -> do
                                                            let axntfs = ntfs ax
                                                                axstate = AxonState (uniqueid ax) axntfs (neusenses ax) (neuntfs ax)
                                                            endneuron <- getneuron cerebrum (enid ax) drmrcfg
                                                            let endneuronstate = NeuronState (uniqueid endneuron) (senses endneuron) (ntfs endneuron) (activetransmavg endneuron)
                                                                axntfspostcontact = map (\(DynNTF ntf,auxs) -> ntf'runaxonpostcontact ntf (DynNTF ntf, getmaybe' $ lookup (DynNTF ntf) axntfs) axstate (neustatepostcontact,endneuronstate) corrinfo tstamp) $ drmrcfg'axonntfs drmrcfg
                                                                axstatepostcontact = axstate {axonstatentfs=axntfspostcontact}
                                                                neuacc' = (endneuronstate,axstatepostcontact):neuacc
                                                            updateaxon cerebrum ax axstatepostcontact drmrcfg
                                                            return (neuacc')
                                                            ) [] axons'scores
                                return (endneus,mrsimp,transm)
                                ) contactlimits

                -- :: M.Map NeuralID (Neuron itd md, NeuronState itd md, [Transmitter itd md], ([STransmMorph], [STransmMorph]), [AxonState itd md])
                return $ (foldl (\neuacc (endneus,mrsimp,transm) ->
                    foldl (\neuacc2 (endneustate,refax) ->
                        let endneuid = neustateid endneustate
                        in case M.lookup endneuid neuacc2 of
                            Just (_,transms,(stransms,stransmsb),refaxs) -> M.insert endneuid (endneustate,transm:transms,((fst mrsimp):stransms,(snd mrsimp):stransmsb), refax:refaxs) neuacc2
                            otherwise -> M.insert endneuid (endneustate,[transm],([fst mrsimp],[snd mrsimp]), [refax]) neuacc2
                        ) neuacc endneus
                    ) M.empty contacts, nids)

        ) (sncontacts', snids) [1,2..((qval maxstages)-1)]

    void $ atomically $ swapTMVar tmvarscdprpgstate Waiting

type LessonDrainer = KafkaConsumer

-- for now, one sense per academy
-- TODO map concurrently over batch then fold through result for any errors
fetchlessons :: (IntuitiveProcess itd md QAtomPos)
                => (AcademyID, TMVar (M.Map TutorID Lesson))
                -> LessonDrainer
                -> TMVar (DreamerConfig itd md)
                -> (Logger, ErrorLogger)
                -> IO ()
fetchlessons (academyid,tmvartutors) drainer tmvardrmrcfg (drmrlog,errorlog) = do
    drmrcfg <- atomically $ readTMVar tmvardrmrcfg
    let fchtmt = acadmcfg'fetchtimeout $ getmaybe' $ lookup academyid $ drmrcfg'academies drmrcfg
        fchsz = acadmcfg'fetchsize $ getmaybe' $ lookup academyid $ drmrcfg'academies drmrcfg
    emsgs <- pollMessageBatch drainer (Timeout $ floor $ qval fchtmt) (BatchSize $ floor $ qval fchsz)
    -- (fetch error, missing tutor, bad tutor id, out of range tutor id, nonsensical tutor id, missing lesson value, bad lesson value, nonsensical lesson)
    (e0,e1,e2,e3,e4,e5,e6,e7) <-
        foldM (\(e0,e1,e2,e3,e4,e5,e6,e7) emsg ->
            case emsg of
                Left _ -> return (e0,e1,e2,e3,e4,e5,e6,e7) -- return (e0+1,e1,e2,e3,e4,e5,e6,e7)
                Right record -> case crKey record of
                    Nothing -> return (e0,e1+1,e2,e3,e4,e5,e6,e7)
                    Just bacadm -> case (readMaybe (B.unpack bacadm) :: Maybe TutorID) of
                        Nothing -> return (e0,e1,e2+1,e3,e4,e5,e6,e7)
                        Just tutorid -> do
                            let maxtuts = acadmcfg'maxtutors $ getmaybe' $ lookup academyid $ drmrcfg'academies drmrcfg
                            if tutorid < 0 || tutorid > ((floor $ qval maxtuts)-1)
                                then return (e0,e1,e2,e3+1,e4,e5,e6,e7)
                                else do
                                    tutors <- atomically $ readTMVar tmvartutors
                                    case M.lookup tutorid tutors of
                                        Nothing -> return (e0,e1,e2,e3,e4+1,e5,e6,e7)
                                        Just olesson -> case crValue record of
                                            Nothing -> return (e0,e1,e2,e3,e4,e5+1,e6,e7)
                                            Just blessonstr -> case (readMaybe (B.unpack blessonstr) :: Maybe Lesson) of
                                                Nothing -> return (e0,e1,e2,e3,e4,e5,e6+1,e7)
                                                Just lesson@((aid,(_,subasps)),parts) -> do
                                                    valid <- foldM (\acc aspid -> do
                                                                if acc
                                                                    then return $ case lookup aspid $ acadmcfg'aspects $ getmaybe' $ lookup academyid $ drmrcfg'academies drmrcfg of
                                                                        Nothing -> False
                                                                        otherwise -> True
                                                                    else return False
                                                                ) True $ (aid:(map fst subasps))++(concatMap (\(aid',(_,subasps')) -> aid':(map fst subasps')) parts)
                                                    if valid
                                                        then do
                                                            tutors' <- atomically $ takeTMVar tmvartutors
                                                            atomically $ putTMVar tmvartutors $ M.insert tutorid lesson tutors'
                                                            return (e0,e1,e2,e3,e4,e5,e6,e7)
                                                        else return (e0,e1,e2,e3,e4,e5,e6,e7+1)
            ) (0,0,0,0,0,0,0,0) emsgs
    mberr <- commitAllOffsets OffsetCommit drainer
    -- logerrors errorlog e0 $ "Failed to fetch from academy, "++academyid
    logerrors errorlog e1 $ "Missing tutors - academy, "++academyid
    logerrors errorlog e2 $ "Bad tutor id given - academy, "++academyid
    logerrors errorlog e3 $ "Out of range tutor id given - academy, "++academyid
    logerrors errorlog e4 $ "Nonsensical tutor id given - academy, "++academyid
    logerrors errorlog e5 $ "Missing lesson value - academy, "++academyid
    logerrors errorlog e6 $ "Bad lesson value - academy, "++academyid
    logerrors errorlog e7 $ "Nonsensical lesson given - academy, "++academyid
    maybe (return ()) (errorlog . ((++) (" ("++academyid++")")) . show) mberr

sendengrams :: [EngramSignal Double] -> KafkaProducer -> String -> (Logger, ErrorLogger) -> IO ()
sendengrams sigs streamer topic (drmrlog,errorlog) = do
    let precs = map ((mkmsg topic Nothing) . Just . B.pack . BL.unpack . encode) sigs
    errs <- produceMessageBatch streamer precs
    flushProducer streamer
    logerrors errorlog (length errs) "had issues streaming engrams"

tuningoptions :: [(String, Tune m itd md -> TuningMod m itd md)]
tuningoptions =
    [
        ("aspecttune", AspectTune),
        ("senseirctune", SenseIRCTune),
        ("enzymeirctune", EnzymeIRCTune),
        ("muampirctune", TransmMuAmpIRCTune),
        ("foampirctune", TransmFoAmpIRCTune),
        ("riampirctune", TransmRIAmpIRCTune),
        ("aspscaleirctune", TransmAspectScaleIRCTune),
        ("emphirctune", TransmEmphIRCTune),
        ("demphirctune", TransmDemphIRCTune),
        ("biasirctune", TransmBiasIRCTune),
        ("rankirctune", TransmRankIRCTune),
        ("contactlimirctune", TransmContactLimIRCTune)
    ]

sensoryoptions = ["enroll", "graduate"]
transmitteroptions = ["transmitter"]
configoptions = ["maxcontact", "maxstages"]

initmodprocessor :: forall l s c n a itd md. Lab l s c n a itd md
                    => KafkaConsumer
                    -> l s c n a itd md
                    -> TMVar TMVarState
                    -> TMVar (DreamerConfig itd md)
                    -> LabConfig
                    -> KafkaProducer
                    -> Maybe (Chief,Crew,TMVar (S.Set WorkerID))
                    -> (c n a itd md, KafkaProducer)
                    -> (DreamerInfo -> IO ())
                    -> IO ()
                    -> (Logger, ErrorLogger)
                    -> IO ()
initmodprocessor drainer lab tmvarscdprpgstate tmvardrmrcfg labcfg streamer mbcrewstate (cerebrum, prpgprod) savedr sleepdr (drmrlog,errorlog) =
    forever $ do
        let modoptions = (map fst tuningoptions) ++ transmitteroptions ++ sensoryoptions ++ configoptions
            modresid = labcfg'modresponseid labcfg
        emsg <- pollMessage drainer (Timeout (maxBound :: Int))
        drmrcfg <- atomically $ readTMVar tmvardrmrcfg
        case emsg of
            Left _ -> return ()
            Right record -> case crKey record of
                Just boption -> let
                    option = B.unpack boption
                    in if option `elem` modoptions
                        then case crValue record of
                            Nothing -> answermodder streamer option "No mod given" Nothing True modresid (drmrlog,errorlog)
                            Just bmod ->
                                case (readEither (B.unpack bmod) :: Either String Mod) of
                                    -- bad parse: error
                                    Left err -> answermodder streamer option err Nothing True modresid (drmrlog,errorlog)
                                    Right (TuningMod tunestr rng) -> do
                                        let resv = Just [("range", show rng)]
                                        case (readEither tunestr :: Either String RawTune)  of
                                            Left err -> answermodder streamer option err resv True modresid (drmrlog,errorlog)
                                            Right rawtune -> do
                                                let mbtune = case rawtune of
                                                                RawTune rawmod -> Just (Tune rawmod)
                                                                RawSignalTune rawsig -> let
                                                                    invsenses = drmrcfg'invsenses drmrcfg
                                                                    invaspects = drmrcfg'invaspects drmrcfg
                                                                    tunesig = foldr (\(sid,rawasps) mbacc ->
                                                                                case mbacc of
                                                                                    Nothing -> Nothing
                                                                                    Just acc -> case lookup (mkQMetaID sid) invsenses of
                                                                                        Nothing -> Nothing
                                                                                        Just dynsense ->
                                                                                            let tuneasps = foldr (\(aid,rawmod) aspmbacc ->
                                                                                                            case aspmbacc of
                                                                                                                Nothing -> Nothing
                                                                                                                Just aspacc -> case lookup (mkQMetaID aid) invaspects of
                                                                                                                    Nothing -> Nothing
                                                                                                                    Just dynasp -> Just $ (dynasp,rawmod):aspacc
                                                                                                            ) (Just []) rawasps
                                                                                            in case tuneasps of
                                                                                                Nothing -> Nothing
                                                                                                otherwise -> Just $ (dynsense,tuneasps):acc
                                                                                ) (Just []) rawsig
                                                                    in case tunesig of
                                                                        Nothing -> Nothing
                                                                        Just dynrawsig -> Just (SignalTune $ map (\(sen, Just asps) -> (sen,asps)) dynrawsig)
                                                case mbtune of
                                                    Nothing -> answermodder streamer option "Bad tune given" resv True modresid (drmrlog,errorlog)
                                                    Just tune -> do
                                                        labres <- lab'applytuningmod lab cerebrum ((getmaybe' $ lookup option tuningoptions) tune) rng (drmrcfg,labcfg)
                                                        case labres of
                                                            -- lab rejected it: error
                                                            Just moderr -> answermodder streamer option moderr resv True modresid (drmrlog,errorlog)
                                                            otherwise -> answermodder streamer option "" resv False modresid (drmrlog,errorlog)
                                    Right (TransmitterMod name m rng maxstages maxcontact) -> do
                                        let resv = Just [("name", name)]
                                        labres <- lab'fromrawtransm lab cerebrum m rng (drmrcfg,labcfg)
                                        case labres of
                                            -- lab rejected it: error
                                            Left moderr -> answermodder streamer option moderr resv True modresid (drmrlog,errorlog)
                                            Right (transm, nids) -> do
                                                answermodder streamer option "" resv False modresid (drmrlog,errorlog)
                                                scdprpgstate <- atomically $ readTMVar tmvarscdprpgstate
                                                case scdprpgstate of
                                                    Running -> answermodder streamer option "Wait for current propagation to end" resv True modresid (drmrlog,errorlog)
                                                    otherwise -> secondarypropagate lab cerebrum (transm, nids) tmvarscdprpgstate tmvardrmrcfg labcfg prpgprod (maxstages,maxcontact) (drmrlog,errorlog)
                                    Right (MkSensoryMod acdm@(acdmid,_) tutidx rng) -> do
                                        let resv = Just [("academyid", show acdmid), ("range", show rng)]
                                        case lookup acdmid $ drmrcfg'academies drmrcfg of
                                            Nothing -> answermodder streamer option ("Nonsensical academy given, "++acdmid) resv True modresid (drmrlog,errorlog)
                                            Just acadmcfg -> do
                                                let maxtuts = acadmcfg'maxtutors acadmcfg
                                                if tutidx < 0 || tutidx > ((floor $ qval maxtuts)-1)
                                                    then answermodder streamer option "Out of range tutor id given" resv True modresid (drmrlog,errorlog)
                                                    else do
                                                        labres <- lab'enroll lab cerebrum acdm tutidx maxtuts rng (drmrcfg,labcfg)
                                                        case labres of
                                                            -- lab rejected it: error
                                                            Just moderr -> answermodder streamer option moderr resv True modresid (drmrlog,errorlog)
                                                            otherwise -> answermodder streamer option "" resv False modresid (drmrlog,errorlog)
                                    Right (GraduateMod rng) -> do
                                        let resv = Just [("range", show rng)]
                                        labres <- lab'graduate lab cerebrum rng (drmrcfg,labcfg)
                                        case labres of
                                            -- lab rejected it: error
                                            Just moderr -> answermodder streamer option moderr resv True modresid (drmrlog,errorlog)
                                            otherwise -> answermodder streamer option "" resv False modresid (drmrlog,errorlog)
                                    Right (MaxContactMod acdmid q) -> do
                                        let resv = Just [("value", show q), ("academyid", acdmid)]
                                        case lookup acdmid $ drmrcfg'academies drmrcfg of
                                            Nothing -> answermodder streamer option "Nonsensical academy ID given" resv True modresid (drmrlog,errorlog)
                                            Just acadmcfg -> do
                                                drmrcfg' <- atomically $ takeTMVar tmvardrmrcfg
                                                atomically $ putTMVar tmvardrmrcfg $ drmrcfg' {drmrcfg'academies=M.toList $ M.insert acdmid acadmcfg {acadmcfg'maxcontact=q} $ M.fromList $ drmrcfg'academies drmrcfg'}
                                                answermodder streamer option "" resv False modresid (drmrlog,errorlog)
                                    Right (MaxStagesMod acdmid q) -> do
                                        let resv = Just [("value", show q), ("academyid", acdmid)]
                                        case lookup acdmid $ drmrcfg'academies drmrcfg of
                                            Nothing -> do
                                                answermodder streamer option "Nonsensical academy ID given" resv True modresid (drmrlog,errorlog)
                                                atomically $ putTMVar tmvardrmrcfg drmrcfg
                                            Just acadmcfg -> do
                                                atomically $ putTMVar tmvardrmrcfg $ drmrcfg {drmrcfg'academies=M.toList $ M.insert acdmid acadmcfg {acadmcfg'maxstages=q} $ M.fromList $ drmrcfg'academies drmrcfg}
                                                answermodder streamer option "" resv False modresid (drmrlog,errorlog)
                                    Right (RemoveWorker wid) -> do
                                        let resv = Just [("workerId", wid)]
                                        case mbcrewstate of
                                            Nothing -> answermodder streamer option "The crew modification option applies only to distributed labs" resv True modresid (drmrlog,errorlog)
                                            Just (_,crewstate,delwidstate) -> do
                                                crew <- atomically $ takeTMVar crewstate
                                                case M.lookup wid crew of
                                                    Nothing -> do
                                                        atomically $ putTMVar crewstate crew
                                                        answermodder streamer option "Nonsensical worker ID given" resv True modresid (drmrlog,errorlog)
                                                    Just (_,_,_,js) -> do
                                                        delwids <- atomically $ takeTMVar delwidstate
                                                        if js == 0
                                                            then do
                                                                atomically $ putTMVar delwidstate delwids
                                                                atomically $ putTMVar crewstate $ M.delete wid crew
                                                                drmrlog $ "Removed worker, " ++ wid
                                                                answermodder streamer option "Worker has been removed" resv False modresid (drmrlog,errorlog)
                                                            else do
                                                                atomically $ putTMVar delwidstate $ S.insert wid delwids
                                                                atomically $ putTMVar crewstate crew
                                                                answermodder streamer option "Worker will be removed after it has completed all of its jobs" resv False modresid (drmrlog,errorlog)
                                    Right GetWorkers -> do
                                        case mbcrewstate of
                                            Nothing -> answermodder streamer option "The crew modification option applies only to distributed labs" Nothing True modresid (drmrlog,errorlog)
                                            Just (ch,crewstate,delwidstate) -> do
                                                crew <- atomically $ readTMVar crewstate
                                                delwids <- atomically $ readTMVar delwidstate
                                                let resv = Just [("workerIds", show $ (chiefid ch):(M.keys crew)), ("toRemove", show $ S.toList delwids)]
                                                answermodder streamer option "(Lab chief ID is the first in the list of workers)" resv False modresid (drmrlog,errorlog)
                                    Right DreamerPersist -> do
                                        savedr $ drmrcfg'drmrinfo drmrcfg
                                        answermodder streamer option "Persisted dreamer cerebrum" Nothing False modresid (drmrlog,errorlog)
                                    Right DreamerSleep -> sleepdr

                        -- invalid option: error
                        else answermodder streamer option "Invalid option given" Nothing True modresid (drmrlog,errorlog)

                -- no key: error
                otherwise -> answermodder streamer "" "No option given" Nothing True modresid (drmrlog,errorlog)

        commitAllOffsets OffsetCommit drainer >>= maybe (return ()) (errorlog . show)

answermodder :: KafkaProducer -> String -> String -> Maybe [(String, String)] -> Bool -> String -> (Logger, ErrorLogger) -> IO ()
answermodder streamer option msg mbinfo iserr topic (drmrlog,errorlog) = let
    iserr' = if iserr then "true" else "false"
    msg' = "{\"error\":"++iserr'++", \"option\":"++(show option)++", \"message\":"++(show msg)
    msg'' = case mbinfo of
        Just info -> (foldl (\msgacc (k,v) -> msgacc++", "++(show k)++":"++(show v)) msg' info) ++ "}"
        otherwise -> msg' ++ "}"
    in do
        err <- produceMessage streamer (mkmsg topic Nothing (Just $ B.pack msg''))
        flushProducer streamer
        forM_ err (errorlog . show)

consumerprops :: String -> [BrokerAddress] -> ConsumerProperties
consumerprops groupid brkrs = C.brokersList brkrs
             <> groupId (ConsumerGroupId groupid)
             <> noAutoCommit

consumersub :: [String] -> Subscription
consumersub tpcs = (topics $ map TopicName tpcs)
           <> offsetReset Earliest

producerprops :: [BrokerAddress] -> ProducerProperties
producerprops brkrs = P.brokersList brkrs
             <> sendTimeout (Timeout 0)

mkmsg :: String -> Maybe B.ByteString -> Maybe B.ByteString -> ProducerRecord
mkmsg topic k v = ProducerRecord
                  { prTopic = TopicName topic
                  , prPartition = UnassignedPartition
                  , prKey = k
                  , prValue = v
                  }
