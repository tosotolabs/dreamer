{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Dreamer.Conscious
(
    Conscious(..),
) where

import           Dreamer.Academy
import           Dreamer.Atmosphere.Atom
import           Dreamer.Atmosphere.Molecule
import           Dreamer.Atmosphere.ProboCorpus
import           Dreamer.Configuration
import           Dreamer.Metadata
import           Dreamer.Neural.Cerebrum
import           Dreamer.Neural.NTF
import           Dreamer.Neural.Sense
import           Dreamer.Neural.Type

class (Cerebrum c n a itd md, Metadata (dr c n a itd md)) => Conscious dr c n a itd md where
    -- | Dreamer's cerebrum
    dr'cerebrum :: dr c n a itd md -> c n a itd md
