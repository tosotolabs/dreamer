module Dreamer.Academy
(
    Academy,
    AcademyConfig(..),
    AcademyID,
    DefaultLesson,
    Education,
    FetchInterval,
    FetchSize,
    FetchTimeout,
    Lesson,
    LessonPart,
    MaxTutors,
    TutorID
) where

import           Dreamer.Atmosphere.Atom
import           Dreamer.Metadata
import           Dreamer.Neural.Sense
import           Dreamer.Neural.Type

-- A little confusion: SenseID corresponds to the meta ID of a sense while
-- AspectID corresponsds to an academy's associated ID to a dreamer's aspect
-- FIXME SenseID is currently utterly useless

type Academy = (AcademyID, SenseID)
data AcademyConfig itd md = AcademyConfig {
                            acadmcfg'maxtutors      :: MaxTutors,
                            acadmcfg'fetchinterval  :: FetchInterval,
                            acadmcfg'fetchtimeout   :: FetchTimeout,
                            acadmcfg'fetchsize      :: FetchSize,
                            acadmcfg'maxstages      :: MaxStages,
                            acadmcfg'maxcontact     :: MaxContact,
                            acadmcfg'deflesson      :: DefaultLesson,
                            acadmcfg'aspects        :: [(AspectID, DynAspect itd md)],
                            acadmcfg'engramstreamid :: String,
                            acadmcfg'groupid        :: String -- ^ group ID for this academy's designated kafka consumer
                        }
type AcademyID = MetaID
type Education = (Academy, TutorID)
type FetchInterval = QAtomPosInf
type FetchSize = QAtomPosInf
type FetchTimeout = QAtomPosInf
type LessonPart = (AspectID, (Double, [(AspectID, Double)]))
type Lesson = (LessonPart, [LessonPart]) -- Change data rather than concrete data
type MaxTutors = QAtomPosInf
type TutorID = Integer -- a tutor could be a specific pixel index, for example
type DefaultLesson = Lesson
