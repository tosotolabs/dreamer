{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes            #-}

{- Here lies a model for intuition. -}

module Dreamer.Mufori
(
    Fo(..),
    Fo2(..),
    MFRAmp,
    MFRCombo,
    MFREntry,
    MFRRI,
    MFRTuning(..),
    Mu(..),
    Mu2(..),
    RI(..),
    RI2(..),
    Tuning(..),

    Process(..),
) where

import           Dreamer.Atmosphere.Atom
import           Dreamer.Metadata
import           Dreamer.Process.Update

type MFREntry v = v
type MFRAmp = CoreProcess
type MFRCombo v = ComboProcess v
type MFRRI v = v
type ComboTuning v = MFRCombo v QAtomPos -> MFRRI v -> ProcessUpdate (MFRCombo v) QAtomPos
type AmpTuning v = MFRAmp v -> QAtomPos -> ProcessUpdate MFRAmp v
data MFRTuning cv av = MFRTuning {
    combotuning :: ComboTuning cv,
    amptuning   :: AmpTuning av
}

data Tuning cv av = Tuning {
    tuningbias :: QMetaID QPosInf,
    mfrtuning  :: MFRTuning cv av
}

class (MFRProcess d v) => Mu d v where
    -- Params: Mu process, new RI value, a new element entry, function to update combo process (this takes the combo process of the subjecct Mu,
    -- the new RI, and returns an updated combo), function to update amp process (this takes the amp of Mu process, the diversity value of the
    -- aforementioned combo process, and returns an updated amp process)
    -- Returns an updated Mu process
    updmu :: d v
            -> MFRRI v
            -> MFREntry v
            -> (MFRCombo v QAtomPos -> MFRRI v -> ProcessUpdate (MFRCombo v) QAtomPos)
            -> (MFRAmp QAtomFull -> QAtomPos -> ProcessUpdate MFRAmp QAtomFull)
            -> Maybe (MFRTuning v QAtomFull)
            -> (ProcessUpdate d) v

class (MFRProcess d v) => Fo d v where
    -- Params: Fo process, new RI value, a new element entry, function to update combo process (this takes the combo process of the subjecct Fo,
    -- the new RI, and returns an updated combo), function to update amp process (this takes the amp of Fo process, the diversity value of the
    -- aforementioned combo process, and returns an updated amp process)
    -- Returns an updated Fo process
    updfo :: d v
            -> MFRRI v
            -> MFREntry v
            -> (MFRCombo v QAtomPos -> MFRRI v -> ProcessUpdate (MFRCombo v) QAtomPos)
            -> (MFRAmp QAtomFull -> QAtomPos -> ProcessUpdate MFRAmp QAtomFull)
            -> Maybe (MFRTuning v QAtomFull)
            -> (ProcessUpdate d) v

-- This is the part of perception that measures consistency of reality. It is the process to be first updated amongst the Mufori triad.
class (MFRProcess d v) => RI d v where
    -- Params: RI process, new element entry (reality facet), function to update combo process (this takes the combo process of the subject RI,
    -- the new element, and returns an updated combo process), function to update amp process (this takes the amp of RI process, the diversity
    -- value of the aforementioned combo process, and returns an updated amp process)
    -- Returns an updated RI process
    updri :: d v
            -> MFREntry v
            -> (MFRCombo v QAtomPos -> MFREntry v -> ProcessUpdate (MFRCombo v) QAtomPos)
            -> (MFRAmp QAtomFull -> QAtomPos -> ProcessUpdate MFRAmp QAtomFull)
            -> Maybe (MFRTuning v QAtomFull)
            -> (ProcessUpdate d) v

--
-- Processes with inner details.

class (MFRProcess2 d v) => Mu2 d v where

class (MFRProcess2 d v) => Fo2 d v where

class (MFRProcess2 d v) => RI2 d v where
