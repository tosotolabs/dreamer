import           Control.Concurrent.STM
import qualified Data.Map               as M (empty)
import           Dreamer.Lab
import           Dreamer.Lab.Crew
import           System.Environment     (setEnv)

env = "CREW_AUTH"
pass = "TheSweetestNabu"

dochiefserve :: String -> Int -> IO ()
dochiefserve h p = do
    let chf = Chief "Denry" 10 h p
    crewstate <- atomically $ newTMVar M.empty
    setEnv env pass
    cs <- chiefserve chf crewstate env print (putStrLn, putStrLn)
    case cs of
        Left err -> putStrLn err
        Right _  -> return ()

doworker :: String -> Int -> String -> IO ()
doworker h p chaddr= do
    let wrkr = Worker "TuxAnari" pass 10
    startworker wrkr h p (\mbg -> do print mbg; return mbg) chaddr "Denry" (putStrLn, putStrLn)
