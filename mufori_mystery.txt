Mufori mystery

State is defined as the duality of the conscious (core) and intuition (mufori).
State can also be explicitly defined as that duality and the duality's agreement (biased average).

Reaction is defined as processing some message in the conscious and intuition, changing state.

The messenger brings a message of change to the king's state.
The advisors process the offered state in their conscious and intuition.
We get back a reaction (novelty and a modification to the state) (AMR).
The king reacts to the messenger's offered state (KMR) in the same way.
The king reacts to his advisors' modifications (KAR).
We perform a fractal biased average of KMR, AMR, and KAR.

The offered amps are supplementary to the offered aspect value. That means the
offer is useless without a suggested aspect value. The center point for an aspect
is its amplitude or value.

The messenger offers a change in the following:
    1.) Aspect value (aspval)
    2.) Roughness Indicator amplification (riamp)
    3.) Mutational Tendency amplification (muamp)
    4.) Focus amplification (foamp)

--
Reactions to offers.

1.) Aspect value (aspval)
-- Advisors -[:REACTS_TO]-> aspval
The advisors update their conscious and intuition with aspval.
We get (advcorev, advcoren), (advmuv, advmun), and (advfov, advfon).
Take the biased average of the three to get advaspval.
-- King -[:REACTS_TO]-> aspval
The king updates his conscious and intuition with aspval.
We get (kingcorev, kingcoren), (kingmuv, kingmun), and (kingfov, kingfon).
Take the biased average of the three to get kingaspval.
-- King -[:REACTS_TO]-> Advisors' modified aspval
The king updates his conscious and intuition with advaspval.
We get (kingadvcorev, kingadvcoren), (kingadvmuv, kingadvmun), and (kingadvfov, kingadvfon).
Take the biased average of the three to get kingadvaspval.
-- Average reactions
The king takes the simple average of each of the novelties of the three reactions.
The three novelty averages are used in a biased average of advaspval, kingaspval, and kingadvaspval.
The result is aspval'

2.) Roughness Indicator amplification (riamp)
-- Advisors -[:REACTS_TO]-> riamp
The advisors update the RI portion of their intuition with aspval' and riamp.
We get (advriampv, advriampn)
-- King -[:REACTS_TO]-> riamp
The king updates the RI portion of his intuition with aspval' and riamp.
We get (kingriampv, kingriampn)
-- King -[:REACTS_TO]-> Advisors' RI amp
The king updates the RI portion of his intuition with aspval' and his advisors' amp value of their updated RI.
We get (kingadvriampv, kingadvriampn)
-- Average reactions
We perform a biased average of the three RI amps to get riamp'

3.) Mutational Tendency amplification (muamp)
-- Advisors -[:REACTS_TO]-> muamp
The advisors update the Mu portion of their intuition with advriampv, aspval', and muamp.
We get (advmuampv, advmuampn)
-- King -[:REACTS_TO]-> muamp
The king updates the Mu portion of his intuition with kingriampv, aspval', and muamp.
We get (kingmuampv, kingmuampn)
-- King -[:REACTS_TO]-> Advisors' Mu amp
The king updates the Mu portion of his intuition with kingadvriampv, aspval', and his advisors' amp value of their updated Mu.
We get (kingadvmuampv, kingadvmuampn)
-- Average reactions
We perform a biased average of the three Mu amps to get muamp'

4.) Focus amplification (foamp)
-- Advisors -[:REACTS_TO]-> foamp
The advisors update the Fo portion of their intuition with advriampv, aspval', and foamp.
We get (advfoampv, advfoampn)
-- King -[:REACTS_TO]-> foamp
The king updates the Fo portion of his intuition with kingriampv, aspval', and foamp.
We get (kingfoampv, kingfoampn)
-- King -[:REACTS_TO]-> Advisors' Fo amp
The king updates the Fo portion of his intuition with kingadvriampv, aspval' and his advisors' amp value of their updated Fo.
We get (kingadvfoampv, kingadvfoampn)
-- Average reactions
We perform a biased average of the three Fo amps to get foamp'

--
The king's final ruling

The king updates the RI portion of his intuition with aspval' and riamp'.
We get kingrin
The king updates the Mu portion of his intuition with kingriv, aspval' and muamp'.
We get (kingmuv', kingmun')
The king updates the Fo portion of his intuition with kingriv, aspval' and foamp'.
We get (kingfov', kingfon')
The king updates his conscious with aspval'
We get (kingcorev', kingcoren')
-- Get duality's agreement
We perform a biased average of kincorev', kingmuv', and kinfov' to get aspval''.
We update the king's conscious and intuition the core and mufori we just updated.
